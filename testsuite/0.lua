local image = Image()

function begin_test()
  set_resolution(1024, 600)
  
  require("lfs")
  local count = 0
  for i = 1, 1000 do
    local fn = string.format("testsuite/%d.lua", i)
    local attr = lfs.attributes(fn, "mode")
    if attr ~= "file" then
      break
    end
    count = count + 1
  end
  
  sprite = Sprite()
  display.viewport:add_child(sprite)
  local rows, cols = 8, 8
  local w, h = (1024/cols), (600/rows)
  local c = sprite.canvas
  for i = 1, count do
    local x = i%cols * w - 512
    local y = math.floor(i/rows) * h - 300
    x = x + w/2
    y = y + h/2
    print_log(i, x, y)
    c:move_to(x, y)
    c:rectangle(w - 2, h - 2)
    c:stroke()
  end
  local function press(x, y)
    x = x + 512
    y = y + 300
    local lx = math.floor(x/w)
    local ly = math.floor(y/h)
    local i = ly*cols + lx
    if i <= count then
      run_test(i)
    end
  end
  if mouse then
    mouse.on_press = function(m, b)
      press(mouse.xaxis, mouse.yaxis)
    end
  end
  if mtouch then
    mtouch.on_begin = function(m, x, y)
      press(x, y)
    end
  end
end

function end_test()
  clear_log()
  if mouse then
    mouse.on_press = nil
  end
  if mtouch then
    mtouch.on_begin = nil
  end
  display.viewport:remove_child(sprite)
end
