local image = Image()

function begin_test()
  set_resolution(640, 480)
  
  local sz =
[[print_log() test
================
Tests the "print_log" function used by this test suite.
This text should be aligned with the left edge of the window.
Press the right arrow key for the next test.
]]
  print_log(sz, -320, 0)
end

function end_test()
  clear_log()
end
