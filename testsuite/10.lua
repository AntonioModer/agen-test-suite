local sprite = Sprite()
local font = Font()
  
local buttons =
{
  JBUTTON_1, JBUTTON_2, JBUTTON_3, JBUTTON_4,
  JBUTTON_5, JBUTTON_6, JBUTTON_7, JBUTTON_8,
  JBUTTON_9, JBUTTON_10, JBUTTON_11, JBUTTON_12,
  JBUTTON_13, JBUTTON_14, JBUTTON_15, JBUTTON_16 
}

local untested = {}

local function draw_joystick(index, x, y)
  local j = joysticks[index]
  
  local c = sprite.canvas
  
  c:set_font(font, WHITE)
  c:move_to(x - 60, y + 40)
  local device = "none"
  if j then
    device = j.name or device
  end
  local sz = string.format("Device %d:%s", index, device)
  c:write(sz)
  
  if j == nil then
    return
  end
  
  c:set_fill_style(WHITE, 1)
  
  c:move_to(x, y)
  c:circle(30)
  c:stroke()
  c:rel_move_to(30*j.xaxis, 30*j.yaxis)
  c:circle(5)
  c:fill()
  c:move_to(x, y)
  c:rel_move_to(-60, -50)
  local sz = string.format("%f, %f", j.xaxis, j.yaxis)
  c:write(sz)
  
  c:move_to(x + 330, y)
  c:circle(30)
  c:stroke()
  c:rel_move_to(30*j.zaxis, 0)
  c:circle(5)
  c:fill()
  c:move_to(x + 330, y)
  c:rel_move_to(-60, -50)
  local sz = string.format("%f, %f", j.zaxis, 0)
  c:write(sz)

  c:move_to(x + 420, y)
  c:circle(30)
  c:stroke()
  c:rel_move_to(-15, -50)
  c:write("POV")
  c:set_line_style(1, WHITE, 1)
  if j:is_down(JDPAD_NORTH) then
    c:move_to(x + 420, y)
    c:rel_line_to(0, 30)
    c:stroke()
  end
  if j:is_down(JDPAD_EAST) then
    c:move_to(x + 420, y)
    c:rel_line_to(30, 0)
    c:stroke()
  end
  if j:is_down(JDPAD_SOUTH) then
    c:move_to(x + 420, y)
    c:rel_line_to(0, -30)
    c:stroke()
  end
  if j:is_down(JDPAD_WEST) then
    c:move_to(x + 420, y)
    c:rel_line_to(-30, 0)
    c:stroke()
  end

  local br = 15
  for i, v in ipairs(buttons) do
    local bx = (i - 1)%8 * br*2 + 60
    local by = math.floor((i - 1)/8) * -br*2 + br
    c:move_to(x + bx, y + by)
    c:circle(br - 1)
    local col = WHITE
    if untested[j][v] == true then
      col = GRAY
    end
    if j:is_down(v) then
      untested[j][v] = false
      col = RED
    end
    c:set_fill_style(col)
    c:fill()
    c:move_to(x + bx, y + by)
    c:rel_move_to(-br/4, -br/2)
    c:set_font(font, BLACK)
    c:write(i)
  end
end

function begin_test()
  set_resolution(640, 640)
  
  font:load_system("Arial", 10)

  display.viewport:add_child(sprite)
  
  for i, joystick in ipairs(joysticks) do
    untested[joystick] = {}
    for j, button in ipairs(buttons) do
      untested[joystick][button] = true
    end
  end
  
  local sz =
[[joystick is_down() and axis test
================================
Press a button to confirm that it is detected
This test should work with multiple joysticks connected at the same time]]
  print_log(sz, -320, 320 - 10)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:add_child(sprite)
  font:unload()
  clear_log()
end

function update_test()
  sprite.canvas:clear()
  for i = 1, 5 do
    draw_joystick(i, -200, 200 - (i - 1)*115)
  end
end