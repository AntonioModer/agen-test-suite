local buffer = {}
local sprite = Sprite()

local keys = {}
for i, v in pairs(_G) do
  if string.sub(i, 1, 4) == "KEY_" then
    if type(v) == "number" then
      keys[v] = i
    end
  end
end

local function rfind(i, type, key)
  for j = i, 1, -1 do
    if buffer[j][2] == key and buffer[j][1] == type then
      return j
    end
  end
end

local function key_event(type, key)
  clear_log()
  local sz =
[[keyboard on_press() and on_release() test
=========================================
Checks the order of keyboard events.
Press a series of keys to begin the test.
Watch out for warnings and error messages (RED)]]
  print_log(sz, -320, 320 - 10)
  
  table.insert(buffer, { type, key, nil })
  
  -- find pair
  if #buffer > 1 then
    if type == "release" then
      local j = rfind(#buffer - 1, "press", key)
      if j then
        -- pair with last press
        if buffer[j][3] == nil then
          buffer[j][3] = #buffer
          buffer[#buffer][3] = j
        end
      end
    end
  end
  
  local l = 20

  local min = math.max(1, #buffer - 30)
  for i = #buffer, min, -1 do
    local x, y = 120, 320 - (i - min + 1)*l
    local szkey = keys[buffer[i][2]] or "INVALID KEY"
    local sz = string.format("%s (%s)", szkey, buffer[i][1])
    --print_log(buffer[i][2], x, y)
    print_log(sz, x, y)
  end
  

  local c = sprite.canvas
  c:clear()
  for i = #buffer, min, -1 do
    local color
    local x, y = 100, 320 - (i - min + 1)*l
    if buffer[i][3] then
      if buffer[i][1] == "release" then
        color = BLUE
        local j = buffer[i][3]
        if j then
          local x1, y1 = x - 10, 320 - (i - min + 1)*l
          local x2, y2 = x1, 320 - (j - min + 1)*l
          local mx = x1 - math.abs(y2 - y)/2
          local my = (y + y2)/2
          c:move_to(x1, y1)
          c:curve_to(x2, y2, mx, my)
          c:set_line_style(6, BLACK, 1)
          c:stroke_preserve()
          c:set_line_style(1, WHITE, 1)
          c:stroke()
        end
      elseif buffer[i][1] == "press" then
        color = LIME
      else
        color = RED
      end
    else
      color = RED
    end
    if buffer[i][2] == KEY_NONE then
      color = RED
    end
    c:move_to(x, y)
    c:square(15)
    c:set_fill_style(color)
    c:fill()
  end
end

function begin_test()
  set_resolution(640, 640)

  display.viewport:add_child(sprite)
  
  key_event('started test', 0)
  
  keyboard.on_press = function(k, key)
    key_event('press', key)
  end
  keyboard.on_release = function(k, key)
    key_event('release', key)
  end
end

function end_test()
  buffer = {}
  keyboard.on_press = nil
  keyboard.on_release = nil
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  clear_log()
end
