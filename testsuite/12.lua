local buffer = {}
local sprite = Sprite()
local font = Font()

local buttons = {}
for i, v in pairs(_G) do
  local prefix8 = string.sub(i, 1, 8)
  local prefix6 = string.sub(i, 1, 6)
  if prefix8 == "JBUTTON_" or prefix6 == "JDPAD_" then
    if type(v) == "number" then
      buttons[v] = i
    end
  end
end

local function rfind(i, type, button, jid)
  for j = i, 1, -1 do
    if buffer[j][3] == jid and buffer[j][2] == button and buffer[j][1] == type then
      return j
    end
  end
end

local function button_event(type, button, jid)
  clear_log()
  local sz =
[[joystick on_press() and on_release() test
=========================================
Checks the order of joystick events.
Press a series of buttons to begin the test.
Watch out for warnings and errors in RED.
This test should work with multiple controllers.]]
  print_log(sz, -320, 320 - 10)
  
  table.insert(buffer, { type, button, jid, nil })
  
  -- find pair
  if #buffer > 1 then
    if type == "release" then
      local j = rfind(#buffer - 1, "press", button, jid)
      if j then
        -- pair with last press
        if buffer[j][4] == nil then
          buffer[j][4] = #buffer
          buffer[#buffer][4] = j
        end
      end
    end
  end
  
  local l = 20

  local min = math.max(1, #buffer - 30)
  for i = #buffer, min, -1 do
    local x, y = 80, 320 - (i - min + 1)*l
    local szkey = buttons[buffer[i][2]] or "INVALID BUTTON"
    local sz = string.format("%s (%s)", szkey, buffer[i][1])
    --print_log(buffer[i][2], x, y)
    print_log(sz, x, y)
  end

  local c = sprite.canvas
  c:clear()
  for i = #buffer, min, -1 do
    local x, y = 60, 320 - (i - min + 1)*l
    local color
    if buffer[i][4] then
      if buffer[i][1] == "release" then
        color = BLUE
        local j = buffer[i][4]
        if j then
          local x1, y1 = x - 10, y
          local x2, y2 = x1, 320 - (j - min + 1)*l
          local mx = x1 - math.abs(y2 - y1)/2
          local my = (y1 + y2)/2
          c:move_to(x1, y1)
          c:curve_to(x2, y2, mx, my)
          c:set_line_style(6, BLACK, 1)
          c:stroke_preserve()
          c:set_line_style(1, WHITE, 1)
          c:stroke()
        end
      elseif buffer[i][1] == "press" then
        color = LIME
      else
        color = RED
      end
    else
      color = RED
    end
    if buffer[i][2] == BUTTON_NONE then
      color = RED
    end
    c:move_to(x, y)
    c:square(15)
    c:set_fill_style(color)
    c:fill()
    c:move_to(x - 4, y - 4)
    c:set_font(font, BLACK)
    c:write(buffer[i][3])
  end
end

function begin_test()
  set_resolution(640, 640)

  font:load_system("Arial", 10)
  display.viewport:add_child(sprite)
  
  button_event('started test', 0, 0)
  
  if joystick == nil then
    return
  end
  
  for i, v in pairs(joysticks) do
    v.on_press = function(j, button)
      button_event('press', button, i)
    end
    v.on_release = function(j, button)
      button_event('release', button, i)
    end
  end
end

function end_test()
  buffer = {}
  if joystick then
    for i, v in pairs(joysticks) do
      v.on_press = nil
      v.on_release = nil
    end
  end
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  font:unload()
  clear_log()
end
