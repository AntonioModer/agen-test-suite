local sprite = Sprite()

function begin_test()
  set_resolution(640, 640)

  display.viewport:add_child(sprite)

  local duplicates = 0
  local colors = {}
  local r, g, b = Color.get_red, Color.get_green, Color.get_blue
  
  local colors = { 'ALICEBLUE', 'ANTIQUEWHITE', 'AQUA', 'AQUAMARINE', 
  'AZURE', 'BEIGE', 'BISQUE', 'BLACK', 'BLANCHEDALMOND', 
  'BLUE', 'BLUEVIOLET', 'BROWN', 'BURLYWOOD', 'CADETBLUE',
  'CHARTREUSE', 'CHOCOLATE', 'CORAL', 'CORNFLOWERBLUE',
  'CORNSILK', 'CRIMSON', 'CYAN', 'DARKBLUE',
  'DARKCYAN', 'DARKGOLDENROD', 'DARKGRAY', 'DARKGREEN',
  'DARKKHAKI', 'DARKMAGENTA', 'DARKOLIVEGREEN', 'DARKORANGE',
  'DARKORCHID', 'DARKRED', 'DARKSALMON', 'DARKSEAGREEN',
  'DARKSLATEBLUE', 'DARKSLATEGREY', 'DARKTURQUOISE', 'DARKVIOLET',
  'DEEPPINK', 'DEEPSKYBLUE', 'DIMGRAY', 'DODGERBLUE',
  'FIREBRICK', 'FLORALWHITE', 'FORESTGREEN', 'FUCHSIA',
  'GAINSBORO', 'GHOSTWHITE', 'GOLD', 'GOLDENROD',
  'GOLDENRODYELLOW', 'GRAY', 'GREEN', 'GREENYELLOW',
  'HONEYDEW', 'HOTPINK', 'INDIANRED', 'INDIGO',
  'IVORY', 'KHAKI', 'LAVENDER', 'LAVENDERBLUSH',
  'LAWNGREEN', 'LEMONCHIFFON', 'LIGHTBLUE', 'LIGHTCORAL',
  'LIGHTCYAN', 'LIGHTGREEN', 'LIGHTGREY', 'LIGHTPINK',
  'LIGHTSALMON', 'LIGHTSEAGREEN', 'LIGHTSKYBLUE', 'LIGHTSLATEGRAY',
  'LIGHTSTEELBLUE', 'LIGHTYELLOW', 'LIME', 'LIMEGREEN',
  'LINEN', 'MAGENTA', 'MAROON', 'MEDIUMAQUAMARINE',
  'MEDIUMBLUE', 'MEDIUMORCHID', 'MEDIUMPURPLE', 'MEDIUMSEAGREEN',
  'MEDIUMSLATEBLUE', 'MEDIUMSPRINGGREEN', 'MEDIUMTURQUOISE', 'MEDIUMVIOLETRED',
  'MIDNIGHTBLUE', 'MINTCREAM', 'MISTYROSE', 'MOCCASIN',
  'NAVAJOWHITE', 'NAVY', 'OLDLACE', 'OLIVE',
  'OLIVEDRAB', 'ORANGE', 'ORANGERED', 'ORCHID',
  'PALEGOLDENROD', 'PALEGREEN', 'PALETURQUOISE', 'PALEVIOLETRED',
  'PAPAYAWHIP', 'PEACHPUFF', 'PERU', 'PINK',
  'PLUM', 'POWDERBLUE', 'PURPLE', 'RED',
  'ROSYBROWN', 'ROYALBLUE', 'SADDLEBROWN', 'SALMON',
  'SANDYBROWN', 'SEAGREEN', 'SEASHELL', 'SIENNA',
  'SILVER', 'SKYBLUE', 'SLATEBLUE', 'SLATEGRAY',
  'SNOW', 'SPRINGGREEN', 'STEELBLUE', 'TAN',
  'TEAL', 'THISTLE', 'TOMATO', 'TURQUOISE',
  'VIOLET', 'WHEAT', 'WHITE', 'WHITESMOKE',
  'YELLOW', 'YELLOWGREEN' }

  table.sort(colors)
  
  local missing = 0
  for i, v in ipairs(colors) do
    local y = (i - 1)%40 *-11 + 200
    local x = math.floor((i - 1)/40) *160 - 310
    sprite.canvas:move_to(x, y)
    sprite.canvas:square(10)
    if _G[v] and _G[v].red and _G[v].green and _G[v].blue then
      sprite.canvas:set_fill_style(_G[v])
      sprite.canvas:fill()
    else
      local c = Color(255, 0, 0)
      sprite.canvas:set_line_style(1, c)
      sprite.canvas:stroke()
      missing = missing + 1
    end
    print_log(v, x + 5, y - 2)
  end
  
  local sz =
[[Color viewer
============
Draws all colors listed in the documentation.
Colors found: %d/140(according to manual)
Colors missing: %d]]
  sz = string.format(sz, #colors, duplicates, missing)
  print_log(sz, -320, 320 - 20)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  clear_log()
end
