function begin_test()
  set_resolution(640, 480)

  if lfs == nil then
    require('lfs')
  end
  local dir = lfs.currentdir()
  local file = string.format("%s/settings.ini", dir)
  
  local f = io.open(file)
  local out
  if f then
    out = f:read("*a")
    f:close()
  else
    out = "Warning: Could not open settings.ini!\nThe engine will run using its default plugins"
  end
  
  local sz =
[[Settings.ini viewer
===================

%s]]
  sz = string.format(sz, out)
  print_log(sz, -320, 240 - 10)
end

function end_test()
  clear_log()
end
