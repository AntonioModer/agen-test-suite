local function scan_dir(path, rec, res)
  local res = res or {}
  for file in lfs.dir(path) do
    if file ~= '.' and file ~= '..' then
      local full = string.format("%s/%s", path, file)
      local attr = lfs.attributes(full)
      if attr.mode == 'directory' then
        if rec == true then
          scan_dir(full, rec, res)
        end
      else
        table.insert(res, { file, attr.size, attr.modification, full })
      end
    end
  end
  return res
end

function begin_test()
  set_resolution(800, 640)

  if lfs == nil then
    require('lfs')
  end
  local cur = lfs.currentdir()
  
  local out = {}
  
  table.insert(out, "\nBinaries:")
  table.insert(out, "---------")
  local oldest = nil
  local oldest_date = nil
  local bins = { "Agenoria.exe", "Framework.dll", "lua51f.dll", "tolua++.dll",  }
  for i, v in ipairs(bins) do
    local full = string.format("%s/%s", cur, v)
    local attr = lfs.attributes(full)
    if attr then
      local kb = math.floor(attr.size/1024)
      local mod = os.date("%c", attr.modification)
      local o = string.format("%s - %s Kb (%s)", v, kb, mod)
      table.insert(out, o)
      if oldest_date == nil or oldest_date > attr.modification then
        oldest = v
        oldest_date = attr.modification
      end
    end
  end


  local plugs = scan_dir(cur .. "/Plugins")
  plugs = scan_dir(cur .. "/Contents/PlugIns", false, plugs)
  table.insert(out, "\nPlugins:")
  table.insert(out, "--------")
  for i, v in ipairs(plugs) do
    local kb = math.floor(v[2]/1024)
    local mod = os.date("%c", v[3])
    local status = ""
    if v[2] == 0 then
      status = "(Warning: zero filesize)"
    elseif v[3] < oldest_date then
      status = string.format("(Warning: older than '%s')", oldest)
    end
    local o = string.format("%s - %s Kb (%s) %s", v[1], kb, mod, status)
    table.insert(out, o)
  end
  
  local exts = scan_dir(cur .. "/Extensions/dll", true)
  exts = scan_dir(cur .. "/Contents/Frameworks/Lua.framework/Libraries/", true, exts)
  table.insert(out, "\nExtensions:")
  table.insert(out, "-----------")
  for i, v in ipairs(exts) do
    local kb = math.floor(v[2]/1024)
    local mod = os.date("%c", v[3])
    local status = ""
    if v[2] == 0 then
      status = "(Warning: zero filesize)"
    elseif v[3] < oldest_date then
      status = string.format("\n(Warning: '%s' is older than '%s')", v[1], oldest)
    end
    local o = string.format("%s - %s Kb (%s) %s", v[4], kb, mod, status)
    table.insert(out, o)
  end
  
  local sz =
[[Binary files check
==================
Checks if any plugins or extensions are older than the oldest binary
%s]]
  sz = string.format(sz, table.concat(out, "\n"))
  print_log(sz, -400, 320 - 10)
end

function end_test()
  clear_log()
end
