
function begin_test()
  set_resolution(640, 640)

  if b2World == nil then
    require('Box2D')
  end
 
  local b2_pi = math.pi
  local vars = 
  {
    { "b2_maxManifoldPoints", 2 },
    { "b2_maxPolygonVertices", 8 },
    { "b2_maxProxies", 512 },
    { "b2_maxPairs", 8 * 512 }, 
    { "b2_linearSlop", 0.005 },
    { "b2_angularSlop", 2.0 / 180.0 * b2_pi },
    { "b2_toiSlop", 8.0 * 0.005 },
    { "b2_maxTOIContactsPerIsland", 32 },
    { "b2_velocityThreshold", 1.0 },
    { "b2_maxLinearCorrection", 0.2 },
    { "b2_maxAngularCorrection", 8.0 / 180.0 * b2_pi },
    { "b2_maxLinearVelocity", 200.0 },
    { "b2_maxLinearVelocitySquared", 200.0 * 200.0 },
    { "b2_maxAngularVelocity", 250.0 },
    { "b2_maxAngularVelocitySquared", 250.0 * 250.0 },
    { "b2_contactBaumgarte", 0.2 },
    { "b2_timeToSleep", 0.5 },
    { "b2_linearSleepTolerance", 0.01 },
    { "b2_angularSleepTolerance", 2.0 / 180.0 }
  }
  --sz = sz .. "b2_version = " .. b2_version.major .. "." .. b2_version.minor .. "." .. b2_version.revision .. "\n"

  for i, v in ipairs(vars) do
    local stat = ""
    local user = _G[v[1]] or "nil"
    local default = v[2] or "nil"
    if user == "nil" then
      stat = "(Warning: value is nil)"
    elseif default == "nil" then
      stat = "(Warning: unknown default value)"
    elseif user ~= default then
      stat = string.format("(Warning: default is %f)", default)
    end
    vars[i] = string.format("%s = %s %s", v[1], user, stat)
  end
  
  local sz =
[[Box2D Constants viewer
======================
Comparions versus the default values in Box2D 2.0.1

%s]]
  sz = string.format(sz, table.concat(vars, "\n"))
  print_log(sz, -320, 320 - 10)
end

function end_test()
  clear_log()
end
