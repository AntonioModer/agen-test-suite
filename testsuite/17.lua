local sprite = Sprite()
local sprite2 = Sprite()
sprite2.depth = -1
local ns = {}
local nsk = {}
local g = {}
local gsz = {}
local font = Font()
local rows = 46

local function get_index(x, y)
  local x, y = x + 512, 300 - y - 50
  local cx = math.floor(x/160)
  local cy = math.floor(y/11)
  if cx < 0 or cy < 1 or cy > rows then
    return
  end
  local i = cx*rows + cy
  if i <= 0 or i > #ns then
    return
  end
  return i, cx, cy
end

local function get_position(c, r)
  return c*160 - 512, r*-11 + 300 - 50
end

local function select_namespace(lg)
  ns = {}
  nsk = {}
  for i, v in pairs(lg) do
    if type(i) == "string" or type(i) == "number" then
      table.insert(ns, i)
    else
      local k = tostring(i) or "?"
      table.insert(ns, k)
      nsk[k] = i
    end
  end
  table.sort(ns)
  if lg ~= _G then
    table.insert(ns, 1, "..")
  end
  
  clear_log()
  sprite.canvas:clear()
  
  local vars = 0
  local numbers = 0
  local strings = 0
  local tables = 0
  local userdatas = 0
  local functions = 0
  local nils = 0
  local unknown = 0
  for i, v in ipairs(ns) do
    local y = (i - 1)%rows
    local x = math.floor((i - 1)/rows)
    x, y = get_position(x, y + 1)
    
    sprite.canvas:move_to(x + 4, y - 4)
    sprite.canvas:square(8)
    local color = BLACK
    local k = v
    if nsk[v] then
      k = nsk[v]
    end
    local obj = lg[k]
    local t = type(obj)
    if v ~= ".." then
      vars = vars + 1
      if t == "number" then
        numbers = numbers + 1
        color = GRAY
      elseif t == "string" then
        strings = strings + 1
        color = SILVER
      elseif t == "function" then
        functions = functions + 1
        color = WHITE
      elseif t == "table" then
        tables = tables + 1
        color = ORANGE
      elseif t == "userdata" then
        userdatas = userdatas + 1
        color = YELLOW
      elseif t == "nil" then
        nils = nils + 1
      else
        unknown = unknown + 1
      end
    end
    sprite.canvas:set_fill_style(color)
    sprite.canvas:fill()
    if t == "table" then
      sprite.canvas:square(6)
      sprite.canvas:set_fill_style(BLACK)
      sprite.canvas:fill()
    end
    local out = v
    if string.len(out) > 18 then
      out = string.sub(out, 1, 18)
    end
    sprite.canvas:move_to(x + 8, y - 8)
    sprite.canvas:set_font(font)
    sprite.canvas:write(out)
  end
  
  local sz =
[[Lua environment explorer
========================
Version: %s

Namespace: %s
%d Values: %d numbers/%d strings/%d tables/%d userdata/%d functions/%d nil/%d unknown:]]
  local nsdir = table.concat(gsz, "/")
  sz = string.format(sz, _VERSION, nsdir, vars, numbers, strings, tables, userdatas, functions, nils, unknown)
  print_log(sz, -512, 300 - 10)
end

local function exit_namespace()
  assert(#g > 0)
  assert(g[#g] ~= g[#g - 1])
  table.remove(g)
  table.remove(gsz)
  select_namespace(g[#g])
end

-- accepts a string index from "gs"
local function enter_namespace(k)
  if k == ".." then
    exit_namespace()
    return
  end
  if nsk[k] then
    k = nsk[k]
  end
  local lg = g[#g][k]
  if type(lg) ~= "table" then
    return
  end
  if lg == g[#g] then
    return
  end
  table.insert(g, lg)
  table.insert(gsz, k)
  select_namespace(lg)
end

local function highlight(mx, my)
  local i, cx, cy = get_index(mx, my)
  if i == nil then
    sprite2.visible = false
    return
  end
  sprite2.visible = true
  
  local x, y = get_position(cx, cy)
  sprite2:set_position(x, y)
  local c = sprite2.canvas
  c:clear()
  c:move_to(80, -4)
  c:rectangle(160, 8)
  local color, alpha = WHITE, 0.5
  local sz
  if ns[i] ~= ".." then
    local k = ns[i]
    if nsk[k] then
      k = nsk[k]
    end
    local obj = g[#g][k]
    local t = type(obj)
    if t == "string" then
      sz = obj
      color = SILVER
      alpha = 1
    elseif t == "number" then
      sz = tostring(obj)
      color = GRAY
      alpha = 1
    elseif t == "function" then
      sz = tostring(obj)
      color = WHITE
      alpha = 1
    elseif t == "nil" then
      sz = "nil"
      alpha = 1
    elseif t == "table" then
      sz = tostring(obj)
      color = ORANGE
      alpha = 1
    elseif t == "userdata" then
      sz = tostring(obj)
      color = YELLOW
      alpha = 1
    end
  end
  c:set_fill_style(color, alpha)
  c:fill()
  if sz then
    c:set_font(font, BLACK, 1)
    c:move_to(8, -8)
    c:write(sz)
  end
end

function begin_test()
  set_resolution(1024, 600)
  
  local dir = get_suitedir()
  --font:load_system("Courier", 10)
  local sz = string.format("%s/apple.ttf", dir)
  font:load_file(sz, 6)
  display.viewport:add_child(sprite)
  

  display.viewport:add_child(sprite)
  display.viewport:add_child(sprite2)
  
  mouse.on_press = function(mouse, b)
    local i = get_index(mouse.xaxis, mouse.yaxis)
    if i then
      enter_namespace(ns[i])
      highlight(mouse.xaxis, mouse.yaxis)
    end
  end
  mouse.on_move = function(mouse, dx, dy)
    highlight(mouse.xaxis, mouse.yaxis)
  end

  g = { _G }
  gsz = { "_G" }
  select_namespace(_G)
end

function end_test()
  mouse.on_press = nil
  mouse.on_move = nil
  font:unload()
  
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  sprite2.canvas:clear()
  display.viewport:remove_child(sprite2)
  clear_log()
end
