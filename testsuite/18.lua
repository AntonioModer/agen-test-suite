local image = Image()
local sprite = Sprite()

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local sz = string.format("%s/128x128circle.png", dir)
  image:load(sz)
  
  sprite.canvas:set_source_image(image, 0, -128)
  sprite.canvas:paint()
  sprite.canvas:move_to(0, 128)
  sprite.canvas:circle(64)
  sprite.canvas:fill()
  display.viewport:add_child(sprite)

  local sz =
[[vector circle vs pre-rendered
=============================
Opaque white circle with radius 64px (128px diamater)
Top: drawn using "circle" and "fill"
Bottom: drawn using "set_source_image" and "paint"]]
  print_log(sz, -256, 20)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  image:unload()
  clear_log()
end
