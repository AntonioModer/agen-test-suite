local image = Image()
local image2 = Image()
local sprite = Sprite()
local sprite2 = Sprite()
local sprite3 = Sprite()
local sprite4 = Sprite()
local sprite5 = Sprite()
local sprite6 = Sprite()

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local sz = string.format("%s/256x256circles.png", dir)
  image:load(sz)
  local sz = string.format("%s/128x128circle.png", dir)
  image2:load(sz)
  
  sprite.canvas:set_source_image(image, -128, -128 - 32 - 8)
  sprite.canvas:paint()
  display.viewport:add_child(sprite)
  
  sprite2.alpha = 0.5
  sprite2.canvas:set_source_image(image2, 128, -64 - 64)
  sprite2.canvas:paint()
  sprite2.canvas:set_source_image(image2, 96, -128 - 64)
  sprite2.canvas:paint()
  sprite2.canvas:set_source_image(image2, 160, -128 - 64)
  sprite2.canvas:paint()
  display.viewport:add_child(sprite2)
  
  sprite3.canvas:set_fill_style(WHITE, 0.5)
  sprite3.canvas:move_to(-128 + 32, 128)
  sprite3.canvas:circle(64)
  sprite3.canvas:fill()
  sprite3.canvas:move_to(-128 - 32, 128)
  sprite3.canvas:circle(64)
  sprite3.canvas:fill()
  sprite3.canvas:move_to(-128, 128 + 64)
  sprite3.canvas:circle(64)
  sprite3.canvas:fill()
  display.viewport:add_child(sprite3)
  
  sprite4.canvas:move_to(128 - 32, 128)
  sprite4.canvas:circle(64)
  sprite4.canvas:fill()
  sprite4.alpha = 0.5
  display.viewport:add_child(sprite4)
  sprite5.canvas:move_to(128 + 32, 128)
  sprite5.canvas:circle(64)
  sprite5.canvas:fill()
  sprite5.alpha = 0.5
  display.viewport:add_child(sprite5)
  sprite6.canvas:move_to(128, 128 + 64)
  sprite6.canvas:circle(64)
  sprite6.canvas:fill()
  sprite6.alpha = 0.5
  display.viewport:add_child(sprite6)
  

  local sz =
[[vector vs pre-rendered alpha blending
=====================================
All four are supposed to look identical.
Top left: 1 sprite with 3 circles "filled" at 0.5
Top right: 3 sprites at 0.5 ".alpha" with circles "filled" at 1
Bottom left: sigle image
Bottom right: 3 images on 1 sprite with 0.5 ".alpha"]]
  print_log(sz, -256, 40)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  sprite2.canvas:clear()
  display.viewport:remove_child(sprite2)
  sprite3.canvas:clear()
  display.viewport:remove_child(sprite3)
  sprite4.canvas:clear()
  display.viewport:remove_child(sprite4)
  sprite5.canvas:clear()
  display.viewport:remove_child(sprite5)
  sprite6.canvas:clear()
  display.viewport:remove_child(sprite6)
  image:unload()
  image2:unload()
  clear_log()
end