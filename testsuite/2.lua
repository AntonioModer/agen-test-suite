local image = Image()
local sprite = Sprite()

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local sz = string.format("%s/512x512squares.png", dir)
  image:load(sz)
  
  sprite.canvas:set_source_image(image)
  sprite.canvas:paint()
  display.viewport:add_child(sprite)

  local sz =
[[paint() test
============
Paints a static non-transparent .png file.
Can you fully see all 8 red corner squares?
Can you see all 4 red lines along the window edges?]]
  print_log(sz, -256, 0)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  image:unload()
  clear_log()
end
