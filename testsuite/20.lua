local sprite = Sprite()
local font = Font()

local keys =
{
  "KEY_NONE",
  "KEY_ESCAPE",
  "KEY_F1",
  "KEY_F2",
  "KEY_F3",
  "KEY_F4",
  "KEY_F5",
  "KEY_F6",
  "KEY_F7",
  "KEY_F8",
  "KEY_F9",
  "KEY_F10",
  "KEY_F11",
  "KEY_F12",
  "KEY_F13",
  "KEY_F14",
  "KEY_F15",
  "KEY_1",
  "KEY_2",
  "KEY_3",
  "KEY_4",
  "KEY_5",
  "KEY_6",
  "KEY_7",
  "KEY_8",
  "KEY_9",
  "KEY_0",
  "KEY_MINUS",
  "KEY_EQUALS",
  "KEY_BACK",
  "KEY_TAB",
  "KEY_Q",
  "KEY_W",
  "KEY_E",
  "KEY_R",
  "KEY_T",
  "KEY_Y",
  "KEY_U",
  "KEY_I",
  "KEY_O",
  "KEY_P",
  "KEY_LSQB",
  "KEY_RSQB",
  "KEY_BSLASH",
  "KEY_CAPS",
  "KEY_A",
  "KEY_S",
  "KEY_D",
  "KEY_F",
  "KEY_G",
  "KEY_H",
  "KEY_J",
  "KEY_K",
  "KEY_L",
  "KEY_COLON",
  "KEY_APOS",
  "KEY_ENTER",
  "KEY_LSHIFT",
  "KEY_Z",
  "KEY_X",
  "KEY_C",
  "KEY_V",
  "KEY_B",
  "KEY_N",
  "KEY_M",
  "KEY_COMMA",
  "KEY_DOT",
  "KEY_SLASH",
  "KEY_RSHIFT",
  "KEY_LCTRL",
  "KEY_LWIN",
  "KEY_LALT",
  "KEY_SPACE",
  "KEY_RALT",
  "KEY_RWIN",
  "KEY_RMENU",
  "KEY_RCTRL",
  "KEY_PSCREEN",
  "KEY_SLOCK",
  "KEY_PAUSE",
  "KEY_INS",
  "KEY_HOME",
  "KEY_PGUP",
  "KEY_DEL",
  "KEY_END",
  "KEY_PGDOWN",
  "KEY_UP",
  "KEY_LEFT",
  "KEY_DOWN",
  "KEY_RIGHT",
  "KEY_NUMLOCK",
  "KEY_NUMDIV",
  "KEY_NUMMUL",
  "KEY_NUMMINUS",
  "KEY_NUM_1",
  "KEY_NUM_2",
  "KEY_NUM_3",
  "KEY_NUM_4",
  "KEY_NUM_5",
  "KEY_NUM_6",
  "KEY_NUM_7",
  "KEY_NUM_8",
  "KEY_NUM_9",
  "KEY_NUM_0",
  "KEY_NUMDEL",
  "KEY_NUMPLUS",
  "KEY_NUMENTER",
  "KEY_TILDE"
}

local presses = {}
local releases = {}

function begin_test()
  set_resolution(800, 600)
  
  font:load_system("Courier", 10)
  display.viewport:add_child(sprite)

  for i, v in ipairs(keys) do
    presses[i] = false
    releases[i] = false
  end

  local errors = {}
  if keyboard.name == nil then
    table.insert(errors, "No device name!")
  end
  for i, v in ipairs(keys) do
    local v = keys[i]
    if i - 1 ~= _G[v] then
      local sz = string.format("%s ~= %d", v, i - 1)
      table.insert(errors, sz)
    end
  end
  -- scan for new/missed keys!
  for i, v in pairs(_G) do
    local prefix8 = string.sub(i, 1, 8)
    local prefix6 = string.sub(i, 1, 6)
    if prefix8 == "KEY_" then
      local exists = false
      for j, k in ipairs(keys) do
        if i == k then
          exists = true
          break
        end
      end
      if exists == false then
        table.insert(errors, "Unknown: " .. i)
      end
    end
  end
  
  local errsz = ""
  if #errors > 0 then
    errsz = table.concat(errors, "\n")
  end
  
  local dev = "none"
  if keyboard then
    dev = keyboard.name or dev
  end
  
  local sz =
[[keyboard key id verifier
========================
Press a key to check if the proper "on_press" and "on_release" are called
Device: %s
Number of key ids: %d
Incorrect or missing: %d
Warnings and errors:
%s]]
  sz = string.format(sz, dev, #keys, #errors, errsz)
  print_log(sz, -400, 300 - 10)
  
  if keyboard then
    keyboard.on_press = function(keyboard, key)
      presses[key + 1] = true
    end
    keyboard.on_release = function(keyboard, key)
      releases[key + 1] = true
    end
  end
end

function end_test()
  sprite.canvas:clear()
  font:unload()
  display.viewport:remove_child(sprite)
  clear_log()
  if keyboard then
    keyboard.on_press = nil
    keyboard.on_release = nil
  end
end

function update_test(dt)
  local x = -200
  local y = 200

  local c = sprite.canvas
  c:clear()
  for i = 1, #keys do
    local row = (i - 1)%32
    local column = math.floor((i - 1)/32)
    local lx, ly = x + column * (600/4), y - row * 15
    c:move_to(lx, ly)
    c:set_font(font, GRAY)
    local sz = string.format("%d.%s", i - 1, keys[i])
    c:write(sz)
    
    c:move_to(lx, ly)
    c:rel_move_to(-5, 0)
    c:rel_line_to(-10, 0)
    c:rel_line_to(5, 10)
    c:rel_line_to(5, -10)
    c:set_fill_style(RED, 0.5)
    if releases[i] == true then
      c:set_fill_style(LIME, 0.5)
    end
    c:fill()
    
    c:move_to(lx, ly)
    c:set_fill_style(RED, 0.5)
    if presses[i] == true then
      local a = 0.5
      if keyboard:is_down(i - 1) then
        a = 1
      end
      c:set_fill_style(LIME, a)
    end
    c:rel_move_to(-20, 10)
    c:rel_line_to(-10, 0)
    c:rel_line_to(5, -10)
    c:rel_line_to(5, 10)
    c:fill()
  end
end