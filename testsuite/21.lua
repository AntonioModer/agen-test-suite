local sprite = Sprite()
local font = Font()
local button1id = JBUTTON_1 or 112

local buttons =
{
  "JBUTTON_1", "JBUTTON_2", "JBUTTON_3", "JBUTTON_4",
  "JBUTTON_5", "JBUTTON_6", "JBUTTON_7", "JBUTTON_8",
  "JBUTTON_9", "JBUTTON_10", "JBUTTON_11", "JBUTTON_12",
  "JBUTTON_13", "JBUTTON_14", "JBUTTON_15", "JBUTTON_16",
  "JDPAD_NORTH", "JDPAD_EAST", "JDPAD_SOUTH", "JDPAD_WEST"
}

local presses = {}
local releases = {}


function begin_test()
  set_resolution(800, 600)
  
  font:load_system("Courier", 10)
  display.viewport:add_child(sprite)

  for i, v in ipairs(buttons) do
    presses[i] = false
    releases[i] = false
  end

  local errors = {}
  for i, v in ipairs(buttons) do
    if i - 1 + button1id ~= _G[v] then
      local sz = string.format("%s ~= %d", v, i - 1)
      table.insert(errors, sz)
    end
  end
  -- scan for new/missed buttons!
  for i, v in pairs(_G) do
    local prefix8 = string.sub(i, 1, 8)
    local prefix6 = string.sub(i, 1, 6)
    if prefix8 == "JBUTTON_" or prefix6 == "JDPAD_" then
      local exists = false
      for j, k in ipairs(buttons) do
        if i == k then
          exists = true
          break
        end
      end
      if exists == false then
        table.insert(errors, "Unknown: " .. i)
      end
    end
  end
  
  local errsz = ""
  if #errors > 0 then
    errsz = table.concat(errors, "\n")
  end
  
  local dev = "none"
  if joystick then
    dev = joystick.name or dev
  end
  
  local sz =
[[joystick button id verifier
===========================
Press a button to check if the proper "on_press" and "on_release" are called
Device: %s
Number of button ids: %d
Incorrect or missing: %d
Warnings and errors:
%s]]
  sz = string.format(sz, dev, #buttons, #errors, errsz)
  print_log(sz, -400, 300 - 10)
  
  if joystick then
    joystick.on_press = function(joystick, button)
      presses[button - button1id + 1] = true
    end
    joystick.on_release = function(joystick, button)
      releases[button - button1id + 1] = true
    end
  end
end

function end_test()
  sprite.canvas:clear()
  font:unload()
  display.viewport:remove_child(sprite)
  clear_log()
  if joystick then
    joystick.on_press = nil
    joystick.on_release = nil
  end
end

function update_test(dt)
  local x = -200
  local y = 200

  local c = sprite.canvas
  c:clear()
  for i = 1, #buttons do
    local row = (i - 1)%32
    local column = math.floor((i - 1)/32)
    local lx, ly = x + column * (600/4), y - row * 15
    c:move_to(lx, ly)
    c:set_font(font, GRAY)
    local sz = string.format("%d.%s", i - 1 + button1id, buttons[i])
    c:write(sz)
    
    c:move_to(lx, ly)
    c:rel_move_to(-5, 0)
    c:rel_line_to(-10, 0)
    c:rel_line_to(5, 10)
    c:rel_line_to(5, -10)
    c:set_fill_style(RED, 0.5)
    if releases[i] == true then
      c:set_fill_style(LIME, 0.5)
    end
    c:fill()
    
    c:move_to(lx, ly)
    c:set_fill_style(RED, 0.5)
    if presses[i] == true then
      local a = 0.5
      if joystick:is_down(i - 1 + button1id) then
        a = 1
      end
      c:set_fill_style(LIME, a)
    end
    c:rel_move_to(-20, 10)
    c:rel_line_to(-10, 0)
    c:rel_line_to(5, -10)
    c:rel_line_to(5, 10)
    c:fill()
  end
end