local dir = get_suitedir()
local calls = 0
local total_calls = 0
local tests = 0
local max_number_power = 31
local max_number = 2^max_number_power
local max_string = 128
local max_calls = 128
local fontfilename = dir .. "/apple.ttf"
local imagefilename = dir .. "/particle.png"
local test_prefix = "\n-- TEST START --\n---------------\n"
local test_postfix = "\n-- TEST END --\n--------------\n"

local code = {}

local function log_chunk(sz)
  file_log("")
  file_log(sz)
  file_log("")
end

local function run_chunk(sz) 
  local chunk, msg = loadstring(sz)
  assert(chunk, msg)
  chunk()
end

local v = { n = "number", i = "integer", img = "image", c = "color", f = "font", s = "string" }
local canvas_api =
{
  { "arc", { v.n, v.n, v.n, v.n } },
  { "circle", { v.n } },
  { "clear", {} },
  { "close_path", {} },
  { "curve_to", { v.n, v.n, v.n, v.n } },
  { "curve_to", { v.n, v.n, v.n, v.n, v.n, v.n } },
  { "ellipse", { v.n, v.n } },
  { "fill", {} },
  { "fill_preserve", {} },
  { "line_to", { v.n, v.n } },
  { "move_to", { v.n, v.n } },
  { "paint", {} },
  { "rectangle", { v.n, v.n } },
  { "rel_line_to", { v.n, v.n } },
  { "rel_move_to", { v.n, v.n } },
  { "set_fill_style", { v.c, v.n } },
  { "set_font", { v.f } },
  { "set_font", { v.f, v.c } },
  { "set_font", { v.f, v.c, v.a } },
  { "set_line_style", { v.n } },
  { "set_line_style", { v.n, v.c } },
  { "set_line_style", { v.n, v.c, v.a } },
  { "set_source_image", { v.img } },
  { "set_source_image", { v.img, v.n, v.n } },
  { "set_source_subimage", { v.img, v.n, v.n, v.n, v.n } },
  { "set_source_subimage", { v.img, v.n, v.n, v.n, v.n, v.n, v.n } },
  { "square", { v.n } },
  { "stroke", {} },
  { "stroke_preserve", {} },
  { "write", { v.s } }
}
local scenenode_api =
{
  { "change_position", { v.n, v.n } },
  { "change_rotation", { v.n } },
  { "change_rotation_r", { v.n } },
  { "change_scale", { v.n, v.n } },
  { "change_scalex", { v.n } },
  { "change_scaley", { v.n } },
  { "get_local_point", { v.n, v.n } },
  { "get_world_point", { v.n, v.n } },
  { "set_position", { v.n, v.n } },
  { "set_rotation", { v.n } },
  { "set_rotation_r", { v.n } },
  { "set_scale", { v.n, v.n } },
}

local function rand_number()
  local a = math.random() - 0.5
  return a*max_number*2
end

local function rand_integer()
  return math.random(-max_number, max_number)
end

local function rand_string()
  local n = math.random(1, max_string)
  local chars = {}
  for i = 1, n do
    local r = math.random(32, 128)
    -- replace " and backslash
    if r == 34 or r == 92 then
      r = 32
    end
    chars[i] = string.char(r)
  end
  local sz1 = table.concat(chars)
  local sz2 = string.format('"%s"', sz1)
  return sz2
end

local function get_args(t)
  local outsz = {}
  for i, v in ipairs(t) do
    local argsz
    if v == "number" then
      argsz = tostring(rand_number())
    elseif v == "integer" then
      argsz = tostring(rand_integer())
    elseif v == "image" then
      argsz = "test_data.image"
    elseif v == "color" then
      argsz = "test_data.color"
    elseif v == "font" then
      argsz = "test_data.font"
    elseif v == "string" then
      argsz = rand_string()
    end
    outsz[i] = argsz
  end
  return table.concat(outsz, ",")
end

local function get_func(objectsz, api)
  local fid = math.random(1, #api)
  local funcname = api[fid][1]
  local argsz = get_args(api[fid][2])
  return string.format("%s:%s(%s)", objectsz, funcname, argsz)
end

local function get_canvas_code(out, font, image)
  table.insert(out, "test_data = {}")
  table.insert(out, "test_data.canvas = Canvas()")
  table.insert(out, "test_data.font = Font()")
  if font then
    local randsize = math.random(5, 30)
    local sz = string.format('test_data.font:load_file("%s", %d)', font, randsize)
    table.insert(out, sz)
  end
  table.insert(out, "test_data.image = Image()")
  if image then
    local sz = string.format('test_data.image:load("%s")', image)
    table.insert(out, sz)
  end
  table.insert(out, "test_data.color = Color()")
  for j = 1, max_calls do
    table.insert(out, get_func("test_data.canvas", canvas_api))
  end
  table.insert(out, "test_data.canvas:clear()")
  return out
end

local function canvas_tests(font, image)
  tests = tests + 1
  calls = 0
  
  code = {}
  table.insert(code, test_prefix)
  code = get_canvas_code(code, font, image)
  table.insert(code, test_postfix)
  
  local codesz = table.concat(code, "\n")
  log_chunk(codesz)
  draw_status()
end

local function get_sprite_code(out)
  table.insert(out, "test_data = {}")
  table.insert(out, "test_data.sprite = Sprite()")
  for j = 1, max_calls do
    table.insert(out, get_func("test_data.sprite", scenenode_api))
  end
  return out
end

local function sprite_tests()
  tests = tests + 1
  calls = 0
  
  code = {}
  table.insert(code, test_prefix)
  code = get_sprite_code(code)
  table.insert(code, test_postfix)
  
  local codesz = table.concat(code, "\n")
  log_chunk(codesz)
  draw_status()
end

local function get_layer_code(out)
  table.insert(out, "test_data = {}")
  table.insert(out, "test_data.layer = Layer()")
  for j = 1, max_calls do
    table.insert(out, get_func("test_data.layer", scenenode_api))
  end
  return out
end

local function layer_tests()
  tests = tests + 1
  calls = 0
  
  code = {}
  table.insert(code, test_prefix)
  code = get_layer_code(code)
  table.insert(code, test_postfix)
  
  local codesz = table.concat(code, "\n")
  log_chunk(codesz)
  draw_status()
end

function draw_status()
  clear_log()
  if #code > 0 then
    local sz =
[[Test in progress!
=================
Test: %d
Calls: %d\%d
Running: %s

See "suite.log" for a dump of the Lua test code
(Press escape to stop test)]]
    sz = string.format(sz, tests, calls, max_calls, code[1])
    print_log(sz, -400, 300 - 10, RED)
    return
  end
  
  local sz =
[[Automated crash test
====================
Executes a series of random function calls
in an attempt to produce a crash on the backend.

Tests stats
-----------
Maxnumber:2^%d = %f
Maxstring size:%d

Tests attempted: %d
Functions called: %d (%d per test)

Press 1-6 key to run the test
-----------------------------
1 = Canvas (with unloaded font&image)
2 = Canvas (with loaded font '%s')
3 = Canvas (with loaded image '%s')
4 = Canvas (with loaded font&image
5 = Sprite
6 = Layer

Up/Down = adjust number of function calls per test]]
  sz = string.format(sz, max_number_power, max_number, max_string, tests, total_calls, max_calls, fontfilename, imagefilename)
  print_log(sz, -400, 300 - 10)
end

function begin_test()
  set_resolution(800, 600)

  draw_status()
  
  keyboard.on_press = function(keyboard, key)
    if #code > 0 then
      if key == KEY_ESCAPE then
        code = {}
      end
      return
    end
    if key == KEY_1 then
      canvas_tests()
    elseif key == KEY_2 then
      canvas_tests(fontfilename)
    elseif key == KEY_3 then
      canvas_tests(nil, imagefilename)
    elseif key == KEY_4 then
      canvas_tests(fontfilename, imagefilename)
    elseif key == KEY_5 then
      sprite_tests()
    elseif key == KEY_6 then
      layer_tests()
    elseif key == KEY_UP then
      max_calls = max_calls*2
    elseif key == KEY_DOWN then
      max_calls = math.max(max_calls/2, 2)
    end
  end
end

function end_test()
  keyboard.on_press = nil
  clear_log()
end

function update_test(sz)
  if #code > 0 then
    local sz = table.remove(code, 1)
    calls = calls + 1
    total_calls = total_calls + 1
    run_chunk(sz)
  end
  draw_status()
end