local sprite = Sprite()
local sound = Sound()
local font = Font()
local players = {}
local dir = get_suitedir()
local file = string.format("%s/acapella.wav", dir)
local file_ms = 5700 -- sample time in ms
local wasdown = false

function begin_test()
  set_resolution(640, 640)
  
  local bits, channels, frequency = 16, 2, 44100
  local mode = audio:set_mode(bits, channels, frequency)
  local warnings = ""
  if mode == false then
    warnings = "(Warning:failed to set mode)\n"
  end

  display.viewport:add_child(sprite)
  sound = Sound()
  if sound:load(file) == false then
    local sz = "%s(Warning:failed to load sound)\n"
    warnings = string.format(sz, warnings)
  end
  
  font:load_system("Courier", 10)

  local sz =
[[Multiple players test
=====================
Test playing multiple instances of a sound (%s)
Audio mode bits:%d channels:%d frequency:%d
%s
Press the space bar several times in a row to overlay sounds]]
  sz = string.format(sz, file, bits, channels, frequency, warnings)
  print_log(sz, -320, 320 - 10)
  
  wasdown = false
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  clear_log()
  font:unload()
  sound:unload()
  while #players > 0 do
    local p = table.remove(players)
    p:stop()
  end
end

local function draw_player(c, x, y, player)
  if player:is_playing() then
    c:move_to(x, y)
    c:rectangle(190, 30)
    c:set_fill_style(RED, 0.4)
    c:fill()
    
    local r = player.time/file_ms
    c:move_to(x - ((1 - r)/2)*190, y)
    c:rectangle(r*190, 30)
    c:set_fill_style(RED, 1)
    c:fill()
  end
  c:move_to(x, y)
  c:rectangle(190, 30)
  c:stroke()
  c:move_to(x - 90, y + 5)
  local vol, pan, pitch = player.volume, player.pan, player.pitch
  local sz = "time: %d/%d\nvolume:%f pan:%f pitch:%f"
  sz = string.format(sz, player.time, file_ms, vol, pan, pitch)
  c:write(sz)
end

function update_test(dt)
  for i = #players, 1, -1 do
    local v = players[i]
    if v:is_playing() == false then
      table.remove(players, i)
    end
  end
  
  local c = sprite.canvas
  c:clear()
  
  c:set_font(font)
  c:move_to(0, 200)
  c:rectangle(190, 60)
  c:stroke()
  c:move_to(-90, 230 - 15)
  c:write("Sound")
  
  for i = #players, 1, -1 do
    local v = players[i]
    if v:is_playing() then
      v.time = v.time + dt
      draw_player(c, 0, -i * 30 + 150, v)
    end
  end
  
  local is_down = keyboard:is_down(KEY_SPACE)
  if wasdown == false and is_down == true then
    local p = audio:play(sound)
    p.time = 0
    table.insert(players, p)
  end
  wasdown = is_down
end
