local dir = get_suitedir()
local ParticleSystem = dofile(dir .. "/particlesystem.lua")
local image = Image()
local canvas = Canvas()
local ps = nil
local current = 1

local systems = 
{
  {
    gravityx = 1300, gravityy = 0,
    distance = 200, speed = 400, speedvariance = 0,
    direction = 90, spread = 25,
    sscale = 1, escale = 2.5, scolor = ORANGE, ecolor = RED
  },
  {
    gravityx = 0, gravityy = 0,
    distance = 100, speed = 100, speedvariance = 0,
    direction = 0, spread = 360, 
    sscale = 1, escale = 0, scolor = WHITE, ecolor = WHITE
  },
  {
    gravityx = 0, gravityy = -400,
    distance = 300, speed = 380, speedvariance = 0,
    direction = -90, spread = 240,
    sscale = 3.15, escale = 2.85, scolor = Color(187, 117, 7), ecolor = Color(144, 109, 189)
  },
  {
    gravityx = 1900, gravityy = -2200,
    distance = 110, speed = 300, speedvariance = 0,
    direction = -168, spread = 95, 
    sscale = 3.7, escale = 5.3, scolor = Color(83, 12, 225), ecolor = Color(88, 13, 233)
  },
  {
    gravityx = 0, gravityy = 10,
    distance = 220, speed = 50, speedvariance = 50,
    direction = 90, spread = 90, 
    sscale = 0.5, escale = 6, scolor = BLACK, ecolor = Color(234, 144, 119)
  }
}
local randsystemmin = 
{
  gravityx = -5000, gravityy = -5000,
  distance = 50, speed = 10, speedvariance = 0,
  direction = 0, spread = 25,
  sscale = 0, escale = 0, scolor = BLACK, ecolor = BLACK
}
local randsystemmax = 
{
  gravityx = 5000, gravityy = 5000,
  direction = 360, speed = 800, speedvariance = 600,
  distance = 400, spread = 360,
  sscale = 5, escale = 10, scolor = WHITE, ecolor = WHITE
}
local bmodes = { "BM_INHERIT", "BM_ZERO", "BM_ONE", "BM_ALPHA", "BM_ADD", "BM_SUB", "BM_REVSUB", "BM_MIN", "BM_MAX" }
local bmode = 4

local function draw_log()
  clear_log()
  
  local props = systems[current]
  local conc = {}
  table.insert(conc, string.format("blend_mode = %s", bmodes[bmode]))
  table.insert(conc, string.format("particles = %s", ps.pcount))
  for i, _ in pairs(props) do
    local szv
    local v = ps[i]
    local t = type(v)
    if t == "userdata" then
      szv = string.format("Color(%d, %d, %d)", v.red, v.green, v.blue)
    else
      szv = tostring(v)
    end
    local sz = string.format("%s = %s", i, szv)
    table.insert(conc, sz)
  end
  local sz =
[[ParticleSystem test
===================
%s

Keys
----
Up/down: cycle systems
Spacebar: randomize!
W/S: particle count
A/D: blending mode]]
  sz = string.format(sz, table.concat(conc, "\n"))
  print_log(sz, -400, 300 - 10)
end

local function randomize_system()
  for i, min in pairs(randsystemmin) do
    local max = randsystemmax[i]
    local t = type(min)
    if t == "userdata" then
      local r = math.random(min.red, max.red)
      local g = math.random(min.green, max.green)
      local b = math.random(min.blue, max.blue)
      ps[i] = Color(r, g, b)
    else
      local n = math.sin(math.random())
      ps[i] = n*(max - min) + min--math.random(min, max)
    end
  end
  -- lower the chance of editing gravity
  if math.random(1, 5) == 1 then
    ps.gravityx = 0
    ps.gravityy = 0
  end
  --ps:UpdateAll()
  ps:Refresh()
  draw_log()
end

local function set_system(id)
  current = id
  local props = systems[id]
  for i, v in pairs(props) do
    ps[i] = v
  end
  --ps:UpdateAll()
  ps:Refresh()
  draw_log()
  ps:SetBlendMode(_G[bmodes[bmode]])
end

function begin_test()
  set_resolution(800, 600)
  image:load(dir .. "/particle.png")

  canvas = Canvas()
  canvas:set_source_image(image)
  canvas:paint()
  
  current = 1

  ps = ParticleSystem.Create(0, -50, 100, canvas)
  set_system(current)
  display.viewport:add_child(ps.layer)
  
  keyboard.on_press = function(keyboard, key)
    if key == KEY_UP then
      current = current - 1
      if current <= 0 then
        current = #systems
      end
      set_system(current)
    elseif key == KEY_DOWN then
      current = current + 1
      if current > #systems then
        current = 1
      end
      set_system(current)
    elseif key == KEY_A then
      bmode = bmode - 1
      if bmode == 0 then
        bmode = #bmodes
      end
      ps:SetBlendMode(_G[bmodes[bmode]])
      draw_log()
    elseif key == KEY_D then
      bmode = bmode + 1
      if bmode > #bmodes then
        bmode = 1
      end
      ps:SetBlendMode(_G[bmodes[bmode]])
      draw_log()
    elseif key == KEY_SPACE then
      randomize_system()
    end
  end
  
end

function end_test()
  keyboard.on_press = nil
  display.viewport:remove_child(ps.layer)
  ps:Destroy()
  ps = nil
  canvas:clear()
  image:unload()
end

function update_test(dt)
  if keyboard:is_down(KEY_W) then
    ps.pcount = ps.pcount + 10
    draw_log()
    ps:SetBlendMode(_G[bmodes[bmode]])
  elseif keyboard:is_down(KEY_S) then
    ps.pcount = math.max ( ps.pcount - 20, 20 )
    draw_log()
    ps:SetBlendMode(_G[bmodes[bmode]])
  end
  ps:Update(dt/1000)
end