local images = {}
for i = 1, 8 do
  images[i] = Image()
end
local sprites = {}
for i = 0, 7 do
  local x = i%4 * 200 + -400 + 64
  local y = math.floor(i/4) * -300 + 200
  sprites[i + 1] = Sprite(x, y)
end

local function bool2sz(bool, expected)
  local sz = "n/a"
  if bool == false then
    sz = "false"
  elseif bool == true then
    sz = "true"
  end
  if sz ~= expected then
    sz = string.format("%s \n (Warning:%s expected)", sz, expected)
  end
  return sz
end

function begin_test()
  set_resolution(800, 600)

  local dir = get_suitedir()
  local file = string.format("%s/128x128rgb.png", dir)
  
  for i, v in ipairs(sprites) do
    display.viewport:add_child(v)
  end
  
  local a = images[1]:load(file)
  sprites[1].canvas:set_source_image(images[1])
  sprites[1].canvas:paint()
  a = bool2sz(a, "true")
  local sz = string.format(" load:%s \n paint ", a)
  print_log(sz, sprites[1].x - 64, sprites[1].y - 64 - 16)
    
  local a = images[2]:load(file)
  local b = images[2]:load(file)
  sprites[2].canvas:set_source_image(images[2])
  sprites[2].canvas:paint()
  a = bool2sz(a, "true")
  b = bool2sz(b, "false")
  local sz = string.format(" load:%s \n load:%s \n paint ", a, b)
  print_log(sz, sprites[2].x - 64, sprites[2].y - 64 - 16)
  
  local a = images[3]:load(file)
  sprites[3].canvas:set_source_image(images[3])
  sprites[3].canvas:paint()
  local b = images[3]:unload()
  a = bool2sz(a, "true")
  b = bool2sz(b, "false")
  local sz = string.format(" load:%s \n paint \n unload:%s", a, b)
  print_log(sz, sprites[3].x - 64, sprites[3].y - 64 - 16)
  
  local a = images[4]:load(file)
  local b = images[4]:unload()
  local c = images[4]:load(file)
  sprites[4].canvas:set_source_image(images[4])
  sprites[4].canvas:paint()
  a = bool2sz(a, "true")
  b = bool2sz(b, "true")
  c = bool2sz(c, "true")
  local sz = string.format(" load:%s \n unload:%s \n load:%s \n paint", a, b, c)
  print_log(sz, sprites[4].x - 64, sprites[4].y - 64 - 16)
  
  sprites[5].canvas:set_source_image(images[5])
  sprites[5].canvas:paint()
  local sz = string.format(" paint")
  print_log(sz, sprites[5].x - 64, sprites[5].y - 64 - 16)

  local a = images[6]:load(file)
  sprites[6].canvas:set_source_image(images[6])
  sprites[6].canvas:paint()
  sprites[6].canvas:clear()
  local b = images[6]:unload()
  a = bool2sz(a, "true")
  b = bool2sz(b, "true")
  local sz = string.format(" load:%s \n paint \n clear \n unload:%s", a, b)
  print_log(sz, sprites[6].x - 64, sprites[6].y - 64 - 16)
  
  local a = images[7]:load(file)
  local b = images[7]:unload()
  sprites[7].canvas:set_source_image(images[7])
  sprites[7].canvas:paint()
  a = bool2sz(a, "true")
  b = bool2sz(b, "true")
  local sz = string.format(" load:%s \n unload:%s \n paint \n", a, b)
  print_log(sz, sprites[7].x - 64, sprites[7].y - 64 - 16)
  
  local a = images[8]:load(file)
  local b = images[8]:load(file)
  sprites[8].canvas:set_source_image(images[8])
  sprites[8].canvas:paint()
  local c = images[8]:unload()
  sprites[8].canvas:clear()
  local d = images[8]:unload()
  local e = images[8]:unload()
  a = bool2sz(a, "true")
  b = bool2sz(b, "false")
  c = bool2sz(c, "false")
  d = bool2sz(d, "true")
  e = bool2sz(e, "false")
  local sz = string.format(" load:%s \n load:%s \n paint \n unload:%s \n clear \n unload:%s \n unload:%s", a, b, c, d, e)
  print_log(sz, sprites[8].x - 64, sprites[8].y - 64 - 16)

  local sz =
[[image load & unload test
========================
Top tests are supposed to show the image, bottom tests should be blank.
Tries different combinations of image:load("%s") and image:unload().
Notice the results of "unload" while the image is STILL in use by a canvas.
Return values of each call to load/unload are shown on the screen.]]
  sz = string.format(sz, file)
  print_log(sz, -400, 20)
end

function end_test()
  for i = 1, 8 do
    sprites[i].canvas:clear()
    display.viewport:remove_child(sprites[i])
    images[i]:unload()
  end
  clear_log()
end