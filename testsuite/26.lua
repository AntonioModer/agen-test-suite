local fonts = {}
for i = 1, 8 do
  fonts[i] = Font()
end
local sprites = {}
for i = 0, 7 do
  local x = i%4 * 200 + -400 + 64
  local y = math.floor(i/4) * -300 + 200
  sprites[i + 1] = Sprite(x, y)
end

local function bool2sz(bool, expected)
  local sz = "n/a"
  if bool == false then
    sz = "false"
  elseif bool == true then
    sz = "true"
  end
  if sz ~= expected then
    sz = string.format("%s \n (Warning:%s expected)", sz, expected)
  end
  return sz
end

function begin_test()
  set_resolution(800, 600)

  local dir = get_suitedir()
  local file = string.format("%s/apple.ttf", dir)
  --local file = "Arial"
  local size = 12
  local text = "ABCD\nDEFG\nHIJK\nLMNO"
  
  for i, v in ipairs(sprites) do
    display.viewport:add_child(v)
  end
  
  local a = fonts[1]:load_file(file, size)
  sprites[1].canvas:set_font(fonts[1])
  sprites[1].canvas:write(text)
  a = bool2sz(a, "true")
  local sz = string.format(" load:%s \n write ", a)
  print_log(sz, sprites[1].x - 64, sprites[1].y - 64 - 16)
  
    
  local a = fonts[2]:load_file(file, size)
  local b = fonts[2]:load_file(file, size)
  sprites[2].canvas:set_font(fonts[2])
  sprites[2].canvas:write(text)
  a = bool2sz(a, "true")
  b = bool2sz(b, "false")
  local sz = string.format(" load:%s \n load:%s \n write ", a, b)
  print_log(sz, sprites[2].x - 64, sprites[2].y - 64 - 16)
  
  local a = fonts[3]:load_file(file, size)
  sprites[3].canvas:set_font(fonts[3])
  sprites[3].canvas:write(text)
  local b = fonts[3]:unload()
  a = bool2sz(a, "true")
  b = bool2sz(b, "false")
  local sz = string.format(" load:%s \n write \n unload:%s", a, b)
  print_log(sz, sprites[3].x - 64, sprites[3].y - 64 - 16)
  
  local a = fonts[4]:load_file(file, size)
  local b = fonts[4]:unload()
  local c = fonts[4]:load_file(file, size)
  sprites[4].canvas:set_font(fonts[4])
  sprites[4].canvas:write(text)
  a = bool2sz(a, "true")
  b = bool2sz(b, "true")
  c = bool2sz(c, "true")
  local sz = string.format(" load:%s \n unload:%s \n load:%s \n write", a, b, c)
  print_log(sz, sprites[4].x - 64, sprites[4].y - 64 - 16)
  
  sprites[5].canvas:set_font(fonts[5])
  sprites[5].canvas:write(text)
  local sz = string.format(" write")
  print_log(sz, sprites[5].x - 64, sprites[5].y - 64 - 16)

  local a = fonts[6]:load_file(file, size)
  sprites[6].canvas:set_font(fonts[6])
  sprites[6].canvas:write(text)
  sprites[6].canvas:clear()
  local b = fonts[6]:unload()
  a = bool2sz(a, "true")
  b = bool2sz(b, "true")
  local sz = string.format(" load:%s \n write \n clear \n unload:%s", a, b)
  print_log(sz, sprites[6].x - 64, sprites[6].y - 64 - 16)
  
  local a = fonts[7]:load_file(file, size)
  local b = fonts[7]:unload()
  sprites[7].canvas:set_font(fonts[7])
  sprites[7].canvas:write(text)
  a = bool2sz(a, "true")
  b = bool2sz(b, "true")
  local sz = string.format(" load:%s \n unload:%s \n write \n", a, b)
  print_log(sz, sprites[7].x - 64, sprites[7].y - 64 - 16)
  
  local a = fonts[8]:load_file(file, size)
  local b = fonts[8]:load_file(file, size)
  sprites[8].canvas:set_font(fonts[8])
  sprites[8].canvas:write(text)
  local c = fonts[8]:unload()
  sprites[8].canvas:clear()
  local d = fonts[8]:unload()
  local e = fonts[8]:unload()
  a = bool2sz(a, "true")
  b = bool2sz(b, "false")
  c = bool2sz(c, "false")
  d = bool2sz(d, "true")
  e = bool2sz(e, "false")
  local sz = string.format(" load:%s \n load:%s \n write \n unload:%s \n clear \n unload:%s \n unload:%s", a, b, c, d, e)
  print_log(sz, sprites[8].x - 64, sprites[8].y - 64 - 16)

  local sz =
[[font load & unload test
=======================
Top tests are supposed to show the text, bottom tests should be blank.
Tries different combinations of font:load("%s") and font:unload().
Notice the results of "unload" while the font is STILL in use by a canvas.
Return values of each call to load/unload are shown on the screen.]]
  sz = string.format(sz, file, size)
  print_log(sz, -400, 20)
end

function end_test()
  for i = 1, 8 do
    sprites[i].canvas:clear()
    display.viewport:remove_child(sprites[i])
    fonts[i]:unload()
  end
  clear_log()
end