--------------------------------------------------
--
-- Best practices:
--	* create new Sound for each audio file
--	* reuse player
--
--------------------------------------------------
local player = nil
local dir = get_suitedir() .. "/beat"
local files = {}
if lfs == nil then
  require ("lfs")
end
for file in lfs.dir(dir) do
  if file ~= '.' and file ~= '..' then
    local full = string.format("%s/%s", dir, file)
    local attr = lfs.attributes(full)
    if attr.mode ~= 'directory' then
      table.insert(files, file)
    end
  end
end

local volume, pan, pitch = 1, 0, 0
local current = 1
local warnings = {}
local bits, channels, frequency = 16, 2, 44100


local function stop_sound()
  if player ~= nil then
    player:stop()
  end
  --[[
  player = nil
  collectgarbage('collect')
  if sound:unload() == false then
    table.insert(warnings, "(Warning:unload failed)")
  end
  ]]
end

local function play_sound(id)
  stop_sound()
  current = id
  assert(current > 0 and current <= #files)
  assert(files[current])
  local f = string.format("%s/%s", dir, files[current])
  local sz = string.format('sound:load("%s\")', f)
  table.insert(warnings, sz)
  -- will create new sound
  local sound = Sound()
  if sound:load(f) == false then
    table.insert(warnings, "(Warning:load failed)")
	return
  end
  if player == nil then
  	player = audio:play_loop(sound, volume, pan, pitch)
    if player == nil then
      table.insert(warnings, "(Warning:play_loop returned nil)")
    end
  else
    player.sound  = sound
	player.volume = volume
	player.pan    = pan
	player.pitch  = pitch
	if not player:play_loop() then
      table.insert(warnings, "(Warning:play_loop failed)")
	end
  end
  collectgarbage('collect')
end

local function update_log()
  clear_log()
  local isp = "N/A"
  
  local vol, pan, pit = "n/a", "n/a", "n/a"
  local isp = "n/a"
  if player then
    vol = tostring(player.volume)
    pan = tostring(player.pan)
    pit = tostring(player.pitch)
    local ip = player:is_playing()
    if ip == true then
      isp = "true"
    elseif ip == false then
      isp = "false"
    end
  end
  
  local sz =
[[sound file formats
==================
Test loading/playing different sound formats
Current audio mode is bits:%d channels:%d frequency:%d

Sound
-----
file:%s

Player
------
volume:%s
pan:%s
pitch:%s
is_playing:%s

Keys
----
Space: play the current file in a loop
Escape: stop and unload the current file
Up/Down: previous/next file
A/D: pan
W/S: volume
Q/E: pitch

Log:
----
%s]]

  local f = files[current]
  if player == nil then
    f = "n/a"
  end

  while #warnings > 35 do
    table.remove(warnings, 1)
  end
  local warns = table.concat(warnings, '\n')
  sz = string.format(sz, bits, channels, frequency, f, vol, pan, pit, isp, warns)
  print_log(sz, -320, 320 - 10)
end

function begin_test()
  set_resolution(640, 640)
  
  warnings = {}
  current = 1

  if audio:set_mode(bits, channels, frequency) == false then
    table.insert(warnings, "(Warning:set mode failed)")
  end
  if #files == 0 then
    local sz = string.format("(Warning:no sound files found in '%s')", dir)
    table.insert(warnings, sz)
  end
  
  play_sound(current)
  update_log()
  
  keyboard.on_press = function(keyboard, key)
    if key == KEY_SPACE then
      play_sound(current)
    elseif key == KEY_ESCAPE then
      stop_sound()
    elseif key == KEY_UP then
      current = current - 1
      if current <= 0 then
        current = #files
      end
      play_sound(current)
    elseif key == KEY_DOWN then
      current = current + 1
      if current > #files then
        current = 1
      end
      play_sound(current)
    elseif key == KEY_D then
      pan = pan + 0.1
      pan = math.min(pan, 1)
      if player then
        player.pan = pan
      end
    elseif key == KEY_A then
      pan = pan - 0.1
      pan = math.max(pan, -1)
      if player then
        player.pan = pan
      end
    elseif key == KEY_W then
      volume = volume + 0.1
      volume = math.min(volume, 1)
      if player then
        player.volume = volume
      end
    elseif key == KEY_S then
      volume = volume - 0.1
      volume = math.max(volume, 0)
      if player then
        player.volume = volume
      end
    elseif key == KEY_E then
      pitch = pitch + 0.1
      pitch = math.min(pitch, 1)
      if player then
        player.pitch = pitch
      end
    elseif key == KEY_Q then
      pitch = pitch - 0.1
      pitch = math.max(pitch, -1)
      if player then
        player.pitch = pitch
      end
    end
  end
end

function end_test()
  keyboard.on_press = nil
  clear_log()
  if player then
    player:stop()
    player = nil
  end
  player = nil
end

function update_test(dt)
  update_log()
end
