local sprite = Sprite()
local sound = Sound()
local player = nil
local dir = get_suitedir()
local file = string.format("%s/squarewave.wav", dir)
local elapsed = 0
local osi = 1500
local panosi = true
local pitosi = false
local volosi = false

function begin_test()
  set_resolution(512, 512)
  
  local bits, channels, frequency = 16, 2, 44100
  audio:set_mode(bits, channels, frequency)

  display.viewport:add_child(sprite)
  sound = Sound()
  sound:load(file)

  player = audio:play_loop(sound, 0.5)
  elapsed = 0
  
  keyboard.on_press = function(kb, key)
    if key == KEY_1 then
      volosi = not volosi
    elseif key == KEY_2 then
      panosi = not panosi
    elseif key == KEY_3 then
      pitosi = not pitosi
    end
  end
  mouse.on_wheelmove = function(m, z)
    if z < 0 then
      player.volume = player.volume - 0.1
      player.volume = math.min(player.volume, 1)
      volosi = false
    elseif z > 0 then
      player.volume = player.volume + 0.1
      player.volume = math.max(player.volume, 0)
      volosi = false
    end
  end
end

function end_test()
  keyboard.on_press = nil
  mouse.on_wheelmove = nil
  if player then
    player:stop()
  end
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  clear_log()
  sound:unload()
end

function update_test(dt)
  local up = keyboard:is_down(KEY_UP)
  local down = keyboard:is_down(KEY_DOWN)
  if up then
    osi = osi + 10
    volosi = true
  elseif down then
    osi = osi - 10
    osi = math.max(osi, 10)
    volosi = true
  end
  local v = player.pan
  local v2 = player.pitch
  local v3 = player.volume
  if w then
    v3 = v3 + 0.001
    v3 = math.min(v3, 1)
  elseif s then
    v3 = v3 - 0.001
    v3 = math.max(v3, 0)
  end
  if a then
    v = v - 0.001
    v = math.min(v, 1)
  elseif d then
    v = v + 0.001
    v = math.max(v, 0)
  end
  if q then
    v2 = v2 - 0.001
    v2 = math.min(v2, 1)
  elseif e then
    v2 = v2 + 0.001
    v2 = math.max(v2, 0)
  end
  -- update 
  elapsed = elapsed + dt
  local x, y = 256/mouse.xaxis, 256/mouse.yaxis
  local i = elapsed/osi
  if panosi then
    v = math.cos(i*math.pi)
  end
  if pitosi then
    v2 = math.sin(i*math.pi)
  end
  if volosi then
    v3 = (math.cos(i*math.pi) + 1)/2
  end

  if mouse:is_down(MBUTTON_LEFT) then
    v = mouse.xaxis/256
    v2 = mouse.yaxis/256
    panosi = false
    pitosi = false
  end
  
  --assert(v3 >= -1 and v3 <= 1)
  player.pan = v
  player.pitch = v2
  player.volume = v3
  
  local c = sprite.canvas
  c:clear()
  c:move_to(-256, 0)
  c:line_to(256, 0)
  c:move_to(0, 256)
  c:line_to(0, -256)
  c:stroke()
  c:move_to(v*256, v2*256)
  c:square(64*v3)
  c:fill()
  
  clear_log()
  local sz =
[[Pan, pitch and volume oscillation
=================================
volume = %f
pan = %f
pitch = %f
oscillation = %f ms

Keys
----
1 - volume oscillation
2 - pan
3 - pitch
Up/Down - hold to change oscillation speed

Mouse
-----
Left button - hold to set pan/pitch manually
Wheel - adjusts volume manually]]
  sz = string.format(sz, v3, v, v2, osi)
  print_log(sz, -256, 256 - 10)
end
