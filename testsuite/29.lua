local sprite = Sprite()
local font = Font()
local mx, my = mouse.xaxis, mouse.yaxis

function begin_test()
  set_resolution(600, 600)
  
  display.viewport:add_child(sprite)
  font:load_system("Arial", 14)
  
  clear_log()
  local sz =
[[Mouse coords test
=================
Shows the mouse axis coordinates. Center of the window should be 0, 0
RESIZE the window to check if the coords remain correct :)
MOVE your mouse rapidly to see if you can 'shake off' the blue circle]]
  sz = string.format(sz)
  print_log(sz, -300, 300 - 10)
  
  print_log("RED: mouse.xaxis, mouse.yaxis", -300, 300 - 60, RED)
  print_log("BLUE: accumulated dx and dy (click to sync to axes)", -300, 300 - 70, BLUE)

  mx, my = mouse.xaxis, mouse.yaxis
  
  mouse.on_move = function(m, dx, dy)
    mx, my = mx + dx, my + dy
  end
  mouse.on_press = function(m, b)
    mx, my = mouse.xaxis, mouse.yaxis
  end
end

function end_test()
  mouse.on_move = nil
  mouse.on_press = nil
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  clear_log()
end

function update_test(dt)
  local c = sprite.canvas
  c:clear()
  for i = -300, 300, 100 do
    c:move_to(i, -300)
    c:line_to(i, 300)
  end
  for i = -300, 300, 100 do
    c:move_to(-300, i)
    c:line_to(300, i)
  end
  c:set_line_style(1, GRAY)
  c:stroke()
  c:move_to(0, 0)
  c:square(600)
  c:set_line_style(3, WHITE)
  c:stroke()
  
  c:move_to(mx, my)
  c:circle(20)
  c:set_line_style(1, BLUE)
  c:stroke()
  c:rel_move_to(10, 10)
  c:set_font(font, BLUE)
  c:write(mx .. "," .. my)

  if mx ~= mouse.xaxis or my ~= mouse.yaxis then
    c:move_to(mx, my)
    c:line_to(mouse.xaxis, mouse.yaxis)
    c:set_line_style(1, WHITE, 1)
    c:stroke()
  end
  
  c:move_to(mouse.xaxis, -300)
  c:line_to(mouse.xaxis, 300)
  c:move_to(-300, mouse.yaxis)
  c:line_to(300, mouse.yaxis)
  c:set_line_style(1, RED)
  c:stroke()
  c:move_to(mouse.xaxis + 10, mouse.yaxis - 20)
  c:set_font(font, RED)
  c:write(mouse.xaxis .. "," .. mouse.yaxis)
end
