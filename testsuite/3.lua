local image = Image()
local image2 = Image()
local sprite = Sprite()

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local sz = string.format("%s/512x512lines.png", dir)
  image:load(sz)
  local sz = string.format("%s/16x16square.png", dir)
  image2:load(sz)
  
  sprite.canvas:set_source_image(image)
  sprite.canvas:paint()
  local squares = {
    { -256 + 8, 256 - 8 },
    { 256 - 8, 256 - 8 },
    { -256 + 8, -256 + 8 },
    { 256 - 8, -256 + 8 },
    { -128 + 8, 128 - 8 },
    { 128 - 8, 128 - 8 },
    { -128 + 8, -128 + 8 },
    { 128 - 8, -128 + 8 }
  }
  for i, v in ipairs(squares) do
    sprite.canvas:set_source_image(image2, v[1], v[2])
    sprite.canvas:paint()
  end
  display.viewport:add_child(sprite)

  local sz =
[[set_source_image() test
=======================
Diagonal, vertical and horizontal lines: from .png image
Corner squares: painted on top from a second image]]
  print_log(sz, -256, 0)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  image:unload()
  image2:unload()
  clear_log()
end
