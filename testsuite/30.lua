function begin_test()
  set_resolution(640, 480)



  local out = {}
  
  --[[
  local devices = system.enumerate_devices ( system.DEV_CLASS_ALL )
  table.insert(out, "\nEnumeration")
  table.insert(out, "-----------")
  if devices then
    for i, device in ipairs ( devices ) do
      local sz = string.format ( "%d. %s\n", i, device )
      table.insert(out, sz)
    end
  else
    table.insert(out, "Warning: Device enumeration failed!")
  end
]]
  table.insert(out, "\nGlobal device names")
  table.insert(out, "-------------------")
  local devs = { "display", "audio", "keyboard", "mouse", "joystick" }
  for i, v in ipairs(devs) do
    if _G[v] then
      local name = _G[v].name or "nil"
      local warn = ""
      if _G[v].name == nil then
        warn = string.format('(Warning: "%s.name" is nil)', v)
      end
      local sz = string.format ( "%s.name = %s %s\n", v, name, warn)
      table.insert(out, sz)
    else
      local sz = string.format('%s = nil (Warning: global "%s" is nil)\n', v, v)
      table.insert(out, sz)
    end
  end
  
  table.insert(out, "\nJoystick list")
  table.insert(out, "-------------")
  for i, v in ipairs(joysticks) do
    local name = v.name or string.format('(Warning: "joystick[%d].name" is nil)', i)
    local sz = string.format ( "%d.%s\n", i, name )
    table.insert(out, sz)
  end
  
  local sz =
[[Device enumeration & names
==========================
Probably a good idea to check the device names with different plugins :)

%s]]
  sz = string.format(sz, table.concat(out, "\n"))
  print_log(sz, -320, 240 - 10)
end

function end_test()
  clear_log()
end
