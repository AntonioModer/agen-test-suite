local sound = Sound()
local player = nil
local dir = get_suitedir() .. "/pitch"
local files =
{
  "b0.wav", "b0.wav",
  "b-75.wav", "b0.wav",
  "b-50.wav", "b0.wav",
  "b-25.wav", "b0.wav",
  "b25.wav", "b0.wav",
  "b50.wav", "b0.wav",
  "b100.wav", "b0.wav",
  -----
  "0.wav", "0.wav",
  "-0.75.wav", "0.wav",
  "-0.5.wav", "0.wav",
  "-0.25.wav", "0.wav",
  "0.25.wav", "0.wav",
  "0.5.wav", "0.wav",
  "1.wav", "0.wav"
}
local exports =
{
  nil, nil,
  -0.75, nil,
  -0.5, nil,
  -0.25, nil,
  0.25, nil,
  0.5, nil,
  1, nil,
  ----
  nil, nil,
  -0.75, nil,
  -0.5, nil,
  -0.25, nil,
  0.25, nil,
  0.5, nil,
  1, nil
}
local pitches =
{
  nil, 0,
  nil, -0.75,
  nil, -0.5,
  nil, -0.25,
  nil, 0.25,
  nil, 0.5,
  nil, 1,
  ---
  nil, 0,
  nil, -0.75,
  nil, -0.5,
  nil, -0.25,
  nil, 0.25,
  nil, 0.5,
  nil, 1
}
local current = 1
local warnings = {}
local bits, channels, frequency = 16, 2, 44100


local function stop_sound()
  if player == nil then
    return
  end
  table.insert(warnings, "player:stop()")
  player:stop()
  player = nil
  table.insert(warnings, "sound:unload()")
  if sound:unload() == false then
    table.insert(warnings, "(Warning:unload failed)")
  end
end

local function play_sound(id)
  stop_sound()
  assert(player == nil)
  current = id
  assert(current > 0 and current <= #files)
  assert(files[current])
  local f = string.format("%s/%s", dir, files[current])
  local sz = string.format('sound:load("%s\")', f)
  table.insert(warnings, sz)
  if sound:load(f) == false then
    table.insert(warnings, "(Warning:load failed)")
  end
  table.insert(warnings, "audio:play(sound)")
  player = audio:play_loop(sound)
  if player == nil then
    table.insert(warnings, "(Warning:play_loop returned nil)")
  else
    if pitches[current] then
      player.pitch = pitches[current]
    end
  end
end

local function update_log()
  clear_log()
  
  local ep = exports[current] or "NO"
  local cp = pitches[current] or "NO"
  
  local sz =
[[Pre-exported pitch vs engine pitch
==================================
Compares sound files pre-saved at given pitch to output from the engine.
Files were exported using Audacity's "Change Speed" option.

File
----
Saved:    %s pitch
Playing:  %s pitch

Keys
----
Space: play the current file in a loop
Escape: stop and unload the current file
Up/Down: previous/next file

Log:
----
%s]]

  while #warnings > 35 do
    table.remove(warnings, 1)
  end
  local warns = table.concat(warnings, '\n')
  sz = string.format(sz, ep, cp, warns)
  print_log(sz, -320, 320 - 10)
end

function begin_test()
  set_resolution(640, 640)
  
  warnings = {}
  current = 1

  if audio:set_mode(bits, channels, frequency) == false then
    table.insert(warnings, "(Warning:set mode failed)")
  end
  
  play_sound(current)
  update_log()
  
  keyboard.on_press = function(keyboard, key)
    if key == KEY_SPACE then
      play_sound(current)
    elseif key == KEY_ESCAPE then
      stop_sound()
    elseif key == KEY_UP then
      current = current - 1
      if current <= 0 then
        current = #files
      end
      play_sound(current)
    elseif key == KEY_DOWN then
      current = current + 1
      if current > #files then
        current = 1
      end
      play_sound(current)
    end
  end
end

function end_test()
  keyboard.on_press = nil
  clear_log()
  if player then
    player:stop()
  end
  sound:unload()
  player = nil
end

function update_test(dt)
  update_log()
end
