local scene = Layer()
local sprites = {}
for i = 1, 16 do
  sprites[i] = Sprite()
  scene:add_child(sprites[i])
end
local bg = Sprite()
scene:add_child(bg)
local scradius = 30
local bcradius = 150

local font = Font()

local function redraw_sprite(s, radius, n1, bg)
  local c = s.canvas
  c:clear()
  c:circle(radius)
  c:set_fill_style(bg, 1)
  c:fill_preserve()
  c:set_line_style(3, WHITE)
  c:stroke()
  c:set_font(font)

  local w, h = font:get_width(n1), font:get_size()
  c:set_font(font, BLACK)
  c:move_to(-w/2, -h/2)
  c:write(n1)
end

local function redraw_sprites()
  redraw_sprite(bg, bcradius, bg.depth, GRAY)
  for i, v in ipairs(sprites) do
    redraw_sprite(v, scradius, v.depth, GRAY)
  end
end

function begin_test()
  set_resolution(512, 512)
  
  font:load_system("Arial", 20)
 
  display.viewport:add_child(scene)
  scene:set_position(0, -64 - 16)
  bg.depth = 10
  
  for i, v in ipairs(sprites) do
    local r = (i/#sprites - 1)*math.pi*2
    local x = math.cos(r)*100
    local y = math.sin(r)*100
    v.depth = i - 8
    v:set_position(x, y)
  end

  redraw_sprites()

  local sz =
[[Sprite depth test
=================
Can you see a big gray circle with 16 small gray circles on top?
All the spirtes are contained in a single layer.
Each sprite's depth is shown as a number

layer --------------> sprite
depth=0           |   depth=-7
                  |
                  +-> sprite
                  |   depth=-6
                  |
                [...]
                  |
                  +-> sprite
                  |   depth=8
                  |
                  --> sprite (big gray circle)
                      depth=10]]
  print_log(sz, -256, 256 - 10)
end

function end_test()
  display.viewport:remove_child(scene)
  --display.viewport:remove_child(layer2)
  
  clear_log()
end
