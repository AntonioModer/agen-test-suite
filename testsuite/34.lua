local image = Image()
local sprite = Sprite()
local dir = get_suitedir() .. "/imgformats"
local files = {}
if lfs == nil then
  require ("lfs")
end
for file in lfs.dir(dir) do
  if file ~= '.' and file ~= '..' then
    local full = string.format("%s/%s", dir, file)
    local attr = lfs.attributes(full)
    if attr.mode ~= 'directory' then
      table.insert(files, file)
    end
  end
end
local current = 1
local warnings = {}


local function clear_image()
  sprite.canvas:clear()
  if image:unload() == false then
    table.insert(warnings, "(Warning:unload failed)")
  end
end

local function draw_image(id)
  clear_image()
  assert(player == nil)
  current = id
  assert(current > 0 and current <= #files)
  assert(files[current])
  local f = string.format("%s/%s", dir, files[current])
  local sz = string.format('image:load("%s\")', f)
  table.insert(warnings, sz)
  if image:load(f) == false then
    table.insert(warnings, "(Warning:load failed)")
  end
  local c = sprite.canvas
  c:clear()
  local sq = 32
  for i = 0, 29 do
    for j = 0, 29 do
      local n = (i - 1)*29 + j
      if n%2 == 0 then
        c:move_to(i*sq - 15*sq, j*sq - 15*sq)
        c:square(sq)
        c:set_fill_style(GRAY, 0.51)
        c:fill()
      end
    end
  end
  c:set_source_image(image)
  c:paint()
end

local function update_log()
  clear_log()
  
  local sz =
[[Image file formats
==================
Test loading/drawing different image formats

Image
-----
file:%s

Keys
----
Up/Down: previous/next file

Log:
----
%s]]

  local f = files[current]

  while #warnings > 5 do
    table.remove(warnings, 1)
  end
  local warns = table.concat(warnings, '\n')
  sz = string.format(sz, f, warns)
  print_log(sz, -320, 320 - 10)
end

function begin_test()
  set_resolution(640, 640)
  
  warnings = {}
  current = 1
  
  if #files == 0 then
    local sz = string.format("(Warning:no sound files found in '%s')", dir)
    table.insert(warnings, sz)
  end
  
  display.viewport:add_child(sprite)
  
  draw_image(current)
  update_log()
  
  keyboard.on_press = function(keyboard, key)
    if key == KEY_UP then
      current = current - 1
      if current <= 0 then
        current = #files
      end
      draw_image(current)
    elseif key == KEY_DOWN then
      current = current + 1
      if current > #files then
        current = 1
      end
      draw_image(current)
    end
    update_log()
  end
end

function end_test()
  keyboard.on_press = nil
  clear_log()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  image:unload()
  player = nil
end
