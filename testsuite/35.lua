local ntests = 16
local scene = Layer()
local sprites_fill = {}
for i = 1, ntests do
  sprites_fill[i] = Sprite()
  scene:add_child(sprites_fill[i])
end
local sprites_img = {}
for i = 1, ntests do
  sprites_img[i] = Sprite()
  scene:add_child(sprites_img[i])
end
local sprites_scale = {}
for i = 1, ntests do
  sprites_scale[i] = Sprite()
  scene:add_child(sprites_scale[i])
end
local images = {}
for i = 1, ntests do
  images[i] = Image()
end
local bgimage = Image()
local bg = Sprite()
bg.depth = 10
scene:add_child(bg)

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local file = string.format("%s/512x512squares32.png", dir)
  bgimage:load(file)
  for i = 1, ntests do
    local fn = string.format("%s/square/%d.png", dir, i)
    images[i]:load(fn)
  end
  
  display.viewport:add_child(scene)
  
  bg.canvas:set_source_image(bgimage)
  bg.canvas:paint()
    
  for i = 1, ntests do
    local x = i * 32 + -256
    
    local v = sprites_fill[i]
    local y = -128
    v:set_position(x, y)
    v.canvas:square(i)
    v.canvas:set_fill_style(RED, 1)
    v.canvas:fill()

    local v = sprites_img[i]
    local y = -64
    v:set_position(x, y)
    v.canvas:set_source_image(images[i], 8 - i/2, -8 + i/2)
    v.canvas:paint()

    local v = sprites_scale[i]
    local y = 0
    v:set_position(x, y)
    v.canvas:square(1)
    v.canvas:set_fill_style(RED, 1)
    v.canvas:fill()
    v:set_scale(i, i)
  end
  
  print_log("square(1)\nfill(RED)\nset_scale(S, S)", 0, 0 - 20)
  print_log("set_src_img(S.png, S/2, -S/2)\npaint()", 0, -64 - 20)
  print_log("square(S)\nfill(RED)", 0, -128 - 20)
  
  local sz =
[[Sub-pixel precision
===================
This test compares pixel-precise positioning of squares.
The background is a static image with 32x32 squares
Using different methods we draw red squares in increasing size.
Look for X and Y displacement between the different methods.
Notice how 'odd' vs 'even' sized squares are drawn.
Remember to check how it works with different GFX plugins. :)]]
  sz = string.format(sz, file)
  print_log(sz, -256, 256 - 20)
  
end

function end_test()
  display.viewport:remove_child(scene)
  for i = 1, ntests do
    sprites_fill[i].canvas:clear()
    sprites_scale[i].canvas:clear()
    sprites_img[i].canvas:clear()
    
    images[i]:unload()
  end
  bg.canvas:clear()
  clear_log()
  bgimage:unload()
end

































































