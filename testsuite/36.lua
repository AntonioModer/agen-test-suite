local scene = Layer()
local sprite = Sprite()
sprite.depth = 0
scene:add_child(sprite)
local mask = Sprite()
mask.depth = -1
scene:add_child(mask)
local overlay = Sprite(256 - 128, 256 - 64)
overlay:set_scale(0.25, 0.25)
overlay.depth = -10
scene:add_child(overlay)
local bg = Sprite()
bg.depth = 10
scene:add_child(bg)
local bmodes = { "BM_INHERIT", "BM_ZERO", "BM_ONE", "BM_ALPHA", "BM_ADD", "BM_SUB", "BM_REVSUB", "BM_MIN", "BM_MAX" }
local bmode = 1

local image = Image()
local maskimage = Image()

function begin_test()
  set_resolution(512, 512)
  
  display.viewport:add_child(scene)
  
  bmode = 1
  mask.blend_mode = _G[bmodes[bmode]]
  
  local dir = get_suitedir()
  assert(image:load(dir .. "/bmodes/kitten.png"))
  assert(maskimage:load(dir .. "/bmodes/mask.png"))

  sprite.canvas:clear()
  sprite.canvas:set_source_image(image)
  sprite.canvas:paint()
  
  mask.canvas:clear()
  mask.canvas:set_source_image(maskimage)
  mask.canvas:paint()
  
  overlay.canvas:clear()
  overlay.canvas:move_to(-256, 0)
  overlay.canvas:square(512)
  overlay.canvas:set_line_style(8, BLACK, 1)
  overlay.canvas:stroke()
  overlay.canvas:set_source_image(image, -256, 0)
  overlay.canvas:paint()
  overlay.canvas:move_to(256, 0)
  overlay.canvas:square(512)
  overlay.canvas:set_line_style(8, BLACK, 1)
  overlay.canvas:stroke()
  overlay.canvas:set_source_image(maskimage, 256, 0)
  overlay.canvas:paint()
  
  local c = bg.canvas
  c:clear()
  local sq = 32
  for i = 0, 29 do
    for j = 0, 29 do
      local n = (i - 1)*29 + j
      if n%2 == 0 then
        c:move_to(i*sq - 15*sq, j*sq - 15*sq)
        c:square(sq)
        c:set_fill_style(GRAY, 0.51)
        c:fill()
      end
    end
  end
  
  keyboard.on_press = function(kb, key)
    if key == KEY_A then
      bmode = bmode + 1
      if bmode > #bmodes then
        bmode = 1
      end
      mask.blend_mode = _G[bmodes[bmode]]
    elseif key == KEY_D then
      bmode = bmode - 1
      if bmode <= 0 then
        bmode = #bmodes
      end
      mask.blend_mode = _G[bmodes[bmode]]
    elseif key == KEY_W then
      mask.alpha = mask.alpha + 0.1
      mask.alpha = math.min(1, mask.alpha)
    elseif key == KEY_S then
      mask.alpha = mask.alpha - 0.1
      mask.alpha = math.max(0, mask.alpha)
    end
  end
end

function end_test()
  keyboard.on_press = nil
  display.viewport:remove_child(scene)
  sprite.canvas:clear()
  mask.canvas:clear()
  image:unload()
  maskimage:unload()
  clear_log()
end

function update_test()
  local sz =
[[Blending modes test
===================
mask.blend_mode = %s
mask.alpha = %s

Keys
----
A/D: Change blend mode
W/S: Change alpha]]
  sz = string.format(sz, bmodes[bmode], mask.alpha)
  clear_log()
  print_log(sz, -256, 256 - 10)
  
  print_log("image", 128 - 128, 256 - 8)
  print_log("mask", 128, 256 - 8)
end