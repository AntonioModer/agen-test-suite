local w, h = 80, 40
local hw, hh = math.floor(w/2), math.floor(h/2)
local sq_size = 2

local function set_size(lw, lh)
  w, h = math.max(lw, 10), math.max(lh, 5)
  hw, hh = math.floor(w/2), math.floor(h/2)
end

local scene = Layer()
local sprite = Sprite()
scene:add_child(sprite)

local tilemap = Layer()
scene:add_child(tilemap)
local tiles = {}

local canvas_inst = Canvas()
canvas_inst:square(sq_size - 1)
canvas_inst:fill()
local tilemap_inst = Layer()
scene:add_child(tilemap_inst)
local tiles_inst = {}

local overlay = Sprite()
overlay.depth = -10
scene:add_child(overlay)

local deltas = {}
local deltacolors = {}
local oldmode = nil

local modes = { sprite, tilemap, tilemap_inst }
local modesz = { "Single canvas", "Sprites", "Sprites with instancing" }
local mode = nil

local function init()
  for x = 1, w do
    for y = 1, h do
      sprite.canvas:move_to(x*sq_size, y*sq_size)
      sprite.canvas:square(sq_size - 1)
      sprite.canvas:fill()
    end
  end
  
  for x = 1, w do
    for y = 1, h do
      local i = h*(x - 1) + y
      tiles[i] = Sprite(x*sq_size, y*sq_size)
      tiles[i].canvas:square(sq_size - 1)
      tiles[i].canvas:fill()
      tilemap:add_child(tiles[i])
    end
  end
  
  for x = 1, w do
    for y = 1, h do
      local i = h*(x - 1) + y
      tiles_inst[i] = Sprite(x*sq_size, y*sq_size)
      tiles_inst[i].canvas = canvas_inst
      tilemap_inst:add_child(tiles_inst[i])
    end
  end
end

local function release()
  sprite.canvas:clear()
  
  for x = 1, w do
    for y = 1, h do
      local i = h*(x - 1) + y
      tilemap:remove_child(tiles[i])
      tiles[i].canvas:clear()
      tiles[i] = nil
      
      tilemap_inst:remove_child(tiles_inst[i])
      tiles_inst[i].canvas = nil
      tiles_inst[i] = nil
    end
  end
end

local function set_mode(m)
  if mode == nil then
    init()
  end
  mode = m
  for i, v in ipairs(modes) do
    v.visible = (m == i)
    v:set_position(-hw*sq_size, -hh*sq_size)
  end
end

function begin_test()
  set_resolution(512, 512)
  
  display.viewport:add_child(scene)
  
  keyboard.on_press = function(kb, key)
    if key == KEY_TILDE then
      set_mode(0) -- off
    elseif key == KEY_1 then
      set_mode(1)
    elseif key == KEY_2 then
      set_mode(2)
    elseif key == KEY_3 then
      set_mode(3)
    elseif key == KEY_UP then
      local old = mode
      if mode then
        release()
        mode = nil
      end
      set_size(w + 10, h + 5)
    elseif key == KEY_DOWN then
      local old = mode
      if mode then
        release()
        mode = nil
      end
      set_size(w - 10, h - 5)
    end
  end
end

function end_test()
  if mode then
    release()
  end
  keyboard.on_press = nil
  display.viewport:remove_child(scene)
  overlay.canvas:clear()
  clear_log()
end

function update_test(dt)
  local buffersize = 256
  local xscaler = 512/buffersize
  
  local color = GRAY
  if oldmode ~= mode then
    color = WHITE
    oldmode = mode
  end
  table.insert(deltacolors, color)
  if #deltacolors > buffersize then
    table.remove(deltacolors, 1)
  end
  
  local fps = 60/(dt/16.6666667)
  table.insert(deltas, fps)
  if #deltas > buffersize then
    table.remove(deltas, 1)
  end
  local ox, oy = -256, -256
  local c = overlay.canvas
  c:clear()
  c:move_to(0, oy+10*2*3)
  c:rectangle(512, 10*2*6)
  c:set_fill_style(WHITE, 0.25)
  c:fill()
  for i = 0, 6 do
    c:move_to(ox, oy + i*10*2)
    c:line_to(ox + 512, oy + i*10*2)
  end
  c:set_line_style(1, GRAY, 0.5)
  c:stroke()
  
  for i, v in ipairs(deltacolors) do
    if v == WHITE then
      c:move_to(i*xscaler + ox, oy)
      c:line_to(i*xscaler + ox, oy + 6*10*2)
    end
  end
  c:set_line_style(1, GRAY, 1)
  c:stroke()
  
  c:move_to(ox, deltas[1]*2 + oy)
  for i = 2, #deltas do
    local v = deltas[i]
    c:line_to(i*xscaler + ox, v*2 + oy)
  end
  c:set_line_style(1, RED, 1)
  c:stroke()
  
  local msz = "Press key 1-3 to select"
  if mode and mode > 0 then
    msz = modesz[mode]
  end
  local sz =
[[Canvas vs Multiple sprites vs Instancing
=======================================
Mode: %s 
Resolution: %dx%d = %d rectangles
FPS: %d

Keys
----
1: Single canvas (all squares on 1 canvas)
2: Sprites (one sprite per square)
3: Sprites with shared canvas
~: Toggle .visible
Up/Down: Adjust resolution]]
  sz = string.format(sz, msz, w, h, w*h, fps)
  clear_log()
  print_log(sz, -256, 256 - 10)
end
