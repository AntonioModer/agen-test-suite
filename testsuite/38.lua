local scene = Layer()
local sprite = Sprite()
scene:add_child(sprite)

local canvas = Canvas()
canvas:rectangle(2, 10)
canvas:fill()

local rotation = 0
local sprites = {}
local deltas = {}
local memories = {}
local elapsed = 0
local rspeed = 90

local added = 0
local removed = 0
local paused = false

local forcecollect = "restart"
local gcsteps = 1

function begin_test()
  set_resolution(512, 700)
  
  display.viewport:add_child(scene)
  
  rotation = 0
  forcecollect = "restart"
  rspeed = 90
  paused = false
  
  keyboard.on_press = function(kb, key)
    if key == KEY_1 then
      forcecollect = "restart"
      collectgarbage("restart")
    elseif key == KEY_2 then
      forcecollect = "stop"
      collectgarbage("stop")
    elseif key == KEY_3 then
      forcecollect = "collect"
      collectgarbage("restart")
    elseif key == KEY_4 then
      forcecollect = "step"
      collectgarbage("restart")
    elseif key == KEY_W then
      rspeed = rspeed + 10
    elseif key == KEY_S then
      rspeed = math.max(rspeed - 10, 10)
    elseif key == KEY_A then
      gcsteps = math.max(gcsteps - 1, 1)
    elseif key == KEY_D then
      gcsteps = gcsteps + 1
    elseif key == KEY_ESCAPE then
      paused = not paused
    end
  end
end

function end_test()
  collectgarbage("restart")
  keyboard.on_press = nil
  while #sprites > 0 do
    table.remove(sprites, i)
  end
  while #deltas > 0 do
    table.remove(deltas)
    table.remove(memories)
  end
  display.viewport:remove_child(scene)
  clear_log()
end

function update_test(dt)
  local seconds = dt/1000
  elapsed = elapsed + seconds
  local fps = 60/(dt/16.6666667)
  table.insert(deltas, fps)
  table.insert(memories, collectgarbage("count"))
  if #deltas > 256 then
    table.remove(deltas, 1)
    table.remove(memories, 1)
  end
  
  if paused == false then
    local nextrot = rotation + rspeed*seconds
    local s, e = math.floor(rotation), math.floor(nextrot)
    for i = s, e do
      local idx = math.floor(i)%360 + 1
      if sprites[idx] then
        --sprites[idx].canvas:clear()
        --sprites[idx].canvas = nil
        assert(scene:remove_child(sprites[idx]) == true)
        sprites[idx] = nil
        removed = removed + 1
      end
      local r = math.rad(idx)
      local x, y = math.cos(r)*200, math.sin(r)*200
      sprites[idx] = Sprite(x, y)
      scene:add_child(sprites[idx])
      sprites[idx]:set_rotation(math.random(360))
      sprites[idx].canvas:rectangle(2, 5)
      sprites[idx].canvas:fill()
      added = added + 1
    end
    
    rotation = nextrot
  end
  local c = sprite.canvas
  c:clear()
  c:move_to(0, 0)
  local r = math.rad(rotation)
  local x, y = math.cos(r)*150, math.sin(r)*150
  c:line_to(x, y)
  c:set_line_style(1, WHITE, 1)
  c:stroke()

  for i = 0, 6 do
    c:move_to(-128, 240 + i*10)
    c:line_to(128, 240 + i*10)
  end
  c:set_line_style(1, GRAY, 1)
  c:stroke()
  
  c:move_to(-128 + 1, 240 + deltas[1])
  for i = 2, #deltas do
    c:line_to(-128 + i, 240 + deltas[i])
  end
  c:set_line_style(1, RED, 1)
  c:stroke()
  
  local maxmemory = memories[1]
  for i, v in ipairs(memories) do
    maxmemory = math.max(maxmemory, v)
  end
  c:move_to(-128 + 1, 240 + memories[1]/maxmemory*60)
  for i = 2, #memories do
    c:line_to(-128 + i, 240 + memories[i]/maxmemory*60)
  end
  c:set_line_style(1, WHITE, 1)
  c:stroke()
  
  clear_log()
  print_log("FPS:60", -128 - 50, 240 + 60, RED)
  print_log("FPS:0", -128 - 50, 240, RED)
  local sz = string.format("Mem:%d", maxmemory)
  print_log(sz, 128 + 5, 240 + 60, WHITE)
  print_log("Mem:0", 128 + 5, 240, WHITE)
  
  local fc = forcecollect
  if fc == "step" then
    fc = string.format("%s (%d)", forcecollect, gcsteps)
  end
  local sz = [[Added/Removed: %d/%d (%d on screen)
Rate: %d
GCollect: %s
FPS: %d]]
  sz = string.format(sz, added, removed, added - removed, rspeed, fc, fps)
  print_log(sz, -128, 340)
  
  local sz =
[[Garbage collection test
=======================
Adds and removes sprites at a constant rate.
Probably a good idea to also monitor application memory for leaks.
1: "restart" (auto)
2: "stop"
3: "collect" (full cycle each frame)
4: "step" (single GC step each frame)
W/S: adjust rate of creating sprites
A/D: adjust GC steps used by collectgarbage("step")
Escape: pause/unpause (used to check graph overhead)]]
  print_log(sz, -256, -256)
  
  if forcecollect ~= "stop" and forcecollect ~= "restart" then
    if forcecollect == "step" then
      collectgarbage(forcecollect, gcsteps)
    else
      collectgarbage(forcecollect)
    end
  end
end