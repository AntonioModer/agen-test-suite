local scene = Layer()
local sprite = Sprite()
scene:add_child(sprite)

local canvas = Canvas()
canvas:rectangle(2, 10)
canvas:fill()

local rotation = 0
local sprite2 = Sprite(-256, 256)
scene:add_child(sprite2)
local deltas = {}
local memories = {}
local elapsed = 0
local rspeed = 20

local paused = false

local forcecollect = "restart"
local gcsteps = 1

local sq = 16
local w, h = 18, 18

local map = {}

local function resize_map(lw, lh)
  w, h = lw, lh
  sq = 512/(w + 1)
  for i = 1, lw do
    map[i] = {}
    for j = 1, lh do
      map[i][j] = { 0, 0 }
    end
  end
end
resize_map(w, h)

local function reset_map()
  for i = 1, w do
    for j = 1, h do
      local r = map[i][j]
      r[1], r[2] = 0, 0
    end
  end
end

local function get_defpos(i, j)
  return i*sq, -j*sq
end

local function get_pos(i, j)
  local x, y = get_defpos(i, j)
  local r = map[i][j]
  return x + r[1], y + r[2]
end

local function change_pos(i, j, x, y)
  local r = map[i][j]
  r[1] = r[1] + x
  r[2] = r[2] + y
end

local function redraw(c, sq)
  c:clear()
  for i = 1, w do
    for j = 1, h do
      local x, y = get_pos(i, j)
      --c:move_to(x, y)
      --c:square(5)
      --c:fill()
      if i > 1 then
        local lx, ly = get_pos(i - 1, j)
        c:move_to(lx, ly)
        c:line_to(x, y)
        c:stroke()
      end
      if j > 1 then
        local lx, ly = get_pos(i, j - 1)
        c:move_to(lx, ly)
        c:line_to(x, y)
        c:stroke()
      end
    end
  end
end

local function apply_mass(mx, my, attract)
  for i = 1, w do
    for j = 1, h do
      local x, y = get_pos(i, j)
      local dx, dy = x - mx, y - my
      local d = math.sqrt(dx*dx + dy*dy)
      if d > 0 then
        if d > -attract then
          local s = 1/d*attract
          local nx, ny = dx*s, dy*s
          change_pos(i, j, nx, ny)
        else
          change_pos(i, j, -dx, -dy)
        end
      end
    end
  end
end



function begin_test()
  set_resolution(512, 700)
  
  display.viewport:add_child(scene)
  
  rotation = 0
  forcecollect = "restart"
  rspeed = 90
  paused = false
  
  keyboard.on_press = function(kb, key)
    if key == KEY_1 then
      forcecollect = "restart"
      collectgarbage("restart")
    elseif key == KEY_2 then
      forcecollect = "stop"
      collectgarbage("stop")
    elseif key == KEY_3 then
      forcecollect = "collect"
      collectgarbage("restart")
    elseif key == KEY_4 then
      forcecollect = "step"
      collectgarbage("restart")
    elseif key == KEY_W then
      resize_map(w+1, h+1)
    elseif key == KEY_S then
      if w > 5 then
        resize_map(w-1, h-1)
      end
    elseif key == KEY_A then
      gcsteps = math.max(gcsteps - 1, 1)
    elseif key == KEY_D then
      gcsteps = gcsteps + 1
    elseif key == KEY_ESCAPE then
      paused = not paused
    end
  end
end

function end_test()
  collectgarbage("restart")
  keyboard.on_press = nil
  
  sprite.canvas:clear()
  sprite2.canvas:clear()
  
  while #deltas > 0 do
    table.remove(deltas)
    table.remove(memories)
  end
  display.viewport:remove_child(scene)
  clear_log()
end

function update_test(dt)
  local seconds = dt/1000
  elapsed = elapsed + seconds
  local fps = 60/(dt/16.6666667)
  table.insert(deltas, fps)
  table.insert(memories, collectgarbage("count"))
  if #deltas > 256 then
    table.remove(deltas, 1)
    table.remove(memories, 1)
  end
  
  if paused == false then
    local nextrot = rotation + rspeed*seconds

    reset_map()
    local r = math.rad(nextrot)
    local x, y = math.cos(r)*100, math.sin(r)*100
    apply_mass(x + sq*w/2 + sq/2, y - sq*h/2 - sq/2, -15)
    --apply_mass(sq*w/2 + sq/2, -sq*h/2 - sq/2, -50)
    redraw(sprite2.canvas, sq)
    
    rotation = nextrot
  end
  local c = sprite.canvas
  c:clear()

  for i = 0, 6 do
    c:move_to(-128, 240 + i*10)
    c:line_to(128, 240 + i*10)
  end
  c:set_line_style(1, GRAY, 1)
  c:stroke()
  
  c:move_to(-128 + 1, 240 + deltas[1])
  for i = 2, #deltas do
    c:line_to(-128 + i, 240 + deltas[i])
  end
  c:set_line_style(1, RED, 1)
  c:stroke()
  
  local maxmemory = memories[1]
  for i, v in ipairs(memories) do
    maxmemory = math.max(maxmemory, v)
  end
  c:move_to(-128 + 1, 240 + memories[1]/maxmemory*60)
  for i = 2, #memories do
    c:line_to(-128 + i, 240 + memories[i]/maxmemory*60)
  end
  c:set_line_style(1, WHITE, 1)
  c:stroke()
  
  clear_log()
  print_log("FPS:60", -128 - 50, 240 + 60, RED)
  print_log("FPS:0", -128 - 50, 240, RED)
  local sz = string.format("Mem:%d", maxmemory)
  print_log(sz, 128 + 5, 240 + 60, WHITE)
  print_log("Mem:0", 128 + 5, 240, WHITE)
  
  local fc = forcecollect
  if fc == "step" then
    fc = string.format("%s (%d)", forcecollect, gcsteps)
  end
  local sz = [[size: %d/%d
line_to: %d calls
GCollect: %s
FPS: %d]]
  sz = string.format(sz, w, h, w*h*2, fc, fps)
  print_log(sz, -128, 340)
  
  local sz =
[[Canvas test
===========
Draws on a canvas continously.
Probably a good idea to also monitor application memory for leaks.
1: "restart" (auto)
2: "stop"
3: "collect" (full cycle each frame)
4: "step" (single GC step each frame)
W/S: adjust grid size
A/D: adjust GC steps used by collectgarbage("step")
Escape: pause/unpause (used to check graph overhead)]]
  print_log(sz, -256, -256)
  
  if forcecollect ~= "stop" and forcecollect ~= "restart" then
    if forcecollect == "step" then
      collectgarbage(forcecollect, gcsteps)
    else
      collectgarbage(forcecollect)
    end
  end
end