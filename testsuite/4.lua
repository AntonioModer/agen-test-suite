local sprite = Sprite()
local image = Image()

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local sz = string.format("%s/512x512lines.png", dir)
  image:load(sz)
  
  sprite.canvas:set_source_image(image)
  sprite.canvas:paint()
  local squares = {
    { -256 + 8, 256 - 8 },
    { 256 - 8, 256 - 8 },
    { -256 + 8, -256 + 8 },
    { 256 - 8, -256 + 8 },
    { -128 + 8, 128 - 8 },
    { 128 - 8, 128 - 8 },
    { -128 + 8, -128 + 8 },
    { 128 - 8, -128 + 8 }
  }
  for i, v in ipairs(squares) do
    sprite.canvas:move_to(v[1], v[2])
    for i = 16, 2, -2 do
      sprite.canvas:square(i)
      local c = BLACK
      if i%4 == 0 then
        c = RED
      end
      sprite.canvas:set_fill_style(c, 1)
      sprite.canvas:fill()
    end
  end
  display.viewport:add_child(sprite)

  local sz =
[[square() test
=============
Diagonal, vertical and horizontal lines: from .png image
Corner squares: drawn on top using "square" and "fill"]]
  print_log(sz, -256, 0)
end

function end_test()
  image:unload()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  clear_log()
end

function update_test()

end