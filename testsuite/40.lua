local scene = Layer()
local sprites = {}
for i = 1, 8 do
  sprites[i] = Sprite()
  sprites[i].canvas:square(64)
  sprites[i].canvas:fill()
  scene:add_child(sprites[i])
end

local bgimg = Image()
local bg = Sprite()
bg.depth = 10
scene:add_child(bg)

function begin_test()
  set_resolution(512, 512)
  
  bgimg:load("testsuite/512x512scaling.png")
  bg.canvas:set_source_image(bgimg)
  bg.canvas:paint()
  
  display.viewport:add_child(scene)
  sprites[1]:set_position(-256+32*3, 256-32*2)
  
  print_log("x=-256+32*3\ny=256-32*2\nset_position(x,y)", -256+32*0.5, 256-32*4, RED)
  
  sprites[2]:set_position(-256+32*7, 256-32*2)
  sprites[2]:set_rotation(-45)
  
  print_log("x=-256+32*7\ny=256-32*2\nset_position(x,y)\nset_rotation(-45)", -256+32*5, 256-32*4, RED)
  
  sprites[3]:set_position(-256+32*11, 256-32*2)
  sprites[3]:set_scale(2, 1)

  print_log("x=-256+32*11\ny=256-32*2\nset_position(x,y)\nset_scale(2,1)", -256+32*10, 256-32*4, RED)
  
  sprites[4]:set_position(-256+32*7, 256-32*7)
  sprites[4]:change_position(32, 0)
  sprites[4]:set_rotation(-45)

  print_log("x=-256+32*7\ny=256-32*7\nset_position(x,y)\nchange_position(32,0)\nset_rotation(-45)", -256+32*4, 256-32*8.5, RED)
  
  sprites[5]:set_position(-256+32*11, 256-32*7)
  sprites[5]:change_position(32, 0)
  sprites[5]:set_scale(2, 1)

  print_log("x=-256+32*11\ny=256-32*7\nset_position(x,y)\nchange_position(32,0)\nset_scale(-2,1)", -256+32*10, 256-32*8.5, RED)
  
  sprites[6]:set_position(-256+32*3, 256-32*12)
  sprites[6]:change_position(32, 0)
  sprites[6]:set_scale(2, 1)
  sprites[6]:set_rotation(-45)

  print_log("x=-256+32*3\ny=256-32*12\nset_position(x,y)\nchange_position(32,0)\nset_scale(2,1)\nset_rotation(-45)", -256+32*0, 256-32*14.5, RED)
  
  sprites[7]:set_position(-256+32*7, 256-32*12)
  sprites[7]:change_position(32, 0)
  sprites[7]:set_rotation(-45)
  sprites[7]:set_scale(2, 1)

  print_log("x=-256+32*7\ny=256-32*12\nset_position(x,y)\nchange_position(32,0)\nset_rotation(-45)\nset_scale(2,1)", -256+32*5.5, 256-32*14.5, RED)
    
  sprites[8]:set_position(-256+32*11, 256-32*12)
  sprites[8]:set_rotation(-45)
  sprites[8]:set_scale(2, 1)
  sprites[8]:change_position(32, 0)

  print_log("x=-256+32*11\ny=256-32*12\nset_position(x,y)\nset_rotation(-45)\nset_scale(2,1)\nchange_position(32,0)", -256+32*11, 256-32*14.5, RED)
    
  local sz =
[[Transform test
==============
You should see white rects
surrounded by red borders.
The red borders are
drawn from a static
image as a reference.
The white rectangles are
transformed sprites.
]]
  print_log(sz, -256, 80)
end

function end_test()
  bg.canvas:clear()
  
  display.viewport:remove_child(scene)
  clear_log()
end
