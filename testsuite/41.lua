local scene = Layer()
local sprite = Sprite()
scene:add_child(sprite)
local bg = Sprite()
bg.depth = 10
scene:add_child(bg)

local tforms = {}
local max_number_power = 31
local max_number = 2^max_number_power

local funcs =
{
  { "change_rotation", 1 },
  { "change_rotation_r", 1 },
  { "set_rotation", 1 },
  { "set_rotation_r", 1 },
  { "set_scale", 2 },
  { "change_scale", 2 },
  { "change_scalex", 1 },
  { "change_scaley", 1 },
  --{ "set_scalex", 1 },
  --{ "set_scaley", 1 },
  { "set_position", 2 },
  { "change_position", 2 }
}
local props =
{
  "rotation", "scalex", "scaley", "x", "y"
}

local function update_log()
  clear_log()
  local sz =
[[Transform & reset test
======================
Applies a series of random transforms to the WHITE sprite,
then resets its position(0, 0), rotation(0) and scale(1, 1).
After the reset, the WHITE sprite should look like
the reference square (RED).
Keys
----
Press space bar to apply 10 transforms]]
  print_log(sz, -256, 256 - 20, RED)
  
  for i, v in ipairs(tforms) do
    print_log(v, -256, -60 - 10*i, RED)
  end
end

local function rand_number()
  local a = math.random() - 0.5
  return a*max_number*2
end

local function apply_transforms(n)
  tforms = {}
  for i = 1, n do
    local r = math.random(2)
    local n1 = rand_number()
    local n2 = rand_number()
    if r == 1 then
      local f = math.random(#funcs)
      local func = funcs[f]
      if func[2] == 1 then
      assert(sprite[func[1]], func[1])
        sprite[func[1]](sprite, n1)
        local sz = string.format("%s(%s)", func[1], n1)
        table.insert(tforms, sz)
      elseif func[2] == 2 then
      assert(sprite[func[1]], func[1])
        sprite[func[1]](sprite, n1, n2)
        local sz = string.format("%s(%s, %s)", func[1], n1, n2)
        table.insert(tforms, sz)
      end
    elseif r == 2 then
      local p = math.random(#props)
      local prop = props[p]
      sprite[prop] = n1
      local sz = string.format(".%s = %s", prop, n1)
      table.insert(tforms, sz)
    end
  end
  sprite:set_scale(1, 1)
  sprite:set_position(0, 0)
  sprite:set_rotation(0)
  table.insert(tforms, "set_scale(1, 1)")
  table.insert(tforms, "set_position(0, 0)")
  table.insert(tforms, "set_rotation(0)")
  update_log()
end

function begin_test()
  set_resolution(512, 512)

  tforms = {}
  display.viewport:add_child(scene)
  
  bg.canvas:square(32)
  bg.canvas:set_line_style(2, RED)
  bg.canvas:stroke()
  sprite.canvas:square(30)
  sprite.canvas:fill()

  update_log()
  
  keyboard.on_press = function(k, b)
    if b == KEY_SPACE then
      apply_transforms(10)
    end
  end
end

function end_test()
  bg.canvas:clear()
  
  display.viewport:remove_child(scene)
  clear_log()
  keyboard.on_press = nil
end
