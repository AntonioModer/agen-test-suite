local function loadworld()
  world = xml.loadWorld("testsuite/box2d.xml")
  scene = b2Scene:Create(world)
  display.viewport.camera:set_scale(0.01, 0.01)
  scene:AddCamera(display.viewport.camera)
  local b = world:GetBodyList()
  while b do
    scene:AddBody(b)
    scene:RedrawBody(b, 1, GRAY, 1)
    b = b:GetNext()
  end
  scene:SyncSprites()
end
local function unloadworld()
  display.viewport.camera:set_scale(1, 1)
  if scene then
    scene:Destroy()
    scene = nil
  end
  if world then
    xml.unloadWorld(world)
    world = nil
  end
end


function begin_test()
  set_resolution(800, 600)
  
  print_log("Box2D 2.2.1", -400, 300 - 20, RED)
  local r, f = pcall(dofile, "testsuite/b2dxml2/main.lua")
  if r == false then
    print_log("Error loading B2DXML2:", -400, 300 - 40, RED)
    print_log(f, -400, 300 - 50, RED)
    return
  end
  dofile("testsuite/b2dxml2/cast.lua")
  
  print_log("SPACE - Reload", -400, 300 - 40, RED)

  loadworld()
  
  keyboard.on_press = function(k, b)
    if b == KEY_SPACE then
      unloadworld()
      loadworld()
    end
  end
end

function end_test()
  unloadworld()
  clear_log()
  keyboard.on_press = nil
end

function update_test(dt)
  if world then
    world:Step(dt/1000, 10, 8)
    scene:SyncSprites()
    scene:DrawJoints(0.01)
    world:ClearForces()
  end
end