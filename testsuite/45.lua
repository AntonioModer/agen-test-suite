local scene = Layer ( )

local map = {}
map[1] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
map[2] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
map[3] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
map[4] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
map[5] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 5, 6, 1 }
map[6] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 5, 7, 7, 7, 5 }
map[7] = { 1, 1, 1, 1, 1, 1, 4, 5, 5, 6, 1, 1, 1, 4, 7, 7, 7, 7, 7, 7 }
map[8] = { 1, 1, 4, 5, 5, 5, 7, 7, 7, 7, 5, 6, 4, 7, 7, 7, 7, 7, 7, 7 }
map[9] = { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 }
map[10] = { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }

local images = {}
images[1] = Image ( "testsuite/tiles/1.bmp" )
images[2] = Image ( "testsuite/tiles/2.bmp" )
images[3] = Image ( "testsuite/tiles/3.bmp" )
images[4] = Image ( "testsuite/tiles/4.bmp" )
images[5] = Image ( "testsuite/tiles/5.bmp" )
images[6] = Image ( "testsuite/tiles/6.bmp" )
images[7] = Image ( "testsuite/tiles/7.bmp" )

for y = 1, #map, 1 do
  for x = 1, #map[y], 1 do
    local tile = map[y][x]
    local sprite = Sprite ( x * 32, y * -32 )
    sprite.canvas:set_source_image ( images[tile] )
    sprite.canvas:paint ( )
    scene:add_child ( sprite )
  end
end

camera = Camera ( )
camera:set_scale ( 0.5, 0.5 )
camera:set_position ( 10 * 32, 6 * -32 )
scene:add_child ( camera )
camera2 = Camera ( )
camera2:set_scale ( 2, 2 )
scene:add_child ( camera2 )

function begin_test()
  set_resolution(640, 480)

  local sz = [[Viewport/camera test
====================
There should be two small viewports in the bottom left and right corner.
The left viewport uses viewport:set_scale the right uses camera:set_scale.

A/UP - move camera left
D/DOWN - move camera right]]
  print_log(sz, -320, 240 - 10, WHITE)

  view2 = Viewport(0, 380, 100, 100)
  view2:set_scale(0.25, 0.25)
  view2.bgcolor = RED
  view2.camera = camera
  view2.depth = -1
  display.viewport:add_child(view2)
  
  view3 = Viewport(540, 380, 100, 100)
  view3.bgcolor = LIME
  view3.camera = camera2
  view3.depth = -1
  display.viewport:add_child(view3)

  display.viewport.camera = camera
end

function end_test()
  clear_log()
  display.viewport.camera = Camera()
  display.viewport:remove_child(view2)
  display.viewport:remove_child(view3)
end

function update_test(dt)
  if keyboard:is_down ( KEY_A ) then
    camera.x = camera.x - dt/100
  elseif keyboard:is_down ( KEY_D ) then
    camera.x = camera.x + dt/100
  end
  if keyboard:is_down ( KEY_UP ) then
    camera.x = camera.x - dt/10
  elseif keyboard:is_down ( KEY_DOWN ) then
    camera.x = camera.x + dt/10
  end
  camera2.x = camera.x
  camera2.y = camera.y
end