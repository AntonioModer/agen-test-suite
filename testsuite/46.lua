local log = {}

local events =
{
  on_close = "close.wav",
  on_create = "create.wav",
  on_destroy = "destroy.wav",
  on_gainfocus = "gain.wav",
  on_losefocus = "lose.wav",
  on_resize = "resize.wav",
  on_rotate = "rotate.wav",
  on_suspend = "suspend.wav",
  on_unsuspend = "unsuspend.wav"
}
local sevents = {}

local function log_msg(sz)
  table.insert(log, sz)
  audio:play(sevents[sz])
end

local function redraw_log()
  clear_log()
  print_log("display callbacks test\n======================\nMove, drag or resize the window to test", -320, 240 - 10, WHITE)
  local s = math.max(1, #log - 40)
  local e = math.min(#log, s + 40)
  for i = s, e do
    if log[i] then
      local sz = string.format("%d. %s", i, log[i])
      print_log(sz, -320, 220 - (i - s + 2)*10, WHITE)
    end
  end
end

function begin_test()
  audio:set_mode(16, 2, 44100)
  for i, v in pairs(events) do
    sevents[i] = Sound()
    sevents[i]:load("testsuite/events/" .. v)
  end

  set_resolution(640, 480)
  
  display.on_create = function(d)
    log_msg("on_create")
  end
  display.on_destroy = function(d)
    log_msg("on_destroy")
  end
  display.on_close = function(d)
    log_msg("on_close")
  end
  display.on_resize = function(d)
    log_msg("on_resize")
  end
  display.on_gainfocus = function(d)
    log_msg("on_gainfocus")
  end
  display.on_losefocus = function(d)
    log_msg("on_losefocus")
  end
  display.on_suspend = function(d)
    log_msg("on_suspend")
  end
  display.on_unsuspend = function(d)
    log_msg("on_unsuspend")
  end
  display.on_rotate = function(d)
    log_msg("on_rotate")
  end
  
  
  log = {}
  
  redraw_log()
end

function end_test()
  display.on_create = nil
  display.on_destroy = nil
  display.on_close = nil
  display.on_resize = nil
  display.on_gainfocus = nil
  display.on_losefocus = nil
  display.on_suspend = nil
  display.on_unsuspend = nil
  display.on_rotate = nil
  clear_log()
end

function update_test(dt)
  redraw_log()
end