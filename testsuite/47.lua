local scene = Layer()

function begin_test()
  for i = 1, 100 do
    local x = math.random(-2000, 2000)
    local y = math.random(-2000, 2000)
    local s = Sprite(x, y)
    local a = 0
    local v = {}
    for i = 1, 10 do
      a = a + math.rad(36)
      if a > math.pi*2 then
        break
      end
      local d = math.random(10, 500)
      local vx = math.cos(a)*d
      local vy = math.sin(a)*d
      table.insert(v, vx)
      table.insert(v, vy)
    end
    
    s.canvas:move_to(v[1], v[2])
    for i = 3, #v, 2 do
      s.canvas:line_to(v[i], v[i + 1])
    end

    local r = math.random(1, 255)
    local g = math.random(1, 255)
    local b = math.random(1, 255)
    local c = Color(r, g, b)
    s.canvas:close_path()
    s.canvas:set_fill_style(c, 0.5)
    s.canvas:fill_preserve()
    s.canvas:set_line_style(1, WHITE, 1)
    s.canvas:stroke()
    
    for i = 1, #v, 2 do
      s.canvas:move_to(v[i], v[i + 1])
      s.canvas:square(5)
    end
    s.canvas:stroke()

    scene:add_child(s)
  end
  scene:add_child(display.viewport.camera)
  set_resolution(800, 600)

  mouse.on_move = function(mouse, dx, dy)
    if mouse:is_down(MBUTTON_LEFT) then
      local oz = display.viewport.camera.scalex
      display.viewport.camera:change_position(-dx*oz, -dy*oz)
    end
  end
  mouse.on_wheelmove = function(mouse, z)
    if z < 0 then
      z = 0.9
    else
      z = 1.1
    end
    local oz = display.viewport.camera.scalex
    display.viewport.camera:set_scale(oz*z, oz*z)
  end
end

function end_test()
  scene:remove_children()
  clear_log()
  mouse.on_move = nil
  mouse.on_wheelmove = nil
  display.viewport.camera:set_position(0, 0)
  display.viewport.camera:set_rotation(0)
  display.viewport.camera:set_scale(1, 1)
end

function update_test(dt)
  clear_log()
  print_log("Culling test\n=============\nUse the mouse to move/zoom camera\nLook for missing polygons near the edges of the screen", -400, 300 - 10, WHITE)
  print_log("zoom:" .. display.viewport.camera.scalex, -400, 300 - 60, RED)
  print_log("x:" .. display.viewport.camera.x, -400, 300 - 80, RED)
  print_log("y:" .. display.viewport.camera.y, -400, 300 - 100, RED)

end