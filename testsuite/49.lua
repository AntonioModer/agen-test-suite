function begin_test()
  custom_timer = Timer()

  custom_timer:start(16, true)
  custom_timer.accum = 0
  custom_timer.accumms = 0
  custom_timer.on_tick = function(t)
    custom_timer.accum = custom_timer.accum + t:get_delta()*0.016
    custom_timer.accumms = custom_timer.accumms + t:get_delta_ms()/1000
  end
  os_time_start = os.time()
  os_clock_start = os.clock()
  system_ticks_start = system.get_ticks()
  
  os_time_max_delay = 0
  os_clock_max_delay = 0
  system_ticks_max_delay = 0
  accum_max_delay = 0
  accumms_max_delay = 0
  accum_ticks_max_delay = 0
  
  set_resolution(800, 600)
end

function end_test()
  custom_timer:stop()
  clear_log()
end

function update_test(dt)
  clear_log()
  local time = os.time() - os_time_start -- float perhaps doesn't have the needed precision?
  local clock = os.clock () - os_clock_start
  local elapsed = custom_timer:get_elapsed()/1000
  local accum = custom_timer.accum
  local accumms = custom_timer.accumms
  local ticks = (system.get_ticks() - system_ticks_start)/1000
  
  local diff_time = elapsed - time
  local diff_clock = elapsed - clock
  local diff_accum = elapsed - accum
  local diff_accumms = elapsed - accumms
  local diff_accum_ticks = elapsed - ticks
  
  os_time_max_delay = math.max(os_time_max_delay, math.abs(diff_time))
  os_clock_max_delay = math.max(os_clock_max_delay, math.abs(diff_clock))
  accum_max_delay = math.max(accum_max_delay, math.abs(diff_accum))
  accumms_max_delay = math.max(accumms_max_delay, math.abs(diff_accumms))
  accum_ticks_max_delay = math.max(accum_ticks_max_delay, math.abs(diff_accum_ticks))

  print_log([[Timer vs os.clock vs os.time vs system.get_ticks
================================================
Notes:
-the tests assumes that timer:elapsed() is most precise
-os.time has precision around 128 seconds
-delta_ms() is less precise than delta() probably because delta_ms() returns an integer]], -400, 300 - 10, WHITE)
  

  print_log("os.time        " .. time, -400, 300 - 140, WHITE)
  print_log("delay:" .. diff_time, -100, 300 - 140, RED)
  print_log("max delay:" .. os_time_max_delay, 150, 300 - 140, RED)
  
  print_log("os.clock       " .. clock, -400, 300 - 160, WHITE)
  print_log("delay:" .. diff_clock, -100, 300 - 160, RED)
  print_log("max delay:" .. os_clock_max_delay, 150, 300 - 160, RED)
  
  print_log("timer.delta    " .. accum, -400, 300 - 180, WHITE)
  print_log("delay:" .. diff_accum, -100, 300 - 180, RED)
  print_log("max delay:" .. accum_max_delay, 150, 300 - 180, RED)
  
  print_log("timer.delta_ms " .. accumms, -400, 300 - 200, WHITE)
  print_log("delay:" .. diff_accumms, -100, 300 - 200, RED)
  print_log("max delay:" .. accumms_max_delay, 150, 300 - 200, RED)
  
  print_log("get_ticks      " .. ticks, -400, 300 - 220, WHITE)
  print_log("delay:" .. diff_accum_ticks, -100, 300 - 220, RED)
  print_log("max delay:" .. accum_ticks_max_delay, 150, 300 - 220, RED)
  
  print_log("timer.elapsed  " .. elapsed, -400, 300 - 260, WHITE)

end