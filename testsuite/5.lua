local sprite = Sprite()

function begin_test()
  set_resolution(512, 512)
  
  sprite.canvas:move_to(0, 0)
  sprite.canvas:square(512)
  sprite.canvas:set_fill_style(RED, 1)
  sprite.canvas:fill()
  sprite.canvas:square(510)
  sprite.canvas:set_fill_style(BLACK, 1)
  sprite.canvas:fill()
  sprite.canvas:square(256)
  sprite.canvas:set_fill_style(RED, 1)
  sprite.canvas:fill()
  sprite.canvas:square(254)
  sprite.canvas:set_fill_style(BLACK, 1)
  sprite.canvas:fill()

  sprite.canvas:move_to(-256, 256)
  sprite.canvas:line_to(256, -256)
  sprite.canvas:move_to(-256, -256)
  sprite.canvas:line_to(256, 256)

  sprite.canvas:set_line_style(1, RED, 1)
  sprite.canvas:stroke()
  local squares = {
    { -256 + 8, 256 - 8 },
    { 256 - 8, 256 - 8 },
    { -256 + 8, -256 + 8 },
    { 256 - 8, -256 + 8 },
    { -128 + 8, 128 - 8 },
    { 128 - 8, 128 - 8 },
    { -128 + 8, -128 + 8 },
    { 128 - 8, -128 + 8 }
  }
  for i, v in ipairs(squares) do
    sprite.canvas:move_to(v[1], v[2])
    for i = 16, 2, -2 do
      sprite.canvas:square(i)
      local c = BLACK
      if i%4 == 0 then
        c = RED
      end
      sprite.canvas:set_fill_style(c, 1)
      sprite.canvas:fill()
    end
  end
  display.viewport:add_child(sprite)

  local sz =
[[line_to() test
==============
Horizontal and vertical lines: drawn using "square" and "fill"
Diagonal lines: drawn on top using "line_to" and "stroke"
Corner squares: drawn on top using "square" and "fill"]]
  print_log(sz, -256, 0)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  clear_log()
end

function update_test()

end