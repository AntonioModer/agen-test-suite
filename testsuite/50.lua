local function initgui()
    logger = {}

    grab_keyboard = false
    local file = "testsuite/beat/16bit_pcm_44100_stereo.wav" -- music/sound filename

    audio:set_mode ( 16, 2, 44100 )

    require ( "gui.main" )
    require ( "gui.obj.label" )
    require ( "gui.obj.button" )
    require ( "gui.obj.slider" )
    require ( "gui.obj.arrow" )
    require ( "gui.obj.scrollbar" )
    gui.create ( )

    local cd = gui.Container ( 150, -100, 300, 600 )
    gui.container:add_child ( cd )

    local name = gui.Label ( 0, 500, audio.name, 300 )
    cd:add_child ( name )

    local gvolume = gui.Label ( 0, 470, "audio.volume:1 (" .. audio.volume .. ")", 300 )
    cd:add_child ( gvolume )

    local scrollbar = gui.Scrollbar ( 0, 440, 300, 15, 101, true )
    scrollbar.slider.value = math.floor ( audio.volume * 100 + 1 )
    cd:add_child ( scrollbar )

    scrollbar.slider.on_change = function ( self )
      local p = ( self.value - 1 ) / 100
      p = math.max ( p, 0 )
      p = math.min ( p, 1 )
      audio.volume = p
      table.insert(logger, "audio.volume = " .. p)
      gvolume.text = "audio.volume:" .. p .. " (" .. audio.volume .. ")"
    end


    local button1 = gui.Button ( 0, 410, "play" )
    cd:add_child ( button1 )

    local button2 = gui.Button ( 50, 410, "play_loop" )
    cd:add_child ( button2 )

    local button3 = gui.Button ( 150, 410, "stop" )
    cd:add_child ( button3 )

    local button4 = gui.Button ( 200, 410, "pause" )
    cd:add_child ( button4 )

    local button5 = gui.Button ( 250, 410, "resume" )
    cd:add_child ( button5 )

    table.insert(logger, "asset = Music()")
    asset = Music ( )
    assert ( asset:load ( file ) == true )

    table.insert(logger, "player = audio:play(asset, 1, 0, 0)")
    player = audio:play ( asset, 1, 0, 0 )
    table.insert(logger, "player:stop()")
    player:stop ( )

    button1.on_click = function ( self )
      table.insert(logger, "player:play()")
      player:play ( )
    end

    button2.on_click = function ( self )
      table.insert(logger, "player:play_loop()")
      player:play_loop ( )
    end

    button3.on_click = function ( self )
      table.insert(logger, "player:stop()")
      player:stop ( )
    end

    button4.on_click = function ( self )
      table.insert(logger, "player:pause()")
      player:pause ( )
    end

    button5.on_click = function ( self )
      table.insert(logger, "player:resume()")
      player:resume ( )
    end


    local label1 = gui.Label ( 0, 380, "player.volume", 300 )
    cd:add_child ( label1 )

    local scrollbar1 = gui.Scrollbar ( 0, 350, 300, 15, 101, true )
    scrollbar1.slider.value = 101
    cd:add_child ( scrollbar1 )

    scrollbar1.slider.on_change = function ( self )
      local p = ( self.value - 1 ) / 100
      p = math.max ( p, 0 )
      p = math.min ( p, 1 )
      player.volume = p
      label1.text = "player.volume:" .. p .. " (" .. player.volume .. ")"
      table.insert(logger, "player.volume = " .. p)
    end

    local label2 = gui.Label ( 0, 320, "player.pan", 300 )
    cd:add_child ( label2 )

    scrollbar2 = gui.Scrollbar ( 0, 290, 300, 15, 101, true )
    scrollbar2.slider.value = 51
    cd:add_child ( scrollbar2 )

    scrollbar2.slider.on_change = function ( self )
      local p = ( self.value - 51 ) / 50
      p = math.max ( p, -1 )
      p = math.min ( p, 1 )
      player.pan = p
      label2.text = "player.pan:" .. p .. " (" .. player.pan .. ")"
      table.insert(logger, "player.pan = " .. p)
    end

    local label3 = gui.Label ( 0, 260, "player.pitch", 300 )
    cd:add_child ( label3 )

    scrollbar3 = gui.Scrollbar ( 0, 230, 300, 15, 101, true )
    scrollbar3.slider.value = 51
    cd:add_child ( scrollbar3 )

    scrollbar3.slider.on_change = function ( self )
      local p = ( self.value - 51 ) / 50
      p = math.max ( p, -1 )
      p = math.min ( p, 1 )
      player.pitch = p
      table.insert(logger, "player.pitch = " .. p)
      label3.text = "player.pitch:" .. p .. " (" .. player.pitch .. ")"
    end



    local button6 = gui.Button ( 0, 200, "Music" )
    cd:add_child ( button6 )

    local button7 = gui.Button ( 50, 200, "Sound" )
    cd:add_child ( button7 )

    button6.on_click = function ( self )
      local v = ( scrollbar1.slider.value - 1 ) / 100
      v = math.max ( v, 0 )
      v = math.min ( v, 1 )
      local p = ( scrollbar2.slider.value - 51 ) / 50
      p = math.max ( p, -1 )
      p = math.min ( p, 1 )
      local i = ( scrollbar3.slider.value - 51 ) / 50
      i = math.max ( i, -1 )
      i = math.min ( i, 1 )
      
      
      player:stop ( )
      asset:unload ( )
      table.insert(logger, "asset = Music()")
      asset = Music ( )
      assert ( asset:load ( file ) == true, "Couldn't load music:" .. file )
      table.insert(logger, "player = audio:play(asset, " .. v .. ", " .. p .. ", "  .. i .. ")")
      player = audio:play ( asset, v, p, i )
      player:stop ( )
    end

    button7.on_click = function ( self )
      local v = ( scrollbar1.slider.value - 1 ) / 100
      v = math.max ( v, 0 )
      v = math.min ( v, 1 )
      local p = ( scrollbar2.slider.value - 51 ) / 50
      p = math.max ( p, -1 )
      p = math.min ( p, 1 )
      local i = ( scrollbar3.slider.value - 51 ) / 50
      i = math.max ( i, 0 )
      i = math.min ( i, 1 )
      
      player:stop ( )
      asset:unload ( )
      table.insert(logger, "asset = Sound()")
      asset = Sound ( )
      assert ( asset:load ( file ) == true, "Couldn't load sound:" .. file )
      table.insert(logger, "player = audio:play(asset, " .. v .. ", " .. p .. ", "  .. i .. ")")
      player = audio:play ( asset, v, p, i )
      player:stop ( )
    end
    
    local button8 = gui.Button ( 0, 530, "END TEST" )
    button8.on_click = function ( self )
      player:stop()
      asset:unload()
      gui.destroy()
      grab_keyboard = true
      logger = nil
      clear_log()
      begin_test()
    end
    cd:add_child ( button8 )
end

function begin_test()
  set_resolution(640, 480)
  print_log("player test\n===========\nClick to begin the test", -320, 240 - 20, WHITE)
  mouse.on_press = function(mouse)
    clear_log()
    mouse.on_press = nil
    initgui()
  end
end

function end_test()
  clear_log()
  mouse.on_press = nil
end

function update_test()
  if logger then
    clear_log()
    local s = math.max(#logger - 10, 1)
    for i = s, #logger do
      print_log(logger[i], -320, 240 - 20 - (i - s)*10)
    end
  end
end
