local function initgui()
    grab_keyboard = false
    custom_timer = Timer()
    on_ticks = 0

    require ( "gui.main" )
    require ( "gui.obj.label" )
    require ( "gui.obj.button" )
    require ( "gui.obj.slider" )
    require ( "gui.obj.arrow" )
    require ( "gui.obj.scrollbar" )
    require ( "gui.obj.checkbox" )
    gui.create ( )

    local cd = gui.Container ( 150, 50, 300, 500 )
    gui.container:add_child ( cd )

    local intervall = gui.Label ( 0, 0, "timer.interval:" .. custom_timer.interval, 300 )
    cd:add_child ( intervall )

    interval = gui.Label ( 0, 80, "on_tick called:" .. on_ticks, 300 )
    cd:add_child ( interval )
    delta = gui.Label ( 0, 100, "delta: ", 300 )
    cd:add_child ( delta )
    deltams = gui.Label ( 0, 120, "deltams: ", 300 )
    cd:add_child ( deltams )
    elapsed = gui.Label ( 0, 140, "elapsed: ", 300 )
    cd:add_child ( elapsed )

    recur = gui.Checkbox ( 0, 240, "recur: false" )
    cd:add_child ( recur )
    
    recur.on_change = function ( recur, v )
      custom_timer.recur = v
    end
    
    paused = gui.Checkbox ( 0, 260, "paused: true", true )
    cd:add_child ( paused )
    
    paused.on_change = function ( paused, v )
      custom_timer.paused = v
    end

    local scrollbar = gui.Scrollbar ( 0, 20, 300, 15, 1001, true )
    scrollbar.slider.value = 1
    cd:add_child ( scrollbar )

    scrollbar.slider.on_change = function ( self )
      local p = ( self.value - 1 ) / 1000
      p = math.max ( p, 0 )
      p = math.min ( p, 1 )
      p = math.floor ( p * (1000) )
      custom_timer.interval = p
      intervall.text = "timer.interval:" .. custom_timer.interval
      paused.text = "paused: false"
      if custom_timer.paused == true then
        paused.text = "paused: true"
      end
      paused.is_checked = custom_timer.paused
    end

    local start = gui.Button ( 0, 300, "start" )
    start.on_click = function ( self )
      custom_timer:start()
      paused.text = "paused: false"
      if custom_timer.paused == true then
        paused.text = "paused: true"
      end
      paused.is_checked = false
    end
    cd:add_child ( start )
    
    local stop = gui.Button ( 50, 300, "stop" )
    stop.on_click = function ( self )
      custom_timer:stop()
      paused.text = "paused: false"
      if custom_timer.paused == true then
        paused.text = "paused: true"
      end
      paused.is_checked = true
    end
    cd:add_child ( stop )
    
    local pause = gui.Button ( 100, 300, "pause" )
    pause.on_click = function ( self )
      custom_timer:pause()
      paused.text = "paused: false"
      if custom_timer.paused == true then
        paused.text = "paused: true"
      end
      paused.is_checked = true
    end
    cd:add_child ( pause )


    custom_timer.on_tick = function ( t )
      on_ticks = on_ticks + 1
    end
    

    local endb = gui.Button ( 0, 400, "END TEST" )
    endb.on_click = function ( self )
      custom_timer:stop()
      custom_timer = nil
      gui.destroy()
      grab_keyboard = true
      interval = nil
      recur = nil
      paused = nil
      delta = nil
      deltams = nil
      elapsed = nil
      begin_test()
    end
    cd:add_child ( endb )
end

function begin_test()
  set_resolution(640, 480)
  print_log("timer test\n===========\nClick to begin the test", -320, 240 - 20, WHITE)
  mouse.on_press = function(mouse)
    clear_log()
    mouse.on_press = nil
    initgui()
  end
end

function end_test()
  clear_log()
  mouse.on_press = nil
end

function update_test()
  if interval then
    interval.text = "on_tick called:" .. on_ticks
    recur.text = "recur: false"
    if custom_timer.recur == true then
      recur.text = "recur: true"
    end
    paused.text = "paused: false"
    if custom_timer.paused == true then
      paused.text = "paused: true"
    end
    local d = custom_timer:get_delta()
    --assert(d >= 1)
    delta.text = "delta: " .. d
    local ms = custom_timer:get_delta_ms()
    --assert(ms >= timer.interval)
    deltams.text = "deltams: " .. ms
    elapsed.text = "elapsed: " .. custom_timer:get_elapsed()
  end
end