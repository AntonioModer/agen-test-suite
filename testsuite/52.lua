function begin_test()
  set_resolution(640, 480)
  s1 = Sprite(-50, 40)
  s1.canvas:rectangle(10, 60)
  s1.canvas:fill()
  display.viewport:add_child(s1)
  s2 = Sprite(50, 40)
  s2.canvas:rectangle(10, 60)
  s2.canvas:fill()
  display.viewport:add_child(s2)
  s3 = Sprite(-50, -150)
  s3.canvas:rectangle(10, 60)
  s3.canvas:fill()
  display.viewport:add_child(s3)
  s4 = Sprite(50, -150)
  s4.canvas:rectangle(10, 60)
  s4.canvas:fill()
  display.viewport:add_child(s4)
  ang = 0
end

function end_test()
  display.viewport:remove_child(s1)
  display.viewport:remove_child(s2)
  display.viewport:remove_child(s3)
  display.viewport:remove_child(s4)
  s1 = nil
  s2 = nil
  s3 = nil
  s4 = nil
  ang = nil
end

function update_test(dt)
  clear_log()
  print_log("Rotation test\n=============", -320, 240 - 20, WHITE)

  local dtang = (dt/1000)*10
  
  ang = ang + dtang
  s1.rotation = s1.rotation + dtang
  s2.rotation = ang
  s3:change_rotation(dtang)
  s4:set_rotation(ang)
  
  print_log(" s1.rotation = s1.rotation + dt\n print(s1.rotation) => " .. s1.rotation, -300, 120, WHITE)
  print_log(" ang = ang + dt\n print(ang) => " .. ang .. "\n s2.rotation = ang\n print(s2.rotation) => " .. s2.rotation, 0, 120, WHITE)

  if s2.rotation ~= ang then
    print_log(" ERROR: 's2.rotation' != 'ang'", 0, -30, RED)
  end
  print_log(" s3:change_rotation(dt)\n print(s3.rotation) => " .. s3.rotation, -300, -80, WHITE)
  if s3.rotation ~= ang then
    print_log(" ERROR: 's3.rotation' != 'ang'", -300, -200, RED)
  end
  print_log(" s4:set_rotation(ang)\n print(s4.rotation) => " .. s4.rotation, 0, -80, WHITE)
  if s4.rotation ~= ang then
    print_log(" ERROR: 's4.rotation' != 'ang'", 0, -200, RED)
  end
end