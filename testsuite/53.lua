function begin_test()
  set_resolution(640, 480)
  s1 = Sprite(-50, 40)
  s1.canvas:rectangle(10, 60)
  s1.canvas:fill()
  display.viewport:add_child(s1)
  s2 = Sprite(50, 40)
  s2.canvas:rectangle(10, 60)
  s2.canvas:fill()
  display.viewport:add_child(s2)
  s3 = Sprite(-50, -150)
  s3.canvas:rectangle(10, 60)
  s3.canvas:fill()
  display.viewport:add_child(s3)
  s4 = Sprite(50, -150)
  s4.canvas:rectangle(10, 60)
  s4.canvas:fill()
  display.viewport:add_child(s4)
  scl = 1
end

function end_test()
  display.viewport:remove_child(s1)
  display.viewport:remove_child(s2)
  display.viewport:remove_child(s3)
  display.viewport:remove_child(s4)
  s1 = nil
  s2 = nil
  s3 = nil
  s4 = nil
  scl = nil
end

function update_test(dt)
  clear_log()
  print_log("Horizontal scaling test\n=======================", -320, 240 - 20, WHITE)

  local dtscl = (dt/1000)*2
  
  scl = (scl + dtscl)%4
  if scl < 1 then
    scl = scl + 1
  end
  s1.scalex = (s1.scalex + dtscl)%4
  if s1.scalex < 1 then
    s1.scalex = s1.scalex + 1
  end
  s2.scalex = scl
  s3:change_scalex(dtscl)
  s3.scalex = s3.scalex%4
  if s3.scalex < 1 then
    s3.scalex = s3.scalex + 1
  end
  s4:set_scale(scl, 1)
  
  print_log(" s1.scalex = s1.scalex + dt\n print(s1.scalex) => " .. s1.scalex, -300, 120, WHITE)
  print_log(" scl = scl + dt\n print(scl) => " .. scl .. "\n s2.scalex = scl\n print(s2.scalex) => " .. s2.scalex, 0, 120, WHITE)
  
  if s2.scalex ~= scl then
    print_log(" ERROR: 's2.scalex' != 'scl'", 0, -30, RED)
  end
  print_log(" s3:change_scalex(dt)\n print(s3.scalex) => " .. s3.scalex, -300, -80, WHITE)
  if s3.scalex ~= scl then
    print_log(" ERROR: 's3.scalex' != 'scl'", -300, -200, RED)
  end
  print_log(" s4:set_scale(scl, 1)\n print(s4.scalex) => " .. s4.scalex, 0, -80, WHITE)
  if s4.scalex ~= scl then
    print_log(" ERROR: 's4.scalex' != 'scl'", 0, -200, RED)
  end
end