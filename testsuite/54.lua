function begin_test()
  set_resolution(640, 480)
  s1 = Sprite(-50, 40)
  s1.canvas:rectangle(10, 60)
  s1.canvas:fill()
  display.viewport:add_child(s1)
  s2 = Sprite(50, 40)
  s2.canvas:rectangle(10, 60)
  s2.canvas:fill()
  display.viewport:add_child(s2)
  s3 = Sprite(-50, -150)
  s3.canvas:rectangle(10, 60)
  s3.canvas:fill()
  display.viewport:add_child(s3)
  s4 = Sprite(50, -150)
  s4.canvas:rectangle(10, 60)
  s4.canvas:fill()
  display.viewport:add_child(s4)
  scl = 1
end

function end_test()
  display.viewport:remove_child(s1)
  display.viewport:remove_child(s2)
  display.viewport:remove_child(s3)
  display.viewport:remove_child(s4)
  s1 = nil
  s2 = nil
  s3 = nil
  s4 = nil
  scl = nil
end

function update_test(dt)
  clear_log()
  print_log("Vertical scaling test\n======================", -320, 240 - 20, WHITE)

  local dtscl = (dt/1000)
  
  scl = (scl + dtscl)%2
  if scl < 1 then
    scl = scl + 1
  end
  s1.scaley = (s1.scaley + dtscl)%2
  if s1.scaley < 1 then
    s1.scaley = s1.scaley + 1
  end
  s2.scaley = scl
  s3:change_scaley(dtscl)
  s3.scaley = s3.scaley%2
  if s3.scaley < 1 then
    s3.scaley = s3.scaley + 1
  end
  s4:set_scale(1, scl)
  
  print_log(" s1.scaley = s1.scaley + dt\n print(s1.scaley) => " .. s1.scaley, -300, 120, WHITE)
  print_log(" scl = scl + dt\n print(scl) => " .. scl .. "\n s2.scalex = scl\n print(s2.scaley) => " .. s2.scaley, 0, 120, WHITE)
  
  if s2.scaley ~= scl then
    print_log(" ERROR: 's2.scaley' != 'scl'", 0, -30, RED)
  end
  print_log(" s3:change_scaley(dt)\n print(s3.scaley) => " .. s3.scaley, -300, -80, WHITE)
  if s3.scaley ~= scl then
    print_log(" ERROR: 's3.scaley' != 'scl'", -300, -200, RED)
  end
  print_log(" s4:set_scale(scl, 1)\n print(s4.scaley) => " .. s4.scaley, 0, -80, WHITE)
  if s4.scaley ~= scl then
    print_log(" ERROR: 's4.scaley' != 'scl'", 0, -200, RED)
  end
end