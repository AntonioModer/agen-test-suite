function begin_test()
  set_resolution(640, 480)
  s1 = Sprite(-50, 40)
  s1.canvas:rectangle(10, 60)
  s1.canvas:fill()
  display.viewport:add_child(s1)
  s2 = Sprite(50, 40)
  s2.canvas:rectangle(10, 60)
  s2.canvas:fill()
  s2.visible = false
  display.viewport:add_child(s2)
  elapsed = 0
end

function end_test()
  display.viewport:remove_child(s1)
  display.viewport:remove_child(s2)
  s1 = nil
  s2 = nil
  elapsed = nil
end

function update_test(dt)
  elapsed = elapsed + dt/1000
  if elapsed > 1 then
    elapsed = elapsed%1
    s1.visible = not s1.visible
    s2.visible = not s2.visible
  end
  
  clear_log()
  print_log("Visible property test\n=====================", -320, 240 - 20, WHITE)

  local s1b = "?"
  if s1.visible == false then
    s1b = "false"
  elseif s1.visible == true then
    s1b = "true"
  end
  local s2b = "?"
  if s2.visible == false then
    s2b = "false"
  elseif s2.visible == true then
    s2b = "true"
  end
  print_log(" print(s1.visible) => " .. s1b, -300, 120, WHITE)
  print_log(" print(s2.visible) => " .. s2b, 0, 120, WHITE)
end