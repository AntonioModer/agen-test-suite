local function generate()
  local a = 0
  local v = {}
  local r = math.random(30) + 3
  for i = 1, r do
    a = a + math.rad(r)
    if a > math.pi*2 then
      break
    end
    local d = math.random(10, 200)
    local vx = math.cos(a)*d
    local vy = math.sin(a)*d
    table.insert(v, { x = vx, y = vy })
  end
  return v
end

function begin_test()
  set_resolution(640, 480)
  s1 = Sprite(0, 0)
  display.viewport:add_child(s1)
  s2 = Sprite(0, 0)
  display.viewport:add_child(s2)
  s3 = Sprite(0, 0)
  display.viewport:add_child(s3)
  require("Box2D")
  font = Font()
  font:load_system("Arial", 8)
  
  function mouse:on_press(b)
    
    local c = s1.canvas
    c:clear()
    local v = generate()
    c:move_to(v[1].x, v[1].y)
    for i = 2, #v do
      c:line_to(v[i].x, v[i].y)
    end
    c:close_path()
    c:set_line_style(4, WHITE, 1)
    c:stroke_preserve()
    c:set_fill_style(WHITE, 0.25)
    c:fill()

    local out = {}
    local ret = b2.Fill(v, out)
    if ret == true then
      for i = 1, #out - 2, 3 do
        c:move_to(out[i].x, out[i].y)
        c:line_to(out[i + 1].x, out[i + 1].y)
        c:line_to(out[i + 2].x, out[i + 2].y)
        c:close_path()
        c:set_line_style(2, RED, 1)
        c:stroke()
      end
    end
    triangles = out
    
    c:set_font(font, LIME)
    c:move_to(v[1].x, v[1].y)
    c:square(5)
    c:rel_move_to(5, 5)
    c:write(1)
    for i = 2, #v do
      c:move_to(v[i].x, v[i].y)
      c:square(5)
      c:rel_move_to(5, 5)
      c:write(i)
    end
    c:set_fill_style(LIME, 1)
    c:fill()
  end
  
  mouse:on_press(1)
  clear_log()
  print_log("b2.fill test\n============", -320, 220, WHITE)
end

function end_test()
  display.viewport:remove_child(s1)
  s1 = nil
  display.viewport:remove_child(s2)
  s2 = nil
  display.viewport:remove_child(s3)
  s3 = nil
  font:unload()
  font = nil
  triangles = nil
  
  mouse.on_press = nil
end

local function cross(ax, ay, bx, by)
  return ay*bx - ax*by
end

local function point_in_triangle(px, py, ax, ay, bx, by, cx, cy)
  local pab = cross(px - ax, py - ay, bx - ax, by - ay)
  local pbc = cross(px - bx, py - by, cx - bx, cy - by)
  if (pab < 0 and pbc >= 0) or (pab >= 0 and pbc < 0) then
    return false
  end
  local pca = cross(px - cx, py - cy, ax - cx, ay - cy)
  if (pab < 0 and pca >= 0) or (pab >= 0 and pca < 0) then
    return false
  end
  return true
end

function update_test(dt)
  local c = s2.canvas
  c:clear()
  local x, y = mouse.xaxis, mouse.yaxis
  local out = triangles
  for i = 1, #out - 2, 3 do
    local ax, ay = out[i].x, out[i].y
    local bx, by = out[i + 1].x, out[i + 1].y
    local cx, cy = out[i + 2].x, out[i + 2].y
    if point_in_triangle(x, y, ax, ay, bx, by, cx, cy) then
      c:move_to(ax, ay)
      c:line_to(bx, by)
      c:line_to(cx, cy)
      c:close_path()
      c:set_fill_style(LIME, 1)
      c:fill()
    end
  end
end