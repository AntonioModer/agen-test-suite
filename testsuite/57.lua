function begin_test()
end

function end_test()
end

function update_test(dt)
  clear_log()
  local l1 = system.get_locale()
  local l2 = system.get_private_dir()
  local l3 = system.get_ticks()
  print_log(" system functions\n ================\n LOCALE: " .. l1 .. "\n PRIVATE DIR: " .. l2 .. "\n TICKS: " .. l3, -320, 220, WHITE)

end