function begin_test()

  if mtouch == nil then
    -- emulate mtouch via the mouse
    mtouch = {}
    mtouch.emulated = true
    mtouch.on_begin = function(mtouch, id, x, y)
    end
    mtouch.on_end = function(mtouch, id, x, y)
    end
    --mtouch.on_move = function(mtouch, id, dx, dy)
    -- temporary switch to absolute positions
    mtouch.on_move = function(mtouch, id, x, y)
    end
    mtouch.on_cancel = function(mtouch, id)
    end

    if mouse then
      -- redirect all mouse input to mtouch
      mouse.on_press = function(mouse, b)
        mtouch:on_begin(1, mouse.xaxis, mouse.yaxis)
      end
      mouse.on_release = function(mouse, b)
        mtouch:on_end(1, mouse.xaxis, mouse.yaxis)
      end
      mouse.on_move = function(mouse, dx, dy)
        --mtouch:on_move(1, dx, dy)
        -- temporary switch to absolute positions
        mtouch:on_move(1, mouse.xaxis, mouse.yaxis)
      end
    end
  else
    display:multitouch_enable();
  end
  
  touches = {}
  mtouch.on_begin = function(mtouch, id, x, y)
    touches[id] = { { x, y } }
  end
  mtouch.on_end = function(mtouch, id, x, y)
    touches[id] = nil
  end
  mtouch.on_move = function(mtouch, id, x, y)
    if touches[id] then
      table.insert(touches[id], { x, y } )
    end
  end
  mtouch.on_cancel = function(mtouch, id)
    touches[id] = nil
  end
  sprite = Sprite()
  display.viewport:add_child(sprite)
  colors = { WHITE, RED, GREEN, BLUE, YELLOW, PINK, GOLD, GRAY, MAROON, LIME }
end

function end_test()
  touches = nil
  mtouch.on_begin = nil
  mtouch.on_end = nil
  mtouch.on_move = nil
  mtouch.on_cancel = nil
  if mtouch.emulated == true then
    mtouch = nil
    if mouse then
      mouse.on_press = nil
      mouse.on_release = nil
      mouse.on_move = nil
    end
  end
  display.viewport:remove_child(sprite)
  sprite = nil
  colors = nil
end

function update_test(dt)
  clear_log()
  print_log("Multitouch test\n===============", -320, 220, WHITE)

  local c = sprite.canvas
  c:clear()
  local i = 1
  for _, v in pairs(touches) do
    c:set_line_style(2, colors[i], 1)
    c:move_to(v[1][1], v[1][2])
    c:circle(5)
    c:set_fill_style(colors[i], 1)
    c:fill()
    print_log(_, v[1][1] + 5, v[1][2] + 5, colors[i])
    for j = 2, #v do
      c:line_to(v[j][1], v[j][2])
    end
    c:stroke()
    i = i + 1
    if i > #colors then
      i = 1
    end
  end
end
