function begin_test()
  elapsed = 0
  autozoom = true
  scene = Layer()
  sprite = Sprite()
  for i = 1, 100 do
    local r = i/100
    sprite.canvas:square(r*r*400)
    sprite.canvas:set_line_style(r, WHITE, 1)
    sprite.canvas:stroke()
  end
  scene:add_child(sprite)
  scene:add_child(display.viewport.camera)
  mouse.on_press = function(m, b)
    autozoom = not autozoom
  end
  mouse.on_wheelmove = function(m, z)
    autozoom = false
    if z < 0 then
      z = 0.9
    else
      z = 1.1
    end
    local s = display.viewport.camera.scalex*z
    display.viewport.camera:set_scale(s, s)
  end
end

function end_test()
  elapsed = nil
  scene:remove_children()
  scene = nil
  autozoom = nil
  mouse.on_press = nil
  mouse.on_wheelmove = nil
  
  display.viewport.camera:set_position(0, 0)
  display.viewport.camera:set_rotation(0)
  display.viewport.camera:set_scale(1, 1)
end

function update_test(dt)
  clear_log()
  local s = display.viewport.camera.scalex
  print_log("Camera zoom test\n================\nmouse buttons - autozoom\nmouse wheel - manual zoom\nzoom: " .. s, -320, 220, RED)

  if autozoom == false then
    return
  end
  elapsed = elapsed + dt
  local r = ((elapsed/1000)%10)/10
  r = math.sin(r*math.pi)
  assert(r >= 0 and r <= 1)
  local s = 1 - r
  display.viewport.camera:set_scale(s, s)
end