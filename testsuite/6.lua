local image = Image()
local sprite = Sprite()
local sprites = {}

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local sz = string.format("%s/16x16square.png", dir)
  image:load(sz)

  display.viewport:add_child(sprite)
  for i = 0, 31 do
    for j = 0, 31 do
      local x, y = 16*i + 8, 16*j + 8
      x, y = x - 256, y - 256
      if i < 15 then
        sprite.canvas:set_source_image(image, x, y)
        sprite.canvas:paint()
      else
        local s = Sprite(x, y)
        s.canvas:set_source_image(image)
        s.canvas:paint()
        display.viewport:add_child(s)
        table.insert(sprites, s)
      end
    end
  end


  local sz =
[[set_source_image() vs set_position() test
=========================================
The left/right sides of the screen are drawn in different ways.
Left: drawn using "set_source_image"
Right: drawn using "sprite:set_position"]]
  print_log(sz, -256, 0)
end

function end_test()
  sprite.canvas:clear()
  while #sprites > 0 do
    local v = table.remove(sprites)
    v.canvas:clear()
    display.viewport:remove_child(v)
  end
  display.viewport:remove_child(sprite)
  image:unload()
  clear_log()
end

function update_test()

end