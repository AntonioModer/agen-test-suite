function begin_test()
  sprite = Sprite()
  display.viewport:add_child(sprite)
  font = Font()
  font:load_system("arial", 24)
  sprite.canvas:set_font(font, WHITE)
  sz = "This is a test string"
  strings = { "", "a b c d e f", "This is another test string", "ALL CAPS", " ! ! ! ", "This\nstring\nuses\nthe\nnewline\ncharacter" }
  mouse.on_press = function(m, b)
    sz = strings[ math.random(#strings) ]
  end
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  sprite = nil
  sz = nil
  strings = nil
  font:unload()
  font = nil
  mouse.on_press = nil
end

function update_test(dt)
  clear_log()
  print_log("Font metrics test\n=================\nPress mouse to change string", -320, 220, RED)

  local w = font:get_width(sz)
  local h = font:get_height()
  local s = font:get_size()
  local c = sprite.canvas
  c:clear()
  c:set_font(font)
  c:move_to(0, 0)
  c:write(sz)
  c:line_to(w, 0)
  c:set_line_style(2, RED, 1)
  c:stroke()
  c:move_to(0, 0)
  c:line_to(0, h)
  c:set_line_style(2, BLUE, 1)
  c:stroke()
  c:move_to(-4, s)
  c:line_to(w, s)
  c:set_line_style(2, LIME, 1)
  c:stroke()
  
  print_log("width:" .. w, 0, -10, RED)
  print_log("height:" .. h, -80, 10, BLUE)
  print_log("size:" .. s, -80, 20, LIME)
  
  if string.len(sz) == 0 then
    print_log("warning: empty string", 0, -20, RED)
  end
end