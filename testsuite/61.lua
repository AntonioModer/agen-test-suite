function begin_test()
  scene = Layer ( )
  camera = display.viewport.camera
  
  SIZE = 100
  SPACING = 150
  XCOUNT = 300
  YCOUNT = 200

  -- create a shared canvas for instancing
  SHARED = Canvas ( )
  SHARED:square ( SIZE )
  SHARED:set_fill_style ( ORANGE, 1 )
  SHARED:fill ( )

  -- generate an array of sprites
  for x = 0, XCOUNT do
    for y = 0, YCOUNT do
      local x = x * SPACING - ( XCOUNT / 2 * SPACING )
      local y = y * SPACING - ( YCOUNT / 2 * SPACING )
      local temp = Sprite ( x, y )
      temp.canvas = SHARED 
      scene:add_child ( temp )
    end
  end
  scene:add_child ( camera )
  
  -- allow zooming the camera via the wheel
	mouse.on_wheelmove = function ( mouse, z )
		if ( z < 0 ) then
			z = 1.1
		else
			z = 0.9
		end
		camera:change_scale ( z, z )
	end

	-- allow moving the camera via the mouse
	mouse.on_move = function ( mouse, x, y )
		if mouse:is_down ( MBUTTON_RIGHT ) then
			local r = -math.rad ( camera.rotation )

			local cos_d = math.cos ( r )
			local sin_d = math.sin ( r )

			local tx = cos_d * x - sin_d * y
			local ty = cos_d * y + sin_d * x

			tx = tx * camera.scalex
			ty = ty * camera.scaley
			camera:change_position ( -tx, -ty )
		end
		if mouse:is_down ( MBUTTON_LEFT ) then
			camera.rotation = camera.rotation + y / 2 + x / 2
		end
	end
end

function end_test()
  scene:remove_children()
  scene = nil
  camera:set_position(0, 0)
  camera:set_rotation(0)
  camera:set_scale(1, 1)
  camera = nil
  SHARED:clear()
  SHARED = nil
  
  mouse.on_wheelmove = nil
  mouse.on_move = nil
  SIZE = nil
  SPACING = nil
  XCOUNT = nil
  YCOUNT = nil
end

function update_test(dt)
  clear_log()
  print_log("Culling test\n=================\n", -320, 220, RED)
  print_log("Delta: " .. dt, -320, 200, RED)

end