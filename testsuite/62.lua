require ('LuaXml')

local aman = {}

function begin_test()
	--
	-- setup asset loader
	--
	aman = agen.AssetManager ()
	aman.status = 'loading'
	aman.progres = 0
	--
	-- level data
	--
	local levelAssets = {
		{type = 'image', file = 'testsuite/512x512scaling.png'},
		{type = 'image', file = 'testsuite/512x512squares.png'},
		{type = 'image', file = 'testsuite/512x512squares32.png'},
		{type = 'image', file = 'testsuite/512x512fonts.png'},
		{type = 'image', file = 'testsuite/tiles/1.bmp'},
		{type = 'image', file = 'testsuite/tiles/2.bmp'},
		{type = 'image', file = 'testsuite/tiles/3.bmp'},
		{type = 'image', file = 'testsuite/tiles/4.bmp'},
		{type = 'image', file = 'testsuite/tiles/5.bmp'},
		{type = 'image', file = 'testsuite/tiles/6.bmp'},
		{type = 'image', file = 'testsuite/tiles/7.bmp'},
		{type = 'sound', file = 'testsuite/pitch/0.wav'},
		{type = 'sound', file = 'testsuite/pitch/1.wav'},
		{type = 'sound', file = 'testsuite/pitch/b0.wav'},
		{type = 'sound', file = 'testsuite/pitch/b100.wav'},
		{type = 'sound', file = 'testsuite/acapella.wav'},
		{type = 'font',  file = 'testsuite/apple.ttf', size = 14},
		{type = 'font',  file = 'testsuite/apple.ttf', size = 18},
		{type = 'font',  file = 'testsuite/apple.ttf', size = 22},
		{type = 'font',  file = 'testsuite/apple.ttf', size = 26},
		{type = 'text',  file = 'testsuite/box2d.xml'},
	}
	--
	-- will populate aman.images, aman.sounds and aman.fonts
	--
	if not aman:loadLevel (levelAssets)
	then
		aman.status = 'failed'
	else
		--
		-- set completion notifier
		--
		aman.on_load_complete = function (self)
			aman.status = 'done'
		end
		--
		-- kick off
		--
		aman:run ()
	end
end

function end_test()
	--
	-- NOTE: aman keeps ref for each asset
	--
	
	-- force unref now
	aman:unloadAll ()
	aman = nil
end

function update_test(dt)
	clear_log()
	print_log("Asynchronous asset loader test\n=================\n", -320, 220, WHITE)
	--
	-- update progress
	--
	aman.progress = 0
	for _, asset in pairs (aman.assets)
	do
		if asset.status == 'loaded'
		then
			aman.progress = aman.progress + 1
		end
	end

	print_log ('Loaded:' .. aman.progress .. " files", -320, 200, WHITE)
	print_log ('Status:' .. aman.status, -320, 180, WHITE)
end
