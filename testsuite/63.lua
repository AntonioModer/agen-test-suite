require ('sworks')
--
-- setup object / callbacks
--
local lboard = sworks.LeaderBoard ()
lboard.status = 'not connected'
lboard.timeToUpload = 0

lboard.onFound = function (self, status)
	if status
	then
		self.status = 'found'
	else
		self.status = 'not found'
	end
end

lboard.onDownloaded = function (self, status, entries)
	if not status
	then
		self.status = 'failed to download'
		return
	end

	self.status = 'loaded'
	self.table = entries
end

lboard.onUploaded = function (self, status, score, scoreChanged, newRank, oldRank)
	if not status
	then
		self.status = 'failed to upload'
	else
		-- force new download instead of updating
		-- the table locally
		self.status = 'found'
	end
end

function begin_test()
	--
	-- steam client running / logged in
	--
	if sworks.isInitialized
	then
		lboard.status = 'not found'
	end
end

function end_test()

	lboard = nil
end

function update_test(dt)
	clear_log()
	print_log("Steamworks integration test\n=================\n", -320, 220, WHITE)
	print_log("Status:" .. lboard.status, -320, 200, WHITE)
	--
	-- process async Steam tasks
	--
	sworks.update ()

	lboard.timeToUpload = lboard.timeToUpload + dt

	if lboard.status == 'not found'
	then
		if not lboard:Find ('Nuclear Threats (easy/single)')
		then
			print_log("Failed to send find request\n", -320, 180, RED)
		else
			lboard.status = 'busy'
		end
	elseif lboard.status == 'found'
	then
		-- types: global, aroundUser, friends, users
		if not lboard:DownloadEntries ('global', 1, 10)
		then
			print_log("Failed to send download request\n", -320, 180, RED)
		else
			lboard.status = 'busy'
		end
	elseif lboard.status == 'loaded'
	then
		for i = 1, lboard:GetEntryCount ()
		do
			local num = 180 - (i * 20)
			--
			-- create user object from userID
			--
			local user = sworks.SteamUser (lboard.table [i].userID)
			--
			-- create agen image for avatar
			--
			user.avatar = Sprite (-320, num)
			display.viewport:add_child (user.avatar)
			
			local avatar32 = user:GetAvatar32x32 ()
			if avatar32
			then
				local img = Image ()
				if img:load_memory (IMG_FORMAT_R8G8B8A8, avatar32.width, avatar32.height, avatar32.data)
				then
					user.avatar.canvas:set_source_image (img)
					user.avatar.canvas:paint ()
				end
			end
			
			local line = string.format("%d. %s....%d", 
				lboard.table [i].rank,
				user.name,
				lboard.table [i].score)
			print_log(line, -280, num, WHITE)
		end
	end
	--
	-- post new score every 10 secs
	--
	if lboard.timeToUpload > 10000
	then
		lboard.timeToUpload = 0
		if not lboard:UploadScore ('replace', math.random (100), nil)
		then
			print_log("Failed to send upload request\n", -320, 180, RED)
		end
	end
end
