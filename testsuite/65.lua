function begin_test()
	--
	-- a layer where all sprites share the same canvas
	--
	local batch = SpriteBatch ()
	batch.canvas:square (20)
	batch.canvas:set_fill_style (RED, 0.7)
	batch.canvas:fill ()

	for i = 1, 1000, 1
	do
		local sprite = Sprite (math.random (640) - 320, math.random (480) - 240)
		batch:add_child (sprite)
	end

  layer1 = Layer()
  layer1:add_child(batch)
	display.viewport:add_child (layer1)
end

function end_test()

	display.viewport:remove_child (layer1)
  layer1 = nil
end

function update_test(dt)

	clear_log()
	print_log("Instanced rendering test\n=================\n", -320, 220, WHITE)
end
