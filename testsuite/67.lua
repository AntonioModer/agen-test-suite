function begin_test()
  sprite = Sprite(-300, 0)
  display.viewport:add_child(sprite)
  font = Font()
  local codes = {}
  -- punctuation
  for i = 0x20, 0x40 do
    table.insert (codes, i)
  end
  -- latin
  for i = 0x40, 0xff do
    table.insert (codes, i)
  end
  -- cyrillic
  for i = 0x0400, 0x0450 do
    table.insert (codes, i)
  end
  -- hiragana
  for i = 0x3040, 0x319f do
    table.insert (codes, i)
  end
  -- kanji
  for i = 0x5200, 0x5225 do
    table.insert (codes, i)
  end
  table.insert (codes, 0x6587)
  table.insert (codes, 0x3001)
  table.insert (codes, 0x5b57)

  -- greek
  for i = 0x0370, 0x03ff do
    table.insert (codes, i)
  end
  font:load_file_codes("testsuite/OpenSans-Regular.ttf", 24, codes)
  sprite.canvas:set_font(font, WHITE)
  sz = "Това е стринг"
  strings = { "Ez egy másik teszt szöveg", "これは、別のテスト文字列である", "Esta é uma outra seqüência de teste", "Αυτό είναι μια άλλη δοκιμή string" }
  mouse.on_press = function(m, b)
    sz = strings[ math.random(#strings) ]
  end
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  sprite = nil
  sz = nil
  strings = nil
  font:unload()
  font = nil
  mouse.on_press = nil
end

function update_test(dt)
  clear_log()
  print_log("Font metrics test\n=================\nPress mouse to change string", -320, 220, RED)

  local w = font:get_width(sz)
  local h = font:get_height()
  local s = font:get_size()
  local c = sprite.canvas
  c:clear()
  c:set_font(font)
  c:move_to(0, 0)
  c:write(sz)
  c:line_to(w, 0)
  c:set_line_style(2, RED, 1)
  c:stroke()
  c:move_to(0, 0)
  c:line_to(0, h)
  c:set_line_style(2, BLUE, 1)
  c:stroke()
  c:move_to(-4, s)
  c:line_to(w, s)
  c:set_line_style(2, LIME, 1)
  c:stroke()
  
  print_log("width:" .. w, 0, -10, RED)
  print_log("height:" .. h, -80, 10, BLUE)
  print_log("size:" .. s, -80, 20, LIME)
  
  if string.len(sz) == 0 then
    print_log("warning: empty string", 0, -20, RED)
  end
end
