require 'unicode'

local sprite = Sprite()
local font = Font()

function begin_test()
  set_resolution(640, 480)

  display.viewport:add_child(sprite)
  
  font:load_system("Arial", 12)
  
  sprite.canvas:move_to(0, -160)
  sprite.canvas:rectangle(640, 320)
  sprite.canvas:fill()
  
  for i = 0, 255 do
    local char = unicode.utf8.char(i)
    local x = i%32
    local y = math.floor(i/32)
    sprite.canvas:set_font(font, WHITE, 1)
    sprite.canvas:move_to(x * 20 - 320, y * -20 + 240 - 20)
    sprite.canvas:write(char)
    sprite.canvas:set_font(font, BLACK, 1)
    sprite.canvas:move_to(x * 20 - 320, y * -20 - 240 + 20*8)
    sprite.canvas:write(char)
  end

  local sz =
[[font calibration test
=====================
Using "load_system" with Arial 12 on black & white background]]
  print_log(sz, -320, 40)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  font:unload()
  clear_log()
end

function update_test()

end
