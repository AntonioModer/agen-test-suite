function begin_test()
  sprite = Sprite(0, 0)
  display.viewport:add_child(sprite)
  local txts = { "bg-wall.png", "bg-sunset.png", "bg-rocky.png" }
  imgs = {}
  for i, v in ipairs(txts) do
    local img = Image("testsuite/tiling/" .. v)
    table.insert(imgs, img)
  end
  set_resolution(640, 480)
  
  mouse.on_press = function(mouse, b)
    local img = imgs[math.random(#imgs)]
    sprite.canvas:clear()
    -- hacky way to tile any image over the screen
    sprite.canvas:set_source_subimage(img, 0, 0, 640, 480)
    sprite.canvas:paint()
  end
  mouse:on_press()
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  sprite = nil
  for i, v in ipairs(imgs) do
    v:unload()
  end
  imgs = nil
  mouse.on_press = nil
end

function update_test(dt)
  clear_log()
  print_log("Texture subimage tiling\n=================\nPress mouse to change texture", -320, 220, RED)

end
