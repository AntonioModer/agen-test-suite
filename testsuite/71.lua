function begin_test()
  set_resolution(640, 480)

  dm = DisplayMode()
  mouse.on_press = function(mouse, b)
    if b == MBUTTON_LEFT then
      local rdm = display.modes[math.random(#display.modes)]
      rdm.windowed = dm.windowed
      display:set_mode(rdm)
    elseif b == MBUTTON_RIGHT then
      if display:get_mode(dm) then
        dm.windowed = not dm.windowed
        display:set_mode(dm)
      end
    end
  end
end

function end_test()
  mouse.on_press = nil
  dm = nil
end

function update_test(dt)
  clear_log()
  if display:get_mode(dm) then
    print_log("Fullscreen toggle\n=================\nPress left button to change resolution\nPress right button to toggle fullscreen", 0, 0, RED)
  else
    print_log("getmode failed", 0, 0, RED)
  end
end
