function begin_test()
  set_resolution(800, 600)

  local dm = DisplayMode()
  display:get_mode(dm)
  dm.windowed = false
  assert(display:set_mode(dm) == true, "set_mode failed")
end

function end_test()
  local dm = DisplayMode()
  display:get_mode(dm)
  dm.windowed = true
  display:set_mode(dm)
end

function update_test(dt)
  clear_log()
  print_log("Set title in fulscreen test", 0, 0, RED)
  display:set_title(math.random(100000))
end
