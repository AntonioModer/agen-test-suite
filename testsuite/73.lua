require("Clipper")

function begin_test()
  set_resolution(800, 600)

  s1 = Sprite(-400, 300)
  -- y coords go up in the opposite direction
  s1:set_scale(2, -2)
  display.viewport:add_child(s1)
  
  s1.canvas:square(8000)
  s1.canvas:set_fill_style(WHITE, 0.6)
  s1.canvas:fill()

  local function DrawPolygons(paths, c1, a1, c2, a2)
    -- hex to decimal?
    a1 = a1/256
    a2 = a2/256
    
    local c = s1.canvas
	
    for i = 1, #paths do
      local path = paths[i]
      c:move_to(path[1], path[2])
      for j = 3, #path, 2 do
        c:line_to(path[j], path[j + 1])
      end
      c:close_path()
    end
	
    local color = Color()
    color:set_hex(c1)
    
    c:set_fill_style(color, a1)
    c:fill_preserve()
    
    color:set_hex(c2)
    c:set_line_style(1, color, a2)
    c:stroke()
  end
  
  local subj = {
  	{180,200, 260,200, 260,150, 180,150},
    {215,160, 230,190, 200,190},
  }

  local clip = { 
  	{190,210, 240,210, 240,130, 190,130},
  }

  local solution = {}

  -- draw subj
  DrawPolygons(subj, 0x0000FF, 0x16, 0x0000FF, 0x60) -- blue
  DrawPolygons(clip, 0xFFFF00, 0x20, 0xFF0000, 0x30) -- orange

  local c = ClipperLib.Clipper()
  
  c:AddPaths(subj, 'subject', true)
  c:AddPaths(clip, 'clip', true)
  c:Execute('intersection', solution, 'nonZero', 'nonZero')
  DrawPolygons(solution, 0x00FF00, 0x30, 0x006600, 0xFF) -- solution shaded green

end

function end_test()
  display.viewport:remove_child(s1)
  s1 = nil
end

function update_test(dt)
  --clear_log()
  --print_log("Clipper test", 0, 0, RED)
end
