local image = Image()
local sprite = Sprite()
local sprites = {}

function begin_test()
  set_resolution(512, 512)

  image:load("testsuite/alphas.png")
  
  display.viewport:add_child(sprite)
  for i = 1, 10 do
    sprite.canvas:move_to((i - 5)*32 - 16, 32)
    sprite.canvas:square(32)
    sprite.canvas:set_fill_style(WHITE, i/10)
    sprite.canvas:fill()
    local s = Sprite((i - 5)*32 - 16, -32)
    display.viewport:add_child(s)
    table.insert(sprites, s)
    s.canvas:square(32)
    s.canvas:fill()
    s.alpha = i/10
  end
  sprite.canvas:set_source_image(image, 0, -64 - 32)
  sprite.canvas:paint()
  
  local sz =
[[alpha blending test
===================
top: set_fill_style(WHITE, alpha)
middle: sprite.alpha
bottom: image]]
  print_log(sz, -256, 100)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  sprite = nil
  while #sprites > 0 do
    local s = table.remove(sprites)
    display.viewport:remove_child(s)
  end
  sprites = nil
  image:unload()
  image = nil
  clear_log()
end