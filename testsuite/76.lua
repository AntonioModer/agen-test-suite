local image = Image()
local sprite = Sprite()
local sprite2 = Sprite()
local sprite3 = Sprite()

function begin_test()
  set_resolution(512, 512)

  image:load("testsuite/bg-moon.png")
  
  display.viewport:add_child(sprite)
  sprite.canvas:set_source_image(image, 0, -64 - 32)
  sprite.canvas:paint()
  
  sprite2.depth = -1
  display.viewport:add_child(sprite2)
  sprite2.canvas:set_source_image(image, 256, 64 + 32)
  sprite2.canvas:paint()
  sprite2.alpha = 0.5
  sprite2.color = WHITE
  
  sprite3.depth = -1
  display.viewport:add_child(sprite3)
  sprite3.canvas:set_source_image(image, -256, 64 + 32)
  sprite3.canvas:paint()
  sprite3.alpha = 0.5
  
  local sz =
[[alpha blending test
===================
left moon: alpha 0.5
right moon: alpha 0.5 color = WHITE
bottom moon: alpha 1]]
  print_log(sz, -256, 100)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  sprite2.canvas:clear()
  display.viewport:remove_child(sprite2)
  sprite3.canvas:clear()
  display.viewport:remove_child(sprite3)
  sprite = nil
  sprite2 = nil
  sprite3 = nil
  image:unload()
  image = nil
  clear_log()
end