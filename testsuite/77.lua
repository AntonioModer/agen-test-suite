require('unicode')

function begin_test()

	buffer = RenderBuffer(IMG_FORMAT_R8G8B8A8, 128, 128)
	buffer.color = GREY
	display.viewport:add_child (buffer)
	
	viewport = Sprite()
	display.viewport:add_child (viewport)
	viewport.canvas:set_source_image(buffer.image)
	viewport.canvas:paint()
	--
	-- add everything to buffer
	--
	s1 = Sprite (-300, 0)
	buffer:add_child (s1)
	-- mesh
	local p = {}
	p.type 		= 'triList'
	p.vertices	= array.floats { 0,0, 0,100, 100,0, 100,0, 100,100, 0,100 }
	p.colors    = array.uints  { 0xff00ff00, 0xff00ffff, 0xffffff00, 0xffffff00, 0xff00ff00, 0xff00ff00 }
	assert (#p.vertices / 2 == #p.colors)
	s1.canvas:add_shape (p)

	s2 = Sprite (0, 0)
	buffer:add_child (s2)
	-- image
	local image = Image ('testsuite/128x128rgb.png')
	local t = {
		type = 'triStrip',
		vertices = array.floats { 0, 0, image.width, 0, 0, image.height, image.width, image.height },
	-- DX tex coords: 0,0 is top left
		uv = array.floats { 0, 1, 1, 1, 0, 0, 1, 0 },
		texture = image,
	}
	assert (#t.vertices == #t.uv)
	s2.canvas:add_shape (t)
	-- gradient
	--[[
	local g = {}
	g.type = '???'
	g.vertices = { 0,0, 10,10, 10,100, 0,100 }
	g.colors = { 0x00ff00, 0x00ff00, 0x00ff00, 0x00ff00 }
	g.alphas = { 0, 0, 0, 0, 1, 1, 1, 1 }
	s1.canvas:add_child ( g )
	]]
	s3 = Sprite (-200, -100)
	buffer:add_child (s3)
	-- text
	local font = Font()
	font:load_default('sans', 16)

	local f  = {
		type	= 'triList',
		texture = font.texture,
		vertices= {},
		uv		= {},
	}
	local sz = "input string"
	local x, y = 0, 0
	local prevGlyph = nil
	--
	-- will render with x,y being top,left
	--
	for i = 1, unicode.utf8.len(sz) 
	do
		-- next char
		local c = unicode.utf8.byte(sz:sub(i, i))
		-- add kerning
		if prevGlyph ~= nil
		then
			x = x + prevGlyph:get_kerning (c)
		end
		-- note: not GC-ed
		local q = font:get_glyph(c)
		-- align with baseline
		local gx, gy = x + q.hBearingX, y + q.hBearingY
		-- bottom left
		table.insert(f.vertices, gx)
		table.insert(f.vertices, gy - q.height)
		table.insert(f.uv, q.coords[1])
		table.insert(f.uv, q.coords[4])
		-- top left
		table.insert(f.vertices, gx)
		table.insert(f.vertices, gy)
		table.insert(f.uv, q.coords[1])
		table.insert(f.uv, q.coords[2])
		-- bottom right
        table.insert(f.vertices, gx + q.width)
		table.insert(f.vertices, gy - q.height)
		table.insert(f.uv, q.coords[3])
		table.insert(f.uv, q.coords[4])
		-- top right
        table.insert(f.vertices, gx + q.width)
		table.insert(f.vertices, gy)
		table.insert(f.uv, q.coords[3])
		table.insert(f.uv, q.coords[2])
		-- bottom right
        table.insert(f.vertices, gx + q.width)
		table.insert(f.vertices, gy - q.height)
		table.insert(f.uv, q.coords[3])
		table.insert(f.uv, q.coords[4])
		-- top left
		table.insert(f.vertices, gx)
		table.insert(f.vertices, gy)
		table.insert(f.uv, q.coords[1])
		table.insert(f.uv, q.coords[2])
		-- advance
		x = x + q.hAdvance
		prevGlyph = q
	end
	assert (#t.vertices == #t.uv)

	f.vertices = array.floats(f.vertices)
	f.uv = array.floats(f.uv)

	s3.canvas:add_shape(f)
end

function end_test()
  display.viewport:remove_children ()
  s1 = nil
  s2 = nil
  s3 = nil
end

function update_test(dt)
  --clear_log()
end
