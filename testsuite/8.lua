local image = Image()
local sprite = Sprite()
local font = Font()

function begin_test()
  set_resolution(512, 512)

  local dir = get_suitedir()
  local sz = string.format("%s/512x512fonts.png", dir)
  image:load(sz)

  display.viewport:add_child(sprite)
  
  font:load_system("Arial", 9)
  
  sprite.canvas:set_source_image(image)
  sprite.canvas:paint()
  local sz = "ABCDEFGHIJKLMNOPQRSTUVWXYZ\nabcdefghijklmnopqrstuvwxyz"
  sprite.canvas:set_font(font, WHITE, 1)
  sprite.canvas:move_to(-256, -30)
  sprite.canvas:write(sz)
  sprite.canvas:set_font(font, BLACK, 1)
  sprite.canvas:move_to(0, -30)
  sprite.canvas:write(sz)
  
  local sz =
[[font rendering vs pre-rendered
==============================
Arial in Photoshop comparison:
First: 9 in AGen
Second: 9px in PS "strong"
Third: 9px in PS "crisp"
Fourth: 9px in PS "none"]]
  print_log(sz, -256, 256 - 20)
end

function end_test()
  sprite.canvas:clear()
  display.viewport:remove_child(sprite)
  image:unload()
  font:unload()
  clear_log()
end

function update_test()

end