local image = Image()
local sprites = {}
local sprite = Sprite()
local font = Font()

local standard =
{
  {
    { KEY_ESCAPE, 1, 1, "Esc", nil },
    { nil, 0.75, 1, nil, nil },
    { KEY_F1, 1, 1, "F1", nil },
    { KEY_F2, 1, 1, "F2", nil },
    { KEY_F3, 1, 1, "F3", nil },
    { KEY_F4, 1, 1, "F4", nil },
    { nil, 0.625, 1, nil, nil },
    { KEY_F5, 1, 1, "F5", nil },
    { KEY_F6, 1, 1, "F6", nil },
    { KEY_F7, 1, 1, "F7", nil },
    { KEY_F8, 1, 1, "F8", nil },
    { nil, 0.625, 1, nil, nil },
    { KEY_F9, 1, 1, "F9", nil },
    { KEY_F10, 1, 1, "F10", nil },
    { KEY_F11, 1, 1, "F11", nil },
    { KEY_F12, 1, 1, "F12", nil }
  },
  {
    { KEY_TILDE, 1, 1, "~", "`" },
    { KEY_1, 1, 1, "!", "1" },
    { KEY_2, 1, 1, "@", "2" },
    { KEY_3, 1, 1, "#", "3" },
    { KEY_4, 1, 1, "$", "4" },
    { KEY_5, 1, 1, "%", "5" },
    { KEY_6, 1, 1, "^", "6" },
    { KEY_7, 1, 1, "&", "7" },
    { KEY_8, 1, 1, "*", "8" },
    { KEY_9, 1, 1, "(", "9" },
    { KEY_0, 1, 1, ")", "0" },
    { KEY_MINUS, 1, 1, "_", "-" },
    { KEY_EQUALS, 1, 1, "+", "=" },
    { KEY_BACK, 2, 1, "Back", nil },
  },
  {
    { KEY_TAB, 1.5, 1, "Tab", nil },
    { KEY_Q, 1, 1, "Q", nil },
    { KEY_W, 1, 1, "W", nil },
    { KEY_E, 1, 1, "E", nil },
    { KEY_R, 1, 1, "R", nil },
    { KEY_T, 1, 1, "T", nil },
    { KEY_Y, 1, 1, "Y", nil },
    { KEY_U, 1, 1, "U", nil },
    { KEY_I, 1, 1, "I", nil },
    { KEY_O, 1, 1, "O", nil },
    { KEY_P, 1, 1, "P", nil },
    { KEY_LSQB, 1, 1, "{", "[" },
    { KEY_RSQB, 1, 1, "}", "]" },
    { KEY_BSLASH, 1.5, 1, "|", "\\" },
  },
  {
    { KEY_CAPS, 1.75, 1, "Caps", nil },
    { KEY_A, 1, 1, "A", nil },
    { KEY_S, 1, 1, "S", nil },
    { KEY_D, 1, 1, "D", nil },
    { KEY_F, 1, 1, "F", nil },
    { KEY_G, 1, 1, "G", nil },
    { KEY_H, 1, 1, "H", nil },
    { KEY_J, 1, 1, "J", nil },
    { KEY_K, 1, 1, "K", nil },
    { KEY_L, 1, 1, "L", nil },
    { KEY_COLON, 1, 1, ":", ";" },
    { KEY_APOS, 1, 1, '"', "'" },
    { KEY_ENTER, 2.25, 1, "Enter", nil },
  },
  {
    { KEY_LSHIFT, 2.25, 1, "Shift", nil },
    { KEY_Z, 1, 1, "Z", nil },
    { KEY_X, 1, 1, "X", nil },
    { KEY_C, 1, 1, "C", nil },
    { KEY_V, 1, 1, "V", nil },
    { KEY_B, 1, 1, "B", nil },
    { KEY_N, 1, 1, "N", nil },
    { KEY_M, 1, 1, "M", nil },
    { KEY_COMMA, 1, 1, "<", "," },
    { KEY_DOT, 1, 1, ">", "." },
    { KEY_SLASH, 1, 1, "?", "/" },
    { KEY_RSHIFT, 2.75, 1, "Shift", nil },
  },
  {
    { KEY_LCTRL, 1.5, 1, "Ctrl", nil },
    { KEY_LWIN, 1.25, 1, "Win", nil },
    { KEY_LALT, 1.25, 1, "Alt", nil },
    { KEY_SPACE, 5.5, 1, "", nil },
    { KEY_RALT, 1.25, 1, "Alt", nil },
    { KEY_RWIN, 1.25, 1, "Win", nil },
    { KEY_RMENU, 1.5, 1, "Menu", nil },
    { KEY_RCTRL, 1.5, 1, "Ctrl", nil },
  }
}

local extended =
{
  {
    { KEY_PSCREEN, 1, 1, "Print", "Scr" },
    { KEY_SLOCK, 1, 1, "Scroll", "Lock" },
    { KEY_PAUSE, 1, 1, "Pause", "Break" }
  },
  {
    { KEY_INS, 1, 1, "Insert", nil },
    { KEY_HOME, 1, 1, "Home", nil },
    { KEY_PGUP, 1, 1, "Page", "Up" }
  },
  {
    { KEY_DEL, 1, 1, "Delete", nil },
    { KEY_END, 1, 1, "End", nil },
    { KEY_PGDOWN, 1, 1, "Page", "Down" }
  },
  {
    { nil, 1, 1, nil, nil },
    { nil, 1, 1, nil, nil },
    { nil, 1, 1, nil, nil }
  },
  {
    { nil, 1, 1, nil, nil },
    { KEY_UP, 1, 1, "^", nil },
    { nil, 1, 1, nil, nil }
  },
  {
    { KEY_LEFT, 1, 1, "<", nil },
    { KEY_DOWN, 1, 1, "v", nil },
    { KEY_RIGHT, 1, 1, ">", nil }
  },
}

local numpad =
{
  {
    { nil, 1, 1, nil, nil },
    { nil, 1, 1, nil, nil },
    { nil, 1, 1, nil, nil },
    { nil, 1, 1, nil, nil }
  },
  {
    { KEY_NUMLOCK, 1, 1, "Num", "Lock" },
    { KEY_NUMDIV, 1, 1, "/", nil },
    { KEY_NUMMUL, 1, 1, "*", nil },
    { KEY_NUMMINUS, 1, 1, "-", nil }
  },
  {
    { KEY_NUM_7, 1, 1, "7", "Home" },
    { KEY_NUM_8, 1, 1, "8", nil },
    { KEY_NUM_9, 1, 1, "9", "PgUp" },
    { KEY_NUMPLUS, 1, 2, "+", nil }
  },
  {
    { KEY_NUM_4, 1, 1, "4", nil },
    { KEY_NUM_5, 1, 1, "5", nil },
    { KEY_NUM_6, 1, 1, "6", nil }
  },
  {
    { KEY_NUM_1, 1, 1, "1", "End" },
    { KEY_NUM_2, 1, 1, "2", nil },
    { KEY_NUM_3, 1, 1, "3", "PgDn" },
    { KEY_NUMENTER, 1, 2, "Enter", nil }
  },
  {
    { KEY_NUM_0, 2, 1, "1", "Ins" },
    { KEY_NUMDEL, 1, 1, ".", "Del" }
  }
}

local function draw_key(v, x, y, kw, kh)
  if v[1] == nil then
    -- separator
    return
  end
  local sprite = Sprite(x, y)
  sprite.alpha = 0.5
  sprites[v[1]] = sprite
  display.viewport:add_child(sprite)
  local c = sprite.canvas
  c:set_font(font, BLACK)
  c:rel_move_to(v[2]*(kw/2), (v[3] - 1)*(-kh/2))
  c:rectangle(v[2]*kw - 1, v[3]*kh - 1)
  c:fill()
  c:move_to(0, 0)
  c:rel_move_to(kw/8, kw/8)
  if v[4] then
    c:write(v[4])
  end
  if v[5] then
    c:rel_move_to(0, -kh/2)
    c:write(v[5])
  end
end

local function update_keys(list)
  for i, v in ipairs(list) do
    for j, v in ipairs(v) do
      local key = v[1]
      if key then
        sprites[key].color = WHITE
        if keyboard:is_down(v[1]) then
          sprites[key].color = RED
          sprites[key].alpha = 1
        end
      end
    end
  end
end

local function draw_toggle(sz, key, x, y, tw, th)
  sprite.canvas:move_to(x, y)
  sprite.canvas:rel_move_to(0, -th*2 - 2)
  sprite.canvas:set_font(font, GRAY)
  sprite.canvas:write(sz)
  sprite.canvas:move_to(x, y)
  sprite.canvas:rel_move_to(tw/2, -th/2)
  sprite.canvas:rectangle(tw, th)
  local c = GRAY
  if keyboard:is_toggled(key) == true then
    c = RED
  end
  sprite.canvas:set_fill_style(c)
  sprite.canvas:fill()
end

function begin_test()
  set_resolution(800, 600)

  local dir = get_suitedir()
  local sz = string.format("%s/512x512fonts.png", dir)
  image:load(sz)
  
  font:load_system("Arial", 10)

  local kw, kh = 35, 35
  for i, v in ipairs(standard) do
    local x = -400
    for j, v in ipairs(v) do
      draw_key(v, x, i*-kh, kw, kh)
      x = x + v[2]*kw
    end
  end
  for i, v in ipairs(extended) do
    local x = 140
    for j, v in ipairs(v) do
      draw_key(v, x, i*-kh, kw, kh)
      x = x + v[2]*kw
    end
  end
  for i, v in ipairs(numpad) do
    local x = 260
    for j, v in ipairs(v) do
      draw_key(v, x, i*-kh, kw, kh)
      x = x + v[2]*kw
    end
  end
  
  local errors = {}
  if keyboard.name == nil then
    table.insert(errors, "Warning no device name!")
  end
  
  sprite:set_position(260, 0)
  display.viewport:add_child(sprite)
  
  local sz =
[[is_down() and is_toggled() keyboard test
========================================
Press a key to see if it is detected
Note: The Alt keys and F10 seem to do Windows-related stuff
Warnings:
%s]]
  sz = string.format(sz, table.concat(errors))
  print_log(sz, -320, 240 - 20)
end

function end_test()
  for i, v in pairs(sprites) do
    v.canvas:clear()
    display.viewport:remove_child(v)
    sprites[i] = nil
  end
  sprite.canvas:clear()
  display.viewport:add_child(sprite)
  font:unload()
  image:unload()
  clear_log()
end

function update_test()
  update_keys(standard)
  update_keys(extended)
  update_keys(numpad)

  sprite.canvas:clear()
  sprite.canvas:set_font(font, WHITE)
  sprite.canvas:move_to(-300, 10)
  sprite.canvas:write("Device: " .. (keyboard.name or "?"))
  draw_toggle("Num", KEY_NUMLOCK, 0, -20, 20, 10)
  draw_toggle("Caps", KEY_CAPS, 40, -20, 20, 10)
  draw_toggle("Scroll", KEY_SLOCK, 80, -20, 20, 10)
end