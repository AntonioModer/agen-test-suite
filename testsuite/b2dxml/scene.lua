--
-- Scene
--

-- Syncs sprites to bodies
b2Scene = {}
b2SceneMT = { __index = b2Scene }

function b2Scene:Create(world, mt)
  local self = {}
  setmetatable(self, mt or b2SceneMT)

  self.world = world
  self.layer = Layer()
  self.sprites = {}
  self.sprite = Sprite()
  self.sprite.depth = -1
  self.layer:add_child(self.sprite)
--[[
  local b = world:GetBodyList()
  while b do
    self:AddBody(b)
    b = b:GetNext()
  end
]]
  return self
end

function b2Scene:Destroy()
  self.layer:remove_children()

  self.world = nil
  self.layer = nil
  self.sprites = nil
  self.sprite = nil
end

function b2Scene:AddCamera(c)
  self.layer:add_child(c)
end

function b2Scene:RemoveCamera(c)
  self.layer:remove_child(c)
end

function b2Scene:AddBody(body, s)
  s = s or Sprite()
  self.layer:add_child(s)
  self.sprites[body] = s
  -- possible todo: sync at this point?
end

function b2Scene:RemoveBody(body)
  local s = self.sprites[body]
  self.sprites[body] = nil
  self.layer:remove_child(s)
end

function b2Scene:GetSprite(body)
  return self.sprites[body]
end

-- move sprites to body positions (interpolated)
function b2Scene:LerpSprites(dt)
  for b, s in pairs(self.sprites) do
    -- expect a crash if the body was destroyed
    local px, py = b:GetPositionXY(0, 0)
    local a = b:GetAngle()
    -- interpolate using linear/angular velocity
    local lvx, lvy = b:GetLinearVelocityXY(0, 0)
    local av = b:GetAngularVelocity()
    s:set_position(px + lvx*dt, py + lvy*dt)
    s:set_rotation_r(-a + -av*dt)
  end
end

-- move sprites to body positions (non-interpolated)
function b2Scene:SyncSprites()
  --local world = self.world
  for b, s in pairs(self.sprites) do
    -- expect a crash if the body was destroyed
    local px, py = b:GetPositionXY(0, 0)
    local a = b:GetAngle()
    s:set_position(px, py)
    s:set_rotation_r(-a)
  end
end




function b2Scene:RedrawBodies(scale, color)
  local v = self.world:GetBodyList()
  for b, s in pairs(self.sprites) do
    self:RedrawBody(v, scale, color)
    v = v:GetNext()
  end
end

-- todo: user redraw
function b2Scene:RedrawBody(body, scale, color)
  color = color or BLACK
  scale = scale or 1
  local sprite = self.sprites[body]

  --local scale = 1/sprite.scalex
  local c = sprite.canvas
  c:clear()

  -- collision shapes
  local v = body:GetShapeList()
  while v do
    local t = v:GetType()
    if t == e_circleShape then
      local r = v:GetRadius()
      local lp = v:GetLocalPosition()
      c:move_to(lp.x*scale, lp.y*scale)
      c:circle(r*scale)
      c:rel_line_to(0, r*scale)
    elseif t == e_polygonShape then
      local vertices = v:GetVertices()
      c:move_to(vertices[1].x*scale, vertices[1].y*scale)
      for i = 2, #vertices do
        local v = vertices[i]
        c:line_to(v.x*scale, v.y*scale)
      end
      c:close_path()
    else
      assert(false, "Unknown shape")
    end
    local alpha = 1
    if v:IsSensor() == true then
      alpha = alpha/2
    end
    c:set_fill_style(color, alpha)
    c:fill()
    --c:set_line_style(0.01, color, alpha)
    --c:stroke()
    v = v:GetNext()
  end
end

function b2Scene:DrawJoint(joint, scale, c)
  local t = joint:GetType()
  local a1 = joint:GetAnchor1()
  local a2 = joint:GetAnchor2()
  if t == e_revoluteJoint then
    c:move_to(a1.x, a1.y)
    c:circle(scale*5)
    c:set_line_style(scale, WHITE, 1)
    c:stroke()
    -- rotation limits
    local e = joint:IsLimitEnabled()
    if e == true then
      local l = joint:GetLowerLimit()
      local u = joint:GetUpperLimit()
      local b = joint:GetBody1()
      local a = b:GetAngle()
      l, u = l + a, u + a
      local x = math.cos(l)*scale
      local y = math.sin(l)*scale
      local x2 = math.cos(u)*scale
      local y2 = math.sin(u)*scale
      c:move_to(a1.x, a1.y)
      c:line_to(a1.x + x*12, a1.y + y*12)
      c:move_to(a1.x + x2*4, a1.y + y2*4)
      c:line_to(a1.x + x2*12, a1.y + y2*12)
      c:set_line_style(scale, LIME, 1)
      c:stroke()
    end
    -- overstreched revolute joint
    if a1.x ~= a2.x or a1.y ~= a2.y then
      c:move_to(a1.x, a1.y)
      c:line_to(a2.x, a2.y)
      c:set_line_style(scale, RED, 1)
      c:stroke()
    end
  elseif t == e_prismaticJoint then
    local dx, dy = a2.x - a1.x, a2.y - a1.y
    local a = math.atan2(dy, dx)
    local lx = math.cos(a)*scale*5
    local ly = math.sin(a)*scale*5
    c:move_to(a1.x, a1.y)
    c:rel_line_to(ly, -lx)
    c:move_to(a1.x, a1.y)
    c:rel_line_to(-ly, lx)
    c:move_to(a1.x, a1.y)
    c:line_to(a2.x, a2.y)
    c:rel_line_to(ly, -lx)
    c:move_to(a2.x, a2.y)
    c:rel_line_to(-ly, lx)
    c:set_line_style(scale, WHITE, 1)
    c:stroke()
--[[
    local e = joint:IsLimitEnabled()
    if e == true then
      local l = joint:GetLowerLimit()
      local u = joint:GetUpperLimit()
      local d = math.sqrt(dx*dx + dy*dy)
      local nx, ny = dx/d, dy/d
      c:move_to(a1.x, a1.y)
      c:rel_move_to(nx*l, ny*l)
      c:rel_line_to(ly*1.5, -lx*1.5)
      c:move_to(a1.x, a1.y)
      c:rel_move_to(nx*l, ny*l)
      c:rel_line_to(-ly*1.5, lx*1.5)

      c:move_to(a1.x, a1.y)
      c:rel_move_to(nx*u, ny*u)
      c:rel_line_to(ly*1, -lx*1)
      c:move_to(a1.x, a1.y)
      c:rel_move_to(nx*u, ny*u)
      c:rel_line_to(-ly*1, lx*1)
      c:set_line_style(scale, LIME, 1)
      c:stroke()
    end
    ]]
  elseif t == e_distanceJoint then
    c:move_to(a1.x, a1.y)
    c:line_to(a2.x, a2.y)
    c:set_line_style(scale, WHITE, 1)
    c:stroke()
  end
end

function b2Scene:DrawJoints(scale)
  local c = self.sprite.canvas
  c:clear()
  local v = self.world:GetJointList()
  while v do
    self:DrawJoint(v, scale, c)
    v = v:GetNext()
  end
end