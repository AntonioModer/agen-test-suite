local bufferC = b2CircleDef()
local bufferP = b2PolygonDef()
local bufferF = b2FilterData()

xml.tob2ShapeDef = function(s)
  local def

  local t = s.type
  if t == "circle" then
    local r = xml.find(s, "Radius")
    local lp = xml.find(s, "LocalPosition")
    -- assign values
    def = bufferC
    def.radius = xml.tonumber(r[1])
    -- default values
    def.localPosition:Set(0, 0)
    -- optional
    if lp then
      local lpx, lpy = xml.toPosition(lp[1])
      def.localPosition:Set(lpx, lpy)
    end
  elseif t == "polygon" then
    local path = xml.find(s, "Path")
    local vertices = xml.toPath(path)
    assert(path, "Polygon without path")
    assert(#vertices > 1, "Polygon vertices less than two")
    --assert(#vertices <= b2_maxPolygonVertices, "Polygon exceeds b2_maxPolygonVertices")
    -- assign values
    def = bufferP
    def.vertexCount = #vertices
    for i = 1, #vertices do
      def.vertices[i - 1] = vertices[i]
    end
  else
    assert(false, "Unknown shape type")
  end

  -- assign values
  def.density = xml.tonumber(s.density)
  def.friction = xml.tonumber(s.friction)
  def.restitution = xml.tonumber(s.restitution)
  def.isSensor = xml.toboolean(s.isSensor)

  return def
end

xml.createConcave = function(s, b2body)
  local path = xml.find(s, "Path")
  local vertices = xml.toPath(path)
  local out = {}
  local ret = b2Fill(vertices, out)
  if ret == false then
    -- triangulation failed
    return
  end
  local b2world = b2body:GetWorld()
  local proxies = b2world:GetProxyCount()
  if proxies + (#out/3) > b2_maxProxies then
    -- b2_maxProxies exceeded
    return
  end

  local triangles = {}
  local def = bufferP
  -- assign values
  def.density = xml.tonumber(s.density)
  def.friction = xml.tonumber(s.friction)
  def.restitution = xml.tonumber(s.restitution)
  def.isSensor = xml.toboolean(s.isSensor)
  def.vertexCount = 3
  for i = 1, #out, 3 do
    def.vertices[0]:Set(out[i].x, out[i].y)
    def.vertices[1]:Set(out[i + 1].x, out[i + 1].y)
    def.vertices[2]:Set(out[i + 2].x, out[i + 2].y)
    local b2shape = b2body:CreateShape(def)
    if b2shape == nil then
      -- polygon was partially generated
      return
    end
    -- keep a list of all generated triangles
    table.insert(triangles, b2shape)
  end
end

xml.createShape = function(s, b2body)
  if s.type == "concave" then
    xml.createConcave(s, b2body)
    return
  end
  
  local def = xml.tob2ShapeDef(s)

  local b2shape = b2body:CreateShape(def)

  local filter = xml.find(shape, "Filter")
  if filter then
    local fd = bufferF
    fd.groupIndex = xml.tonumber(filter.GroupIndex)
    fd.categoryBits = xml.tonumber(filter.CategoryBits)
    fd.maskBits = xml.tonumber(filter.MaskBits)
    b2shape:SetFilter(fd)
  end
  return b2shape
end
