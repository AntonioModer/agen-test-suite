xml.createWorld = function(w)
  local l = xml.tonumber(w.left)
  local t = xml.tonumber(w.top)
  local r = xml.tonumber(w.right)
  local b = xml.tonumber(w.bottom)
  local aabb = b2AABB()
  aabb.lowerBound:Set(l, t)
  aabb.upperBound:Set(r, b)

  local g = xml.tob2Vec2(w.gravity)
  local doSleep = xml.toboolean(w.doSleep)

  local b2world = b2World(aabb, g, doSleep)
  assert(b2world, "Could not create world")

  xml.indexWorld(b2world)
  
  local objects = #w
  for i = 1, objects, 1 do
    local object = w[i]
    local tag = xml.tag(object)
    if tag == "Body" then
      local b = xml.createBody(object, b2world)
      local id = xml.tonumber(object.id)
      xml.setBody(b2world, id, b)
    elseif tag == "Joint" then
      local j = xml.createJoint(object, b2world)
      local jid = xml.tonumber(object.id)
      xml.setJoint(b2world, jid, j)
    else
      assert(false, "Unkown tag in world")
    end
  end

  return b2world
end

xml.destroyWorld = function(b2world)
  xml.deindexWorld(b2world)
end