local buffer = b2.BodyDef()

xml.tob2BodyDef = function(b)
  local def = buffer

  local px, py = xml.toPosition(b.position)
  def.position:Set(px, py)
  def.type = b.type or "dynamicBody" -- todo: figure out body type
  def.angle = xml.tonumber(b.angle)
  def.linearDamping = xml.tonumber(b.linearDamping)
  def.angularDamping = xml.tonumber(b.angularDamping)
  def.fixedRotation = xml.toboolean(b.fixedRotation)
  def.bullet = xml.toboolean(b.isBullet)
  def.allowSleep = xml.toboolean(b.allowSleep)
  def.isSleeping = xml.toboolean(b.isSleeping)
  def.awake = true
  def.active = true

  return def
end


xml.createBody = function(b, b2world)
  local def = xml.tob2BodyDef(b)
  
  local b2body = b2world:CreateBody(def)
  assert(b2body, "Could not create body")

  for i = 1, #b, 1 do
    local tag = xml.tag(b[i])
    if tag == "LinearVelocity" then
      local lv = xml.tob2Vec2(b[i][1])
      b2body:SetLinearVelocity(lv)
    elseif tag == "AngularVelocity" then
      local av = xml.tonumber(b[i][1])
      b2body:SetAngularVelocity(av)
    elseif tag == "Shape" then
      xml.createShape(b[i], b2body)
    end
  end
  --assert(#b.shapes ~= 0, "Body with no shapes")

  --b2body:SetMassFromShapes()
  return b2body
end