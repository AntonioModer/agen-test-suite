-- converts string to number
xml.tonumber = function(sz)
  return tonumber(sz)
end

-- converts string to boolean
xml.toboolean = function(b)
  if b == "true" then
    return true
  elseif b == "false" then
    return false
  end
end

-- converts string to x, y
xml.toPosition = function(v)
  -- longer but probably faster than pattern matching
  local i = string.find(v, ",", 1, true)
  if i == nil then 
    return
  end
  local sx = string.sub(v, 1, i - 1)
  local sy = string.sub(v, i + 2, -1)
  local x = xml.tonumber(sx)
  local y = xml.tonumber(sy)
  if x == nil or y == nil then
    return
  end
  return x, y
end

xml.PositionStr = function(x, y)
  return string.format("%g, %g", x, y)
end

xml.tob2Vec2 = function(v)
  local x, y = xml.toPosition(v)
  return b2.Vec2(x, y)
end

xml.b2Vec2Str = function(v)
  return xml.PositionStr(v.x, v.y)
end

xml.toPath = function(p)
  local path = {}
  for i = 1, #p, 1 do
    local v = p[i]
    local tag = xml.tag(v)
    assert(tag == "Vertex", "Unknown tag in path")
    local vertex = xml.tob2Vec2(v[1])
    assert(vertex, "Invalid vertex in path")
    table.insert(path, vertex)
  end
  return path
end

xml.PathStr = function(path)
  local p = xml.new("Path")
  for i, v in ipairs(path) do
    p:append("Vertex")[1] = xml.b2Vec2Str(v)			
  end
  return p
end