local bufferR = b2.RevoluteJointDef()
local bufferP = b2.PrismaticJointDef()
local bufferD = b2.DistanceJointDef()
local bufferU = b2.PulleyJointDef()
local bufferM = b2.MouseJointDef()
local bufferG = b2.GearJointDef()

xml.tob2JointDef = function(j, b2world)
  local def

  local t = j.type
  local def
  if t == "revolute" then
    local a = xml.find(j, "ReferenceAngle")
    local l = xml.find(j, "Limit")
    local m = xml.find(j, "Motor")
    -- assign values
    def = bufferR
    def.referenceAngle = xml.tonumber(a[1])
    -- default values
    def.enableLimit = false
    def.lowerAngle = 0
    def.upperAngle = 0
    def.enableMotor = false
    def.motorSpeed = 0
    def.maxMotorTorque = 0
    -- optional
    if l then
      def.enableLimit = xml.toboolean(l.enabled)
      def.lowerAngle = xml.tonumber(l.lowerAngle)
      def.upperAngle = xml.tonumber(l.upperAngle)
    end
    if m then
      def.enableMotor = xml.toboolean(m.enabled)
      def.motorSpeed = xml.tonumber(m.speed)
      def.maxMotorTorque = xml.tonumber(m.maxTorque)
    end
  elseif t == "prismatic" then
    local la1 = xml.find(j, "LocalAxis1")
    local la1x, la1y = xml.toPosition(la1[1])
    local a = xml.find(j, "ReferenceAngle")
    local l = xml.find(j, "Limit")
    local m = xml.find(j, "Motor")
    -- assign values
    def = bufferP
    def.localAxisA:Set(la1x, la1y)
    def.referenceAngle = xml.tonumber(a[1])
    -- default values
    def.enableLimit = false
    def.lowerTranslation = 0
    def.upperTranslation = 0
    def.enableMotor = false
    def.motorSpeed = 0
    def.maxMotorForce = 0
    -- optional
    if l then
      def.enableLimit = xml.toboolean(l.enabled)
      def.lowerTranslation = xml.tonumber(l.lowerTranslation)
      def.upperTranslation = xml.tonumber(l.upperTranslation)
    end
    if m then
      def.enableMotor = xml.toboolean(m.enabled)
      def.motorSpeed = xml.tonumber(m.speed)
      def.maxMotorForce = xml.tonumber(m.maxForce)
    end
  elseif t == "distance" then
    local l = xml.find(j, "Length")
    local f = xml.find(j, "FrequencyHz")
    local d = xml.find(j, "DampingRatio")
    -- assign values
    def = bufferD
    def.length = xml.tonumber(l[1])
    def.frequencyHz = xml.tonumber(f[1])
    def.dampingRatio = xml.tonumber(d[1])
  elseif t == "pulley" then
    local ga1 = xml.find(j, "GroundAnchor1")
    local ga2 = xml.find(j, "GroundAnchor2")
    local ga1x, ga1y = xml.toPosition(ga1[1])
    local ga2x, ga2y = xml.toPosition(ga2[1])
    local l1 = xml.find(j, "Length1")
    local l2 = xml.find(j, "Length2")
    local maxl1 = xml.find(j, "MaxLength1")
    local maxl2 = xml.find(j, "MaxLength2")
    local r = xml.find(j, "Ratio")
    -- values
    def = bufferU
    def.groundAnchorA:Set(ga1x, ga1y)
    def.groundAnchorB:Set(ga2x, ga2y)
    def.lengthA = xml.tonumber(l1[1])
    def.lengthB = xml.tonumber(l2[1])
    def.maxLength1 = xml.tonumber(maxl1[1])
    def.maxLength2 = xml.tonumber(maxl2[1])
    def.ratio = xml.tonumber(r[1])
  elseif t == "mouse" then
    def = bufferM
  elseif t == "gear" then
    local j1 = xml.find(joint, "Joint1")
    local j2 = xml.find(joint, "Joint2")
    local jid1 = xml.tonumber(j1[1])
    local jid2 = xml.tonumber(j2[1])
    -- assign values
    def = bufferG
    def.jointA = xml.getJoint(b2world, jid1)
    def.jointB = xml.getJoint(b2world, jid2)
  end

  local id1 = xml.tonumber(j.body1)
  local id2 = xml.tonumber(j.body2)
  local la1 = xml.find(j, "LocalAnchor1")
  local la2 = xml.find(j, "LocalAnchor2")
  -- assign values
  def.bodyA = xml.getBody(b2world, id1)
  def.bodyB = xml.getBody(b2world, id2)
  def.collideConnected = xml.toboolean(j.collideConnected)
  def.localAnchorA:Set(0, 0)
  def.localAnchorB:Set(0, 0)
  if la1 then
    local la1x, la1y = xml.toPosition(la1[1])
    def.localAnchorA:Set(la1x, la1y)
  end
  if la2 then
    local la2x, la2y = xml.toPosition(la2[1])
    def.localAnchorB:Set(la2x, la2y)
  end

  return def
end

xml.createJoint = function(s, b2world)
  local def = xml.tob2JointDef(s, b2world)

  local b2joint = b2world:CreateJoint(def)
  assert(b2joint, "Could not generate joint")

  return b2joint
end