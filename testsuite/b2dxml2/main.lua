require('LuaXml')
require('Box2D')

require('testsuite.b2dxml2.cast')
require('testsuite.b2dxml2.body')
require('testsuite.b2dxml2.joint')
require('testsuite.b2dxml2.shape')
require('testsuite.b2dxml2.world')

require('testsuite.b2dxml2.scene')

-- keep a list of loaded worlds
xml.bodies = {}
xml.joints = {}

xml.indexWorld = function(b2world)
  xml.bodies[b2world] = {}
  xml.joints[b2world] = {}
end
xml.deindexWorld = function(b2world)
  xml.bodies[b2world] = nil
  xml.joints[b2world] = nil
end

xml.getBody = function(b2world, id)
  return xml.bodies[b2world][id]
end
xml.setBody = function(b2world, id, b2body)
  xml.bodies[b2world][id] = b2body
end

xml.getJoint = function(b2world, id)
  return xml.joints[b2world][id]
end
xml.setJoint = function(b2world, id, b2joint)
  xml.joints[b2world][id] = b2joint
end

xml.loadWorld = function(file)
  local box = xml.load(file)
  -- could not load XML file
  assert(box, "Could not load XML file")
  local world = xml.find(box, "World")
  assert(world, "Not a valid Box2D XML file")
  return xml.createWorld(world)
end
xml.unloadWorld = function(world)
  xml.destroyWorld(world)
end