local bufferC = b2.CircleShape()
local bufferP = b2.PolygonShape()
--local bufferF = b2.FilterData()

local bufferF = b2.FixtureDef()

xml.tob2ShapeDef = function(s)
  local def

  local t = s.type
  if t == "circle" then
    local r = xml.find(s, "Radius")
    local lp = xml.find(s, "LocalPosition")
    -- assign values
    def = bufferC
    def.radius = xml.tonumber(r[1])
    -- default values
    def.position:Set(0, 0)
    -- optional
    if lp then
      local lpx, lpy = xml.toPosition(lp[1])
      def.position:Set(lpx, lpy)
    end
  elseif t == "polygon" then
    local path = xml.find(s, "Path")
    local vertices = xml.toPath(path)
    assert(path, "Polygon without path")
    assert(#vertices > 1, "Polygon vertices less than two")
    --assert(#vertices <= b2_maxPolygonVertices, "Polygon exceeds b2_maxPolygonVertices")
    -- assign values
    def = bufferP
    def.vertexCount = #vertices
    --[[
    for i = 1, #vertices do
      -- vertices are indexed from 1
      def.vertices[i] = vertices[i]
    end
    ]]
    def:Set(vertices)
  else
    assert(false, "Unknown shape type")
  end

  -- assign values
  local fix = bufferF
  fix.density = xml.tonumber(s.density)
  fix.friction = xml.tonumber(s.friction)
  fix.restitution = xml.tonumber(s.restitution)
  fix.isSensor = xml.toboolean(s.isSensor)
  fix.shape = def
  
  return fix
end

xml.createConcave = function(s, b2body)
  local path = xml.find(s, "Path")
  local vertices = xml.toPath(path)
  local out = {}
  local ret = b2.Fill(vertices, out)
  if ret == false then
    -- triangulation failed
    return
  end
  local b2world = b2body:GetWorld()

  -- assign values
  local triangles = {}
  local def = bufferP
  def.vertexCount = 3
  
  local fix = bufferF
  fix.density = xml.tonumber(s.density)
  fix.friction = xml.tonumber(s.friction)
  fix.restitution = xml.tonumber(s.restitution)
  fix.isSensor = xml.toboolean(s.isSensor)

  fix.shape = def
  for i = 1, #out - 1, 3 do
    --[[
    def.vertices[1]:Set(out[i].x, out[i].y)
    def.vertices[2]:Set(out[i + 1].x, out[i + 1].y)
    def.vertices[3]:Set(out[i + 2].x, out[i + 2].y)
    ]]
--[[
    -- todo: reuse b2vec2s instead of creating them
    def.vertices[1] = b2.Vec2(out[i].x, out[i].y)
    def.vertices[2] = b2.Vec2(out[i + 1].x, out[i + 1].y)
    def.vertices[3] = b2.Vec2(out[i + 2].x, out[i + 2].y)
]]
    def:Set ( { out[i], out[i + 1], out[i + 2] } )
    local b2shape = b2body:CreateFixture(fix)
    if b2shape == nil then
      -- polygon was partially generated
      return
    end
    -- keep a list of all generated triangles
    table.insert(triangles, b2shape)
  end
end

xml.createShape = function(s, b2body)
  if s.type == "concave" then
    xml.createConcave(s, b2body)
    return
  end

  local def = xml.tob2ShapeDef(s)

  local b2shape = b2body:CreateFixture(def)

  --[[
  -- todo:filter data
  local filter = xml.find(shape, "Filter")
  if filter then
    local fd = bufferF
    fd.groupIndex = xml.tonumber(filter.GroupIndex)
    fd.categoryBits = xml.tonumber(filter.CategoryBits)
    fd.maskBits = xml.tonumber(filter.MaskBits)
    b2shape:SetFilter(fd)
  end
  ]]
  return b2shape
end
