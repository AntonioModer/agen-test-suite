xml.createWorld = function(w)
  local g = xml.tob2Vec2(w.gravity)
  --local doSleep = xml.toboolean(w.doSleep)

  --local b2world = b2.World(aabb, g, doSleep)
  local b2world = b2.World(g)
  assert(b2world, "Could not create world")

  xml.indexWorld(b2world)
  
  local objects = #w
  for i = 1, objects, 1 do
    local object = w[i]
    local tag = xml.tag(object)
    if tag == "Body" then
      local b = xml.createBody(object, b2world)
      local id = xml.tonumber(object.id)
      xml.setBody(b2world, id, b)
    elseif tag == "Joint" then
      local j = xml.createJoint(object, b2world)
      local jid = xml.tonumber(object.id)
      xml.setJoint(b2world, jid, j)
    else
      assert(false, "Unkown tag in world")
    end
  end

  return b2world
end

xml.destroyWorld = function(b2world)
  xml.deindexWorld(b2world)
end