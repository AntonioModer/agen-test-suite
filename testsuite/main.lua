display:create("Test Suite", 640, 480, 32, true)

local dir = "testsuite"
local test = 1
local file = io.open("suite.log", "w+")

local font = Font()
--font:load_system("Courier", 10)
font:load_file(dir .. "/apple.ttf", 6)
local overlay = Sprite()
overlay.depth = -math.huge
display.viewport:add_child(overlay)

local function get_savefilename()
  local userdir = system.get_private_dir()
  return string.format("%s/AGenTestSuite.save", userdir)
end

local function load_settings()
  local fn = get_savefilename()
  local r, e = pcall(dofile, fn)
  if r == true then
    test = tonumber(e) or 0
  end
end

local function save_settings()
  local fn = get_savefilename()
  local file = io.open(fn, "w+")
  if file then
    local sz = string.format("return %d", test)
    file:write(sz)
    file:close()
  end
end

function get_suitedir()
  return dir
end

function set_resolution(x, y, d, w)
  local dm = DisplayMode()
  display:get_mode(dm)
  dm.width = x
  dm.height = y
  dm.depth = d or dm.depth
  if w then
    dm.windowed = w
  end
  display:set_mode(dm)
end

local function safe_call(func, ...)
  if func == nil then
    return false
  end
  local r, e = pcall(func, ...)
  if r == false then
    show_error(e)
    return false
  end
  return true
end

local function clear_callbacks()
  if end_test then
    pcall(end_test)
    --end_test()
  end
  begin_test = nil
  end_test = nil
  update_test = nil
end

function run_test(n)
  clear_log()
  file_log("\n\n")
  clear_callbacks()
  local sz = string.format("%s/%d.lua", dir, n)
  if safe_call(dofile, sz) == false then
    return false
  end
  safe_call(begin_test)
  display:set_title("Test:" .. n)
  test = n
  return true
end

function show_error(msg)
  set_resolution(640, 480)
  clear_log()
  local sz = "Error in test%d:\n%s"
  sz = string.format(sz, test, msg)
  print_log(sz, -320, 0)
  --error(msg)
end

function run_next_test()
  test = test + 1
  run_test(test)
end

function run_previous_test()
  test = test - 1
  run_test(test)
end

function file_log(sz)
  if file then
    file:write(sz)
    file:flush()
  end
end

function print_log(sz, x, y, color)
  local c = overlay.canvas
  c:set_font(font, color or WHITE)
  c:move_to(x, y)
  c:write(sz)
  --file_log(sz)
end

function clear_log()
  overlay.canvas:clear()
end

local keyboard_on_press = function(keyboard, key)
  if keyboard_on_press ~= old_keyboard_on_press then
    safe_call(old_keyboard_on_press, keyboard, key)
  end
  if grab_keyboard ~= true then
    return
  end
  if key == KEY_RIGHT then
    run_next_test()
  elseif key == KEY_LEFT then
    run_previous_test()
  end
  save_settings()
end

keyboard.on_press = keyboard_on_press
grab_keyboard = true

timer = Timer()
timer:start(16, true)
timer.on_tick = function(timer)
  if grab_keyboard and keyboard.on_press ~= keyboard_on_press then
    if old_keyboard_on_press ~= keyboard_on_press then
      old_keyboard_on_press = keyboard.on_press
    end
    keyboard.on_press = keyboard_on_press
  end
  local ms = timer:get_delta()*16
  if update_test then
    local r, e = pcall(update_test, ms)
    if r == false then
      show_error(e)
    end
  end
end

load_settings()
run_test(test)
