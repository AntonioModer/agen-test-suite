local max_particles = 2^32

local ParticleSystem = {}
local ParticleSystemMT = { __index = ParticleSystem }

ParticleSystem.Create = function ( x, y, count, canvas )
  local self = {}
  setmetatable ( self, ParticleSystemMT )

  self.layer = Layer ( x, y )
  self.nonactive = {}

  self.canvas = canvas
  self.active = {}
  self.time = 0 -- elapsed time
  self.rate = count -- emission rate
  self.gravityx = 0
  self.gravityy = 0

  self.speed = 100
  self.speedvariance = 0
  self.distance = 100
  self.direction = 0
  self.spread = 360
  self.scolor = WHITE -- starting color
  self.ecolor = WHITE -- ending color
  self.sscale = 1 -- starting scale
  self.escale = 0 -- ending scale
  self.salpha = 1 -- starting alpha
  self.ealpha = 0 -- ending alpha
  
  self.pcount = count

  for i = 1, count do
    self:AllocateNonactive(canvas)
  end

  return self
end

ParticleSystem.Destroy = function ( self )
  self:DeactivateAll ( )
  while #self.nonactive > 0 do
    self:CollectNonactive ( )
  end
  
  self.layer = nil
  self.canvas = nil
  self.nonactive = nil
  self.active = nil
  self.time = nil
  self.rate = nil
  self.gravityx = nil
  self.gravityy = nil
  
  self.speed = nil
  self.distance = nil
  self.direction = nil
  self.spread = nil
  self.scolor = nil
  self.ecolor = nil
  self.salpha = nil
  self.ealpha = nil
  
  self.pcount = nil
end

ParticleSystem.AllocateNonactive = function ( self, canvas )
  local p = Sprite ( )
  --p.blend_mode = BM_ZERO
  p.canvas = canvas
  p.depth = self.pcount / max_particles
  p.lifetime = 0
  p.visible = false

  local direction = math.rad ( self.direction )
  local speed = self.speed
  local sr = math.random ( -self.speedvariance, self.speedvariance )
  speed = self.speed + speed
  local dr = math.random ( -self.spread, self.spread )
  local a = direction + math.rad ( dr )
  p.ivx = math.cos ( a ) * speed
  p.ivy = math.sin ( a ) * speed
  p.vx = p.ivx
  p.vy = p.ivy
  p.lifetime = self.distance / speed
  p.time = 0

  table.insert ( self.nonactive, p )
  self.layer:add_child ( p )
end

ParticleSystem.CollectNonactive = function ( self, k )
  local p = table.remove ( self.nonactive, k )
  --assert ( p == pv )
  self.layer:remove_child ( p )
  --p.blend_mode = BM_NONE
  p.canvas = nil
  p.lifetime = nil
  
  p.ivx = nil
  p.ivy = nil
  p.vx = nil
  p.vy = nil
end

ParticleSystem.RefreshList = function ( self, list )
  local distance = self.distance
  local direction = math.rad ( self.direction )
  local spread = self.spread
  local speedvariance = self.speedvariance
  local speed = self.speed
  for i, v in ipairs ( list ) do
    local dr = math.random ( -spread, spread )
    local a = direction + math.rad ( dr )
    local sr = math.random ( -speedvariance, speedvariance )
    local s = speed + sr
    v.ivx = math.cos ( a ) * s
    v.ivy = math.sin ( a ) * s
    v.vx, v.vy = v.ivx, v.ivy
    v.lifetime = distance / speed
  end
end

ParticleSystem.Refresh = function ( self )
  self:RefreshList ( self.nonactive )
  self:RefreshList ( self.active )
end

ParticleSystem.SetBlendMode = function ( self, bm )
  for i, v in ipairs ( self.nonactive ) do
    v.blend_mode = bm
  end
  for i, v in ipairs ( self.active ) do
    v.blend_mode = bm
  end
end

ParticleSystem.DeactivateAll = function ( self )
  while #self.active > 0 do
    local p = table.remove ( self.active )
    p.visible = false
    table.insert ( self.nonactive, p )
  end
end

ParticleSystem.Update = function ( self, delta )
  -- desired number of particles
  self.rate = self.pcount
  while self.pcount > #self.active + #self.nonactive do
    self:AllocateNonactive ( self.canvas )
  end
  while #self.nonactive > 0 and self.pcount < #self.active + #self.nonactive do
    self:CollectNonactive ( #self.nonactive )
  end

  self.time = self.time + delta
  local gx, gy = self.gravityx, self.gravityy
  local next = 1 / self.rate
  while self.time > next do
    self.time = self.time - next
    if #self.nonactive > 0 then
      local p = table.remove ( self.nonactive )
      p.visible = true
      table.insert ( self.active, p )
    end
  end 
  local s = self.scolor
  local e = self.ecolor
  for i = #self.active, 1, -1 do
    local v = self.active[i]
    v.vx = v.vx + gx * delta
    v.vy = v.vy + gy * delta
    v.x = v.x + v.vx * delta
    v.y = v.y + v.vy * delta
    v.time = v.time + delta
    local ratio = v.time / v.lifetime

    local sa, ea = self.salpha, self.ealpha
    v.alpha = ( ea - sa ) * ratio + sa

    local c = v.color
    c.red = ( e.red - s.red ) * ratio + s.red
    c.green = ( e.green - s.green ) * ratio + s.green
    c.blue = ( e.blue - s.blue ) * ratio + s.blue

    local scale = ( self.escale - self.sscale ) * ratio + self.sscale
    v:set_scale ( scale, scale )

    if v.time > v.lifetime then
      v.x, v.y = 0, 0
      v.vx, v.vy = v.ivx, v.ivy
      v.time = 0
      local p = table.remove ( self.active, i )
      p.visible = false
      --p.color:set ( 255, 255, 255 )
      table.insert ( self.nonactive, p )
    end
  end
end

return ParticleSystem